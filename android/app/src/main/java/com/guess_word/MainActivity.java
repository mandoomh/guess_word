package com.guess_word;

import android.os.Bundle;
import com.facebook.react.ReactActivity;
import org.devio.rn.splashscreen.SplashScreen;
// import android.view.View;	// hide bottom bar

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "guess_word";
  }

  	@Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this, true);  // here
        super.onCreate(savedInstanceState);
    }

	// hide bottom bar
	// @Override
    // public void onWindowFocusChanged(boolean hasFocus) {
    //     super.onWindowFocusChanged(hasFocus);
    //     if (hasFocus) {
    //         hideNavigationBar();
    //     }
    // }

    // private void hideNavigationBar() {
    //     getWindow().getDecorView().setSystemUiVisibility(
    //         View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    //         | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    // }
	// \\hide bottom bar
}
