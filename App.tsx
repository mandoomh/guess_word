import React from 'react';
import { Provider } from 'react-redux';

import Main from './src/main';
import { rootStore, persistor } from './src/store/index';
import { PersistGate } from 'redux-persist/integration/react';

const App = () => {
	return (
		<Provider store={rootStore}>
			<PersistGate loading={null} persistor={persistor}>
				<Main />
			</PersistGate>
		</Provider>
	);
};

export default App;
