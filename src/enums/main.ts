export enum LanguageEnum {
	English = 'ENGLISH',
}

export enum PageEnum {
	Menu = 'MENU',
	Stage = 'STAGE',
	Game = 'GAME',
	StageClear = 'STAGE_CLEAR',
}

export enum ColorEnum {
	DarkGray = 'rgb(71, 71, 73)',
	Gray = 'rgb(80, 97, 103)',
	Yellow = 'rgb(240, 215, 103)',	// 아이콘 배경색: 239, 226, 94
	Pink = 'rgb(247, 124, 104)',
	LightGray = 'rgb(198, 198, 198)',
	White = 'rgb(255, 255, 255)',
	Green = 'rgb(75, 157, 100)',
	// CSS에서 별도로 opacity를 적용하면 자식 컴포넌트가 별도의 opacity를 가질 수 없어 rgba를 사용함.
	TransparentLightGray = 'rgba(198, 198, 198, 0.8)',
	TransparentPink = 'rgba(247, 124, 104, 0.9)',
	TransparentWhite = 'rgba(255, 255, 255, 0.6)',
	Red = 'rgb(235, 75, 53)',
	Blue = 'rgb(46, 119, 246)',
}

export enum StageStatus {
	Guessed = 'Guessed',
	Guessing = 'Guessing',
	Locked = 'Locked',
}

export enum ClueStatus {
	Opened = 'Opened',
	Closed = 'Closed',
}

export enum InputStatus {
	None = 'None',
	Focused = 'Focused',
	Hinted = 'Hinted',
	// 띄어쓰기를 위한 공백문자인 경우 사용.
	Blank = 'Blank',
}

export enum UpdatingPencilCount {
	OpenHint = -50,
	OpenClue = -1,
	GuessWord = 2,
	GiftBonus = 10,
	AdsBonus = 50,
	InAppPurchaseFirstItemBonus = 500,
}

export enum ModalType {
	None = 'None',
	OpenHint = 'OpenHint',
	GetPencil = 'GetPencil',
	Setting = 'Setting',
	Status = 'Status',
}

export enum ConsentStatus {
	// 아직 동의를 하지 않았거나 상태 알 수 없음. 동의 필요.
	UNKNOWN = 0,
	// 비맞춤형 광고만 동의.
	NON_PERSONALIZED = 1,
	// 맞춤형 광고 동의.
	PERSONALIZED = 2,
}

export enum SoundEffect {
	// android는 파일 제목을 소문자+언더바 형식으로만 사용(대문자 불가)하여야 한다.
	// 따라서 ios도 동일한 형식을 사용.
	TouchButton = 'touch_button.mp3',
	InsertInput = 'insert_input.mp3',
	DeleteInput = 'delete_input.mp3',
	OpenClue = 'open_clue.mp3',
	OpenHint = 'open_hint.mp3',
	GetPencil = 'get_pencil.mp3',
	SpendPencil = 'spend_pencil.mp3',
	WordIsGuessed = 'word_is_guessed.mp3',
	WordIsWrong = 'word_is_wrong.mp3',
	GoNextWord = 'go_next_word.mp3',
	StageClear = 'stage_clear.mp3',
	NoticeCluesAndHint = 'notice_clues_and_hint.mp3',
}

export enum SoundBgm {
	Bgm01 = 'bgm01.mp3',
}