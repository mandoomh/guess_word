import React, { useMemo } from 'react';
import {
	View,
	StyleSheet,
	NativeSyntheticEvent,
	NativeTouchEvent
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { faLock, faCheckDouble } from '@fortawesome/free-solid-svg-icons';

import SlideDownView from './util/slideDownView';
import CustomButton from './util/customButton';
import CustomText from './util/customText';
import Icon from './util/icon';
import { AppThunkDispatch, movePage } from '../store/mains/thunk';
import { IRootState, IStage } from '../interfaces';
import { stages, commonStyles, getNextStageWordIndex, fontSizes, playSound } from '../constants';
import { PageEnum, ColorEnum, StageStatus, SoundEffect } from '../enums';

const getStageStatus = (stageIndex: number, currentStageIndex: number) => {
	if (stageIndex < currentStageIndex) {
		return StageStatus.Guessed;
	} else if (stageIndex === currentStageIndex) {
		return StageStatus.Guessing;
	} else {
		return StageStatus.Locked;
	}
};

const getLines = (currentStageIndex: number) => {
	const stageCountPerLine = 3;
	const lines: IStage[][] = [];

	const stagesLength = stages.length;
	let lineIndex = 0;
	for (let stageIndex = 0; stageIndex < stagesLength; stageIndex++) {
		const rest = stageIndex % stageCountPerLine;
		if (rest === 0) {
			lines[lineIndex] = [];
		}

		const stageStatus = getStageStatus(stageIndex, currentStageIndex);
		lines[lineIndex].push({ stageIndex, status: stageStatus });

		if (rest === stageCountPerLine - 1) {
			lineIndex++;
		}
	}

	return lines;
};

const getIcon = (isShowIcon: boolean, status: StageStatus, stageIndex: number) => {
	const icon = status === StageStatus.Guessed ? faCheckDouble : faLock;

	return isShowIcon ? (
		<View key={`stage-icon-${stageIndex}`} style={stageIconStyles.viewStageIcon}>
			<Icon iconType={icon} iconSize={wp('6%')} iconStyle={stageButtonStyles[status].iconColor} />
		</View>
	) : null;
};

const getStageItem = (stage: IStage, touchOnStage: () => void) => {
	const { status, stageIndex } = stage;

	const thisStageButtonStyle = stageButtonStyles[status];

	const isShowIcon = [StageStatus.Guessed, StageStatus.Locked].includes(status);

	return (
		<View key={`stage-${stageIndex}`} style={styles.viewStageItem}>
			<CustomButton
				content={
					<View>
						<CustomText
							text={`Stage ${stageIndex + 1}`}
							textStyle={{ ...styles.buttonText, ...thisStageButtonStyle.stageText }}
						/>
					</View>
				}
				onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
					touchOnStage();
				}}
				buttonStyle={{ ...styles.button, ...thisStageButtonStyle.button, ...(isShowIcon && stageIconStyles.opaque) }}
				disabled={status !== StageStatus.Guessing}
			/>
			{getIcon(isShowIcon, status, stageIndex)}
		</View>
	);
};

interface IProps {
	dispatch: AppThunkDispatch;
	state: IRootState;
}

const Stage = (props: IProps) => {
	const { dispatch, state } = props;
	const { wordGuessedIndexes } = state;

	const stageLines = useMemo(() => {
		const { nextStageIndex: thisStageIndex } = getNextStageWordIndex(wordGuessedIndexes);
		return getLines(thisStageIndex);
	}, [wordGuessedIndexes]);

	const moveGamePage = () => {
		playSound(SoundEffect.TouchButton);
		dispatch(movePage(PageEnum.Game));
	};

	return (
		<SlideDownView
			content={(
				<>
					<View style={styles.viewTitle}>
						<CustomText text="Stage." textStyle={styles.title} />
					</View>
					<View style={styles.viewStageContainer}>
						{stageLines.map((line, lineIndex) => {
							return (
								<View key={`line-${lineIndex}`} style={styles.viewStageLine}>
									{line.map((stage) => {
										return getStageItem(stage, moveGamePage);
									})}
								</View>
							);
						})}
						<View style={styles.viewStageLine}>
							<View
								style={styles.continueView}
							>
								<CustomText
									text='To Be Continued.'
									textStyle={styles.continueText}
								/>
							</View>
						</View>
					</View>
				</>
			)}
		/>
	);
};

const styles = StyleSheet.create({
	viewTitle: {
		height: hp('16%'), color: ColorEnum.Yellow,
		paddingVertical: hp('4%'), ...commonStyles.alignCenter
	},
	viewStageContainer: {
		flex: 1, paddingHorizontal: wp('8%'), color: ColorEnum.Yellow
	},
	viewStageLine: {
		flexDirection: 'row', height: hp('8%'), marginBottom: hp('0.5%'), color: ColorEnum.Yellow
	},
	viewStageItem: {
		flex: 1, height: hp('8%')
	},
	continueView: {
		flex: 1, height: hp('6%'), ...commonStyles.buttonRadius, ...commonStyles.alignCenter,
		backgroundColor: ColorEnum.Gray, margin: hp('1%'), opacity: 0.6
	},
	continueText: {
		fontSize: fontSizes.medium, ...commonStyles.bold, color: ColorEnum.White
	},
	title: {
		fontSize: fontSizes.huge, color: ColorEnum.Gray, ...commonStyles.bold
	},
	buttonText: {
		fontSize: fontSizes.medium, ...commonStyles.bold
	},
	button: {
		margin: hp('1%'), ...commonStyles.buttonRadius
	},
});

const stageIconStyles = StyleSheet.create({
	opaque: {
		opacity: 0.6
	},
	viewStageIcon: {
		top: 0, left: 0, right: 0, bottom: 0, position: 'absolute', ...commonStyles.alignCenter
	},
});

const stageButtonStyles = {
	[StageStatus.Guessed]: StyleSheet.create({
		stageText: {
			color: ColorEnum.White,
		},
		button: {
			backgroundColor: ColorEnum.Pink,
			borderColor: ColorEnum.Pink,
		},
		iconColor: {
			color: ColorEnum.Gray
		},
	}),
	[StageStatus.Guessing]: StyleSheet.create({
		stageText: {
			color: ColorEnum.White,
		},
		button: {
			backgroundColor: ColorEnum.Pink,
			borderColor: ColorEnum.Pink,
		},
		iconColor: {}
	}),
	[StageStatus.Locked]: StyleSheet.create({
		stageText: {
			color: ColorEnum.White,
		},
		button: {
			backgroundColor: ColorEnum.Gray,
			borderColor: ColorEnum.Gray,
			opacity: 1
		},
		iconColor: {
			color: ColorEnum.Gray
		},
	}),
};

export default Stage;