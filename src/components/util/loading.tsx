import React from 'react';
import {
	StyleSheet,
	View,
	ActivityIndicator
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { commonStyles } from '../../constants';
import { ColorEnum } from '../../enums';

interface IProps {
	isLoading: boolean;
}

const Loading = (props: IProps) => {
	const { isLoading } = props;

	return isLoading ? (
		<View style={loadingStyles.container}>
			<ActivityIndicator size="large" animating={isLoading} />
		</View>
	) : null;
};

const loadingStyles = StyleSheet.create({
	container: {
		position: 'absolute',
		flex: 1,
		width: wp('100%'),
		height: hp('100%'),
		backgroundColor: ColorEnum.LightGray,
		opacity: 0.5,
		...commonStyles.alignCenter
	},
});

export default Loading;