import React, { useState, useRef } from 'react';
import {
	View,
	StyleSheet,
	Image,
	NativeSyntheticEvent,
	NativeTouchEvent
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import Swiper from 'react-native-swiper';

import CustomButton from './customButton';
import CustomText from './customText';
import { AppThunkDispatch, setIsShowHelp } from '../../store/mains/thunk';
import { ColorEnum, SoundEffect } from '../../enums';
import { commonStyles, fontSizes, playSound } from '../../constants';

const styles = StyleSheet.create({
	image: {
		height: '100%', width: '100%', resizeMode: 'contain'
	},

});

const HelpImages = [{
	image:
		<Image
			source={{ uri: 'help_01' }}
			style={styles.image}
		/>
}, {
	image:
		<Image
			source={{ uri: 'help_02' }}
			style={styles.image}
		/>
}, {
	image:
		<Image
			source={{ uri: 'help_03' }}
			style={styles.image}
		/>
}, {
	image:
		<Image
			source={{ uri: 'help_04' }}
			style={styles.image}
		/>
}, {
	image:
		<Image
			source={{ uri: 'help_05' }}
			style={styles.image}
		/>
}];

interface IProps {
	dispatch: AppThunkDispatch;
}

const swipeDotBottom = 25;

const Help = (props: IProps) => {
	const { dispatch } = props;

	const [swipeIndex, setSwipeIndex] = useState(0);

	const swiper = useRef(null);

	const lastIndex = HelpImages.length - 1;

	const isLast = swipeIndex === lastIndex;

	return (
		<>
			<Swiper
				ref={swiper}
				style={{}} index={0} showsButtons={false} loop={false}
				dot={<View style={swiperStyles.dot} />}
				activeDot={<View style={swiperStyles.activeDot} />}
				buttonWrapperStyle={swiperStyles.button}
				onIndexChanged={(newIndex: number) => setSwipeIndex(newIndex)}
			>
				{HelpImages.map((imageItem, index) => {
					return (
						<View key={index} style={helpImageStyles.containerView}>
							<View style={helpImageStyles.padding} />
							<View style={helpImageStyles.slide}>
								{imageItem.image}
							</View>
							<View style={helpImageStyles.padding} />
						</View>
					);
				})}
			</Swiper>
			{!isLast ? (
				<View style={skipButtonStyles.view}>
					<CustomButton
						content={<CustomText text='Skip' textStyle={skipButtonStyles.text} />}
						onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
							if (swiper && swiper.current) {
								playSound(SoundEffect.TouchButton);
								dispatch(setIsShowHelp(false));
							}
						}}
						buttonStyle={{}}
					/>
				</View>
			) : null}
			<View style={nextButtonStyles.view}>
				<CustomButton
					content={<CustomText text={isLast ? 'Done' : 'Next'} textStyle={skipButtonStyles.text} />}
					onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
						if (swiper && swiper.current) {
							playSound(SoundEffect.TouchButton);
							if (isLast) {
								dispatch(setIsShowHelp(false));
							} else {
								// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
								// @ts-ignore
								(swiper.current as unknown as React.MutableRefObject<Swiper>).scrollBy(1, true);
							}
						}
					}}
					buttonStyle={{}}
				/>
			</View>
		</>
	);
};

const swiperStyles = StyleSheet.create({
	dot: {
		backgroundColor: ColorEnum.TransparentWhite, width: wp('3%'), height: wp('3%'), borderRadius: 100, marginLeft: wp('2%'),
		marginRight: wp('2%'), marginBottom: hp('1%')
	},
	activeDot: {
		backgroundColor: ColorEnum.Yellow, width: wp('3%'), height: wp('3%'), borderRadius: 100, marginLeft: wp('2%'),
		marginRight: wp('2%'), marginBottom: hp('1%')
	},
	button: {
		backgroundColor: 'transparent', flexDirection: 'row', position: 'absolute', top: 0, left: 0, flex: 1, paddingHorizontal: 10,
		paddingVertical: 10, justifyContent: 'space-between', alignItems: 'flex-end'
	},
});

const helpImageStyles = StyleSheet.create({
	containerView: {
		flex: 1, backgroundColor: ColorEnum.DarkGray
	},
	slide: {
		flex: 7, justifyContent: 'center', alignItems: 'center', backgroundColor: ColorEnum.DarkGray
	},
	padding: {
		flex: 1
	},
});

const skipButtonStyles = StyleSheet.create({
	view: {
		position: 'absolute', left: wp('7%'), bottom: (swipeDotBottom + hp('1%'))
	},
	text: {
		color: ColorEnum.Yellow, fontSize: fontSizes.bigSmall, ...commonStyles.bold
	},
});

const nextButtonStyles = StyleSheet.create({
	view: {
		position: 'absolute', right: wp('7%'), bottom: (swipeDotBottom + hp('1%'))
	},
	text: {
		color: ColorEnum.Yellow, fontSize: fontSizes.bigSmall, ...commonStyles.bold
	},
});

export default Help;