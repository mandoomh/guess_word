/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
	StyleSheet,
	View,
	NativeSyntheticEvent,
	NativeTouchEvent
} from 'react-native';

import { ColorEnum, SoundEffect } from '../../enums';
import { IRootState } from '../../interfaces';
import { commonStyles, fontSizes, playSound } from '../../constants';
import CustomButton from './customButton';
import CustomText from './customText';
import {
	AppThunkDispatch, debugAllReset, debugSetLastWord, initConsent,
	debugSetNowGetPencilDate, debugSetWordGuessed
} from '../../store/mains/thunk';

interface IProps {
	dispatch: AppThunkDispatch;
	state: IRootState;
}

/* 디버그 용 버튼 */
const Debug = (props: IProps) => {
	const showDebug = 0;

	return showDebug ? (
		<View style={styles.viewContainer}>
			{__DEV__ ? (
				<View style={{ ...styles.viewLine }}>
					<View style={{ ...styles.viewItem }}>
						<CustomButton
							content={<CustomText text='모두 초기화' textStyle={styles.buttonText} />}
							onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
								props.dispatch(debugAllReset());
							}}
							buttonStyle={styles.button}
						/>
					</View>
					<View style={{ ...styles.viewItem }}>
						<CustomButton
							content={<CustomText text='단어이동' textStyle={styles.buttonText} />}
							onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
								// props.dispatch(debugSetLastWord());

								// playSound(SoundEffect.WordIsGuessed, true);
								props.dispatch(debugSetWordGuessed());
							}}
							buttonStyle={styles.button}
						/>
					</View>
					<View style={{ ...styles.viewItem }}>
						<CustomButton
							content={<CustomText text='동의초기화' textStyle={styles.buttonText} />}
							onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
								props.dispatch(initConsent());
							}}
							buttonStyle={styles.button}
						/>
					</View>
					<View style={{ ...styles.viewItem }}>
						<CustomButton
							content={<CustomText text='시간초기화' textStyle={styles.buttonText} />}
							onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
								props.dispatch(debugSetNowGetPencilDate());
							}}
							buttonStyle={styles.button}
						/>
					</View>
				</View>
			) : null}
		</View>
	) : null;
};

const styles = StyleSheet.create({
	viewContainer: {
		position: 'absolute', bottom: 0, left: 0, width: '100%', ...commonStyles.alignCenter
	},
	viewLine: {
		flex: 1, flexDirection: 'row'
	},
	viewItem: {
		flex: 1, marginLeft: 5, marginRight: 5
	},
	buttonText: {
		fontSize: fontSizes.small, color: ColorEnum.White, padding: 5
	},
	button: {
		backgroundColor: ColorEnum.Green, borderColor: ColorEnum.Green,
	},
});

export default Debug;