import React from 'react';
import {
	StyleSheet,
} from 'react-native';
import * as Animatable from 'react-native-animatable';

interface IProps {
	content: JSX.Element;
}

const SlideDownView = (props: IProps) => {
	return (
		<Animatable.View animation='fadeIn' direction='alternate' style={styles.containerView}>
			{props.content}
		</Animatable.View>
	);
};

const styles = StyleSheet.create({
	containerView: {
		flex: 1,
	},
});

export default SlideDownView;