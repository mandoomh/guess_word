import React from 'react';
import { View, StyleProp, ViewStyle, Image, GestureResponderEvent, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import CustomButton from '../util/customButton';
import CustomText from './customText';
import { UpdatingPencilCount } from '../../enums';
import { commonStyles, fontSizes } from '../../constants';

interface IProps {
	innerViewStyle: StyleProp<ViewStyle>;
	textStyle: Record<string, unknown>;
	pencilStyle: Record<string, unknown>;
	buttonStyle: Record<string, unknown>;
	onTouch: () => void;
	remainingTime: number;
}

const Timer = (props: IProps) => {
	const { innerViewStyle, textStyle, pencilStyle, buttonStyle, onTouch, remainingTime } = props;

	const isReadyToGet = remainingTime < 0;
	const timeText = isReadyToGet ? `+${UpdatingPencilCount.GiftBonus}` : new Date(remainingTime).toISOString().substr(11, 8);

	const handleOnPress = (event: GestureResponderEvent) => {
		isReadyToGet ? onTouch() : () => { };
	};

	return (
		<CustomButton
			content={(
				<View style={innerViewStyle}>
					<View >
						<View style={{ ...commonStyles.alignCenter }}>
							<Image
								source={{ uri: 'gift' }}
								style={pencilStyle}
							/>
						</View>
					</View>
					<View style={styles.textView}>
						<CustomText
							text={timeText}
							textStyle={{ ...textStyle, fontSize: isReadyToGet ? fontSizes.medium : fontSizes.small }}
						/>
					</View>
				</View>
			)}
			onPress={handleOnPress}
			buttonStyle={buttonStyle}
			activeOpacity={isReadyToGet ? 0 : 1}
		/>
	);
};

const styles = StyleSheet.create({
	textView: {
		flex: 1, marginLeft: wp('1%'), justifyContent: 'center', alignItems: 'flex-start'
	},
});

export default Timer;