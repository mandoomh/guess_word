import React from 'react';
import { Text, StyleProp, TextStyle } from 'react-native';

interface IProps {
	text: string;
	textStyle: StyleProp<TextStyle>;
}

const CustomText = (props: IProps) => {
	return (
		<Text style={props.textStyle}>
			{props.text}
		</Text>
	);
};

export default CustomText;