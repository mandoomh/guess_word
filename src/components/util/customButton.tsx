import React from 'react';
import {
	TouchableOpacity,
	StyleSheet,
	GestureResponderEvent,
} from 'react-native';

const defaultActiveOpacity = 0.2;

interface IProps {
	onPress: (event: GestureResponderEvent) => void;
	onLongPress?: (event: GestureResponderEvent) => void;
	buttonStyle?: Record<string, any>;	// eslint-disable-line @typescript-eslint/no-explicit-any
	disabled?: boolean;
	activeOpacity?: number;
	content: JSX.Element;
}

const CustomButton = (props: IProps) => {
	const { onPress, content, onLongPress } = props;
	return (
		<TouchableOpacity
			onPress={onPress}
			onLongPress={onLongPress}
			style={{ ...styles.button, ...(props.buttonStyle && props.buttonStyle) }}
			disabled={props.disabled ? props.disabled : false}
			// activeOpacity를 1로 설정하면 클릭 애니메이션이 제거된다.
			activeOpacity={props.activeOpacity ? props.activeOpacity : defaultActiveOpacity}
		>
			{content}
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	button: {
		flex: 1, alignItems: 'center', justifyContent: 'center',
	},
});

export default CustomButton;