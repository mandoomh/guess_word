import React from 'react';
import { StyleProp, TextStyle } from 'react-native';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import * as Animatable from 'react-native-animatable';

import { AnimationType } from '../../interfaces';

interface IProps {
	text: string;
	textStyle: StyleProp<TextStyle>;
	animation: AnimationType;
	componentKey: string | number;
	duration: number;
}

const AnimationText = (props: IProps) => {
	const { text, textStyle, animation, componentKey, duration } = props;

	return (
		<Animatable.Text
			// animatable은 최초 render시에만 animation이 동작한다.
			// 따라서 강제 animation을 위해 key값을 갱신한다.
			key={componentKey}
			animation={animation}
			duration={duration}
			style={textStyle}
		>
			{text}
		</Animatable.Text>
	);
};

export default AnimationText;