import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

interface IProps {
	iconType: IconDefinition;
	iconSize: number;
	iconStyle: Record<string, any>;	// eslint-disable-line @typescript-eslint/no-explicit-any
}

const Icon = (props: IProps) => {
	const { iconType, iconSize, iconStyle } = props;
	return (
		<FontAwesomeIcon icon={iconType} size={iconSize} style={iconStyle} />
	);
};

export default Icon;