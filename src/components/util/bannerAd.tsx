import React from 'react';
import { Platform } from 'react-native';
import { BannerAd, BannerAdSize, TestIds } from '@react-native-firebase/admob';

interface IProps {
	isShow: boolean;
	showNonPersonalizedAdsOnly: boolean;
}

const adUnitId = __DEV__ ? TestIds.BANNER : (Platform.OS === 'ios' ? 'ca-app-pub-7668923106539535/9723097141' : 'ca-app-pub-7668923106539535/2599968751');

const CustomBannerAd = (props: IProps) => {
	const { isShow, showNonPersonalizedAdsOnly } = props;

	return isShow ? (
		<BannerAd
			unitId={adUnitId}
			size={BannerAdSize.SMART_BANNER}
			requestOptions={{
				requestNonPersonalizedAdsOnly: showNonPersonalizedAdsOnly,
			}}
			onAdLoaded={() => { }}
			onAdFailedToLoad={() => { }}
			onAdOpened={() => { }}
			onAdClosed={() => { }}
			onAdLeftApplication={() => { }}
		/>
	) : null;
};

export default CustomBannerAd;