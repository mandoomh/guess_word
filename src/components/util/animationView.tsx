import React from 'react';
import { StyleProp, TextStyle } from 'react-native';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import * as Animatable from 'react-native-animatable';

import { AnimationType } from '../../interfaces';

interface IProps {
	content: JSX.Element;
	viewStyle: StyleProp<TextStyle>;
	animation: AnimationType;
	componentKey: string | number;
	duration: number;
	iterationCount: number | 'infinite';
	delay?: number;
	iterationDelay?: number;
}

const AnimationView = (props: IProps) => {
	const { content, viewStyle, animation, componentKey, duration, iterationCount, delay, iterationDelay } = props;

	return (
		<Animatable.View
			// animatable은 최초 render시에만 animation이 동작한다.
			// 따라서 강제로 animation이 동작하도록 하기 위해 key값을 갱신한다.
			key={componentKey}
			animation={animation}
			duration={duration}
			style={viewStyle}
			iterationCount={iterationCount}
			delay={delay ? delay : 0}
			iterationDelay={iterationDelay ? iterationDelay : 0}
		>
			{content}
		</Animatable.View>
	);
};

export default AnimationView;