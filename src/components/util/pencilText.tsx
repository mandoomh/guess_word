import React from 'react';
import { View, StyleProp, TextStyle, StyleSheet } from 'react-native';
import { faPen } from '@fortawesome/free-solid-svg-icons';

import Icon from './icon';
import AnimationText from './animationText';
import { commonStyles } from '../../constants';
import { AnimationType } from '../../interfaces';

interface IProps {
	iconSize: number;
	iconStyle: Record<string, unknown>;
	textStyle: StyleProp<TextStyle>;
	animation: AnimationType;
	duration: number;
	pencilCount: number;
	textComponentKey: string | number;
}

const PencilText = (props: IProps) => {
	const { iconSize, iconStyle, textStyle, animation, duration, pencilCount, textComponentKey } = props;

	return (
		<View style={styles.view}>
			<Icon iconType={faPen} iconSize={iconSize} iconStyle={iconStyle} />
			<AnimationText
				componentKey={textComponentKey}
				animation={animation}
				duration={duration}
				// 숫자 3자리마다 쉼표를 붙이기 위해 toLocaleString 사용.
				text={pencilCount.toLocaleString('en')}
				textStyle={textStyle}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	view: {
		flex: 1, flexDirection: 'row', ...commonStyles.alignCenter
	},
});

export default PencilText;