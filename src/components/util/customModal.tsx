import React from 'react';
import { Modal, View, StyleSheet, TouchableOpacity, NativeSyntheticEvent, NativeTouchEvent } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

import { commonStyles } from '../../constants';
import { AppThunkDispatch, hideModal } from '../../store/mains/thunk';
import { ColorEnum } from '../../enums';
import CustomButton from './customButton';
import Icon from './icon';

interface IProps {
	isOpen: boolean;
	useCloseButton: boolean;
	content: JSX.Element;
	dispatch: AppThunkDispatch;
}

const CustomModal = (props: IProps) => {
	const { isOpen, useCloseButton, content, dispatch } = props;

	return (
		<Modal
			animationType="fade"
			transparent={true}
			visible={isOpen}
		>
			<TouchableOpacity
				// 모달 창 바깥 영역을 터치 시 닫는다.
				onPress={() => { dispatch(hideModal()); }}
				// 터치 애니메이션 비활성화.
				activeOpacity={1}
				style={styles.containerView}
			>
				<View style={styles.mainView}>
					<TouchableOpacity
						// 모달 창 영역을 터치 시에는 아무 동작하지 않음.
						onPress={() => { }}
						activeOpacity={1}
					>
						{useCloseButton ? (
							<View style={styles.closeView}>
								<CustomButton
									content={(
										<Icon iconType={faTimesCircle} iconSize={wp('7.5%')} iconStyle={styles.closeIcon} />
									)}
									onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
										dispatch(hideModal());
									}}
								/>
							</View>
						) : null}
						{content}
					</TouchableOpacity>
				</View>
			</TouchableOpacity>
		</Modal>
	);
};

const styles = StyleSheet.create({
	containerView: {
		flex: 1,
	},
	mainView: {
		flex: 1,
		// 모달 창 이외의 부분은 반투명하게 표시.
		backgroundColor: ColorEnum.TransparentLightGray,
		...commonStyles.alignCenter
	},
	closeView: {
		position: 'absolute',
		right: wp('3%'),
		top: wp('3%'),
		// 버튼이 뒤에 숨겨져 있어 클릭이 되지 않아 앞으로 당김.
		zIndex: 10
	},
	closeIcon: {
		color: ColorEnum.LightGray
	},
});

export default CustomModal;