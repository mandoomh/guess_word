import React from 'react';

import GetPencilModal from './getPencilModal';
import HintModal from './hintModal';
import SettingModal from './settingModal';
import StatusModal from './statusModal';
import { AppThunkDispatch } from '../../store/mains/thunk';
import { ModalType } from '../../enums';
import { IRootState } from '../../interfaces';

const getModal = (dispatch: AppThunkDispatch, state: IRootState) => {
	switch (state.modalToShow) {
		case ModalType.GetPencil:
			return <GetPencilModal dispatch={dispatch} isOpen={true} state={state} />;
		case ModalType.OpenHint:
			return <HintModal dispatch={dispatch} isOpen={true} />;
		case ModalType.Setting:
			return <SettingModal dispatch={dispatch} isOpen={true} state={state} />;
		case ModalType.Status:
			return <StatusModal dispatch={dispatch} isOpen={true} state={state} />;
		default:
			return null;
	}
};

interface IProps {
	dispatch: AppThunkDispatch;
	state: IRootState;
}

const MainModal = (props: IProps) => {
	const { dispatch, state } = props;

	return getModal(dispatch, state);
};

export default MainModal;