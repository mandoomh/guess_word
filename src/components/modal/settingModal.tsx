import React from 'react';
import {
	View,
	NativeSyntheticEvent,
	NativeTouchEvent,
	StyleSheet,
	Linking,
	Switch
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { faFontAwesome } from '@fortawesome/free-brands-svg-icons';

import CustomButton from '../util/customButton';
import CustomModal from '../util/customModal';
import CustomText from '../util/customText';
import Icon from '../util/icon';
import { fontSizes, alertMessage, commonStyles, showMessage, playSound } from '../../constants';
import { AppThunkDispatch, hideModal, changeSoundOn, changeMusicOn, initConsent, setIsShowHelp } from '../../store/mains/thunk';
import { ColorEnum, SoundEffect } from '../../enums';
import { IRootState } from '../../interfaces';

const openUrl = (url: string) => {
	Linking.canOpenURL(url)
		.then(supported => {
			Linking.openURL(url);
		});
};

const getContent = (dispatch: AppThunkDispatch, state: IRootState) => {
	const { isNeedConsent, isSoundOn, isMusicOn } = state;

	return (
		<View style={mainStyles.mainView}>
			<View style={titleStyles.containerView}>
				<CustomText text='Setting' textStyle={titleStyles.text} />
			</View>

			<View style={mainStyles.paddingView} />

			<View style={linesStyles.containerView}>
				<View style={linesStyles.lineView}>
					<View style={linesStyles.buttonView}>
						<View style={linesStyles.leftView}>
							<CustomText text='Sound' textStyle={linesStyles.text} />
						</View>
						<View style={linesStyles.rightView}>
							<Switch
								trackColor={{ true: ColorEnum.DarkGray, false: ColorEnum.DarkGray }}
								thumbColor={isSoundOn ? ColorEnum.Yellow : ColorEnum.White}
								ios_backgroundColor={ColorEnum.DarkGray}
								onValueChange={(newValue: boolean) => { playSound(SoundEffect.TouchButton); dispatch(changeSoundOn(newValue)); }}
								value={isSoundOn}
							/>
						</View>
					</View>
				</View>
				<View style={linesStyles.lineView}>
					<View style={linesStyles.buttonView}>
						<View style={linesStyles.leftView}>
							<CustomText text='Music' textStyle={linesStyles.text} />
						</View>
						<View style={linesStyles.rightView}>
							<Switch
								trackColor={{ true: ColorEnum.DarkGray, false: ColorEnum.DarkGray }}
								thumbColor={isMusicOn ? ColorEnum.Yellow : ColorEnum.White}
								ios_backgroundColor={ColorEnum.DarkGray}
								onValueChange={(newValue: boolean) => { playSound(SoundEffect.TouchButton); dispatch(changeMusicOn(newValue)); }}
								value={isMusicOn}
							/>
						</View>
					</View>
				</View>
				{isNeedConsent ? (
					<View style={{ ...linesStyles.lineView, ...linesStyles.buttonView }}>
						<View style={linesStyles.leftView}>
							<CustomText text='Reset Ad Consent' textStyle={linesStyles.text} />
						</View>
						<View style={linesStyles.rightView}>
							<CustomButton
								content={
									<View style={linesStyles.buttonView}>
										<Icon iconType={faChevronRight} iconSize={hp('3%')} iconStyle={linesStyles.resetIcon} />
									</View>
								}
								onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
									playSound(SoundEffect.TouchButton);
									dispatch(initConsent());
									showMessage(alertMessage.InitAdConsent);
								}}
								buttonStyle={linesStyles.button}
							/>
						</View>
					</View>
				) : null}
				<View style={{ ...linesStyles.lineView, ...linesStyles.buttonView }}>
					<View style={linesStyles.leftView}>
						<CustomText text='Guide' textStyle={linesStyles.text} />
					</View>
					<View style={linesStyles.rightView}>
						<CustomButton
							content={
								<View style={linesStyles.buttonView}>
									<Icon iconType={faChevronRight} iconSize={hp('3%')} iconStyle={linesStyles.guideIcon} />
								</View>
							}
							onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
								playSound(SoundEffect.TouchButton);
								dispatch(hideModal());
								dispatch(setIsShowHelp(true));
							}}
							buttonStyle={linesStyles.button}
						/>
					</View>
				</View>

				<View style={linesStyles.licenseLineView}>
					<View style={{ ...linesStyles.leftView, flex: 1 }}>
						<CustomText text='License' textStyle={linesStyles.text} />
					</View>
					<View style={linesStyles.rightView}>
						<CustomButton
							content={
								<View style={linesStyles.icons8View}>
									<CustomText text='ICONS8' textStyle={linesStyles.icons8Text} />
								</View>
							}
							onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
								playSound(SoundEffect.TouchButton);
								const icons8Url = 'https://icons8.com';
								openUrl(icons8Url);
							}}
						/>
						<CustomButton
							content={
								<View style={linesStyles.fontAwesomeView}>
									<Icon iconType={faFontAwesome} iconSize={hp('3%')} iconStyle={linesStyles.fontAwesomeIcon} />
									<CustomText text='Font Awesome' textStyle={linesStyles.fontAwesomeText} />
								</View>
							}
							onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
								playSound(SoundEffect.TouchButton);
								const fontAwesomeUrl = 'https://fontawesome.com';
								openUrl(fontAwesomeUrl);
							}}
						/>
						<CustomButton
							content={
								<View style={linesStyles.gamesoundView}>
									<CustomText text='Gamesound' textStyle={linesStyles.gamesoundText} />
								</View>
							}
							onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
								playSound(SoundEffect.TouchButton);
								const gameSoundUrl = 'http://Gamesound.or.kr';
								openUrl(gameSoundUrl);
							}}
						/>
					</View>
				</View>
			</View>
		</View>
	);
};

interface IProps {
	dispatch: AppThunkDispatch;
	isOpen: boolean;
	state: IRootState;
}

const SettingModal = (props: IProps) => {
	const { dispatch, state, isOpen } = props;

	return (
		<CustomModal
			isOpen={isOpen}
			content={getContent(dispatch, state)}
			dispatch={dispatch}
			useCloseButton={true}
		/>
	);
};

const mainStyles = StyleSheet.create({
	mainView: {
		backgroundColor: ColorEnum.Gray,
		width: wp('90%'),
		padding: wp('7%'),
		...commonStyles.buttonRadius
	},
	paddingView: {
		height: wp('7%'),
	},
});

const titleStyles = StyleSheet.create({
	containerView: {
		height: wp('7%'),
		paddingHorizontal: wp('0.5%'),
	},
	text: {
		color: ColorEnum.Pink,
		fontSize: fontSizes.big,
		...commonStyles.bold
	},
});

const borderBottom = {
	borderBottomWidth: 1,
	borderBottomColor: ColorEnum.LightGray,
};

const borderlessBottom = {
	borderBottomWidth: 0,
};

const linesStyles = StyleSheet.create({
	containerView: {
		paddingHorizontal: wp('4%'),
		borderWidth: 1,
		borderColor: ColorEnum.LightGray,
		...commonStyles.buttonRadius
	},
	lineView: {
		height: hp('9%'),
		paddingVertical: hp('2.5%'),
		...borderBottom
	},
	buttonView: {
		flexDirection: 'row',
		...commonStyles.alignCenter,
	},
	licenseLineView: {
		height: hp('14%'),
		paddingVertical: hp('2.5%'),
		flexDirection: 'row',
		...commonStyles.alignCenter,
		...borderlessBottom
	},
	leftView: {
		flex: 2,
		alignItems: 'flex-start',
	},
	rightView: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'flex-end',
	},
	text: {
		color: ColorEnum.Yellow,
		fontSize: fontSizes.medium,
		...commonStyles.bold
	},
	soundIconView: {
		width: wp('8%'), height: hp('3%')
	},
	soundIcon: {
		color: ColorEnum.Pink,
	},
	resetIcon: {
		color: ColorEnum.White,
	},
	guideIcon: {
		color: ColorEnum.White,
	},
	icons8View: {
		alignItems: 'center', justifyContent: 'flex-end'
	},
	icons8Text: {
		color: ColorEnum.White, fontSize: fontSizes.medium, ...commonStyles.bold
	},
	fontAwesomeView: {
		flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'
	},
	fontAwesomeIcon: {
		color: ColorEnum.White,
	},
	fontAwesomeText: {
		color: ColorEnum.White,
		fontSize: fontSizes.medium,
		marginLeft: wp('0.7%'),
		...commonStyles.bold
	},
	gamesoundView: {
		alignItems: 'center', justifyContent: 'flex-end'
	},
	gamesoundText: {
		color: ColorEnum.White, fontSize: fontSizes.medium, ...commonStyles.bold
	},
	button: {
		height: hp('8%')
	},
	infoRightText: {
		color: ColorEnum.White,
		fontSize: fontSizes.medium,
		...commonStyles.bold
	},
});

export default SettingModal;