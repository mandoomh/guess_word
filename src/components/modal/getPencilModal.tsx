import React from 'react';
import {
	View,
	Image,
	NativeSyntheticEvent,
	NativeTouchEvent,
	StyleSheet,
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import CustomButton from '../util/customButton';
import CustomModal from '../util/customModal';
import CustomText from '../util/customText';
import { fontSizes, commonStyles, showMessage, playSound, log, runCallbackAfterInternetConnectionCheck, alertMessage } from '../../constants';
import { getIsReadyIap, requestPurchase, prepareIapProducts } from '../../constants/inAppPurchase';
import { showRewaredAd } from '../../constants/rewardedAd';
import { AppThunkDispatch, hideModal, showLoading, hideLoading } from '../../store/mains/thunk';
import { ColorEnum, UpdatingPencilCount, SoundEffect } from '../../enums';
import { IRootState, ILocalIapProduct } from '../../interfaces';

type IPencilItemInfo = ILocalIapProduct & { getText: string }

const _showRewaredAd = async (dispatch: AppThunkDispatch) => {
	// admob이 동의 화면이나 광고를 보여줄 때 내부적으로 modal을 사용하는 듯 하다.
	// modal이 이미 띄워진 상태에서 광고를 보여주려고 하면 view controller가 이미 사용 중이라고 하면서 오류가 난다.
	// 따라서 광고 표시할 때에는 항상 먼저 modal을 닫아주어야 한다.
	dispatch(hideModal());

	showRewaredAd(dispatch);
};

const getGetPencilItem = (index: number, getPencilItemInfo: IPencilItemInfo, isLast: boolean, isSoundOn: boolean) => {
	const { pencilBonus, getText, weeksHideAds, handleOnPress } = getPencilItemInfo;

	return (
		<View key={index} style={{ ...itemListStyles.adView, ...(weeksHideAds > 0 && { height: hp('18%') }), ...(isLast && { borderBottomWidth: 0 }) }}>
			<CustomButton
				content={
					<>
						<View style={itemListStyles.adFirstLineView}>
							<View style={itemListStyles.adLeftView}>
								<View style={itemListStyles.adIconView}>
									<Image
										source={{ uri: 'plus' }}
										style={itemListStyles.plusImage}
									/>
								</View>
								<View style={itemListStyles.adIconItemView}>
									<Image
										source={{ uri: 'pencil' }}
										style={itemListStyles.pencilImage}
									/>
								</View>
								<View style={itemListStyles.adIconItemView}>
									<CustomText text={pencilBonus.toLocaleString('en')} textStyle={itemListStyles.adText} />
								</View>
							</View>
							<View style={itemListStyles.adRightView}>
								<View style={itemListStyles.adRightItemView}>
									<CustomText
										text={getText}
										textStyle={itemListStyles.getText}
									/>
								</View>
							</View>
						</View>
					</>
				}
				onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
					playSound(SoundEffect.TouchButton);
					handleOnPress();
				}}
				buttonStyle={itemListStyles.itemButton}
			/>
		</View>
	);
};

const _requestPurchase = (dispatch: AppThunkDispatch, productId: string) => {
	prepareIapProducts(dispatch)
		.then(iapProducts => {
			if (getIsReadyIap(iapProducts)) {
				dispatch(hideModal());
				dispatch(showLoading());
				requestPurchase(productId);
			} else {
				throw new Error();
			}
		})
		.catch(error => {
			log.error(`_requestPurchase > err: `, error);
			dispatch(hideLoading());
			showMessage(alertMessage.SomethingWrong);
		});
};

const getGetPencilItems = (dispatch: AppThunkDispatch, state: IRootState) => {
	const { iapProducts, isSoundOn } = state;

	const getPencilItemInfos = [{
		productId: '', localizedPrice: '', pencilBonus: UpdatingPencilCount.AdsBonus,
		getText: 'Watch Ad', weeksHideAds: 0,
		handleOnPress: () => { runCallbackAfterInternetConnectionCheck(() => { _showRewaredAd(dispatch); }); }
	}, {
		...iapProducts[0], getText: iapProducts[0].localizedPrice,
		handleOnPress: () => { runCallbackAfterInternetConnectionCheck(() => { _requestPurchase(dispatch, iapProducts[0].productId); }); },
	}];

	const itemsLength = getPencilItemInfos.length;

	return getPencilItemInfos.map((getPencilItemInfo, index) => {
		const isLast = (itemsLength - 1) === index;
		return getGetPencilItem(index, getPencilItemInfo, isLast, isSoundOn);
	});
};

const getContent = (dispatch: AppThunkDispatch, state: IRootState) => {
	return (
		<View style={mainStyles.mainView}>
			<View style={titleStyles.containerView}>
				<View style={titleStyles.leftView}>
					<Image source={{ uri: 'basket' }} style={titleStyles.basketImage} />
				</View>
				<View style={titleStyles.rightView}>
					<View style={titleStyles.lineView}>
						<CustomText text={`You have`} textStyle={titleStyles.pencilText} />
						<CustomText text={` ${state.pencilCount.toLocaleString('en')} `} textStyle={titleStyles.pencilCountText} />
						<CustomText text={`pencils.`} textStyle={titleStyles.pencilText} />
					</View>
					<View style={titleStyles.lineView}>
						<CustomText text='Get more Pencils.' textStyle={titleStyles.infoText} />
					</View>
				</View>
			</View>

			<View style={mainStyles.paddingView} />

			<View style={itemListStyles.containerView}>
				{getGetPencilItems(dispatch, state)}
			</View>
		</View>
	);
};

interface IProps {
	dispatch: AppThunkDispatch;
	isOpen: boolean;
	state: IRootState;
}

const GetPencilModal = (props: IProps) => {
	const { dispatch, state, isOpen } = props;

	return (
		<CustomModal
			isOpen={isOpen}
			content={getContent(dispatch, state)}
			dispatch={dispatch}
			useCloseButton={true}
		/>
	);
};

const mainStyles = StyleSheet.create({
	mainView: {
		backgroundColor: ColorEnum.Gray,
		width: wp('90%'),
		padding: wp('7%'),
		...commonStyles.buttonRadius
	},
	paddingView: {
		height: wp('7%'),
	},
});

const titleStyles = StyleSheet.create({
	containerView: {
		height: hp('8%'),
		paddingHorizontal: wp('1%'),
		flexDirection: 'row',
	},
	leftView: {
		width: wp('14%'),
		paddingTop: hp('0.5%'),
		paddingLeft: wp('2%'),
		...commonStyles.alignCenter,
	},
	rightView: {
		flex: 1,
		paddingTop: hp('0.5%'),
		paddingLeft: wp('7%'),
	},
	lineView: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
	},
	pencilText: {
		color: ColorEnum.LightGray,
		fontSize: fontSizes.medium,
		...commonStyles.bold
	},
	pencilCountText: {
		color: ColorEnum.Pink,
		fontSize: fontSizes.big,
		...commonStyles.bold
	},
	infoText: {
		color: ColorEnum.Yellow,
		fontSize: fontSizes.big,
		...commonStyles.bold
	},
	basketImage: {
		height: wp('12%'), width: wp('12%')
	}
});

const borderBottom = {
	borderBottomWidth: 1,
	borderBottomColor: ColorEnum.LightGray,
};

const itemListStyles = StyleSheet.create({
	containerView: {
		paddingHorizontal: wp('4%'),
		borderWidth: 1,
		borderColor: ColorEnum.LightGray,
		...commonStyles.buttonRadius
	},
	adView: {
		height: hp('9%'),
		paddingVertical: hp('2.5%'),
		paddingHorizontal: wp('1%'),
		...borderBottom
	},
	adFirstLineView: {
		height: hp('4%'),
		flexDirection: 'row',
	},
	adSecondLineView: {
		height: hp('10%'),
		flexDirection: 'row',
		paddingTop: hp('1.5%'),
	},
	adLeftView: {
		flex: 1,
		flexDirection: 'row',
	},
	adRightView: {
		flex: 1,
	},
	adRightItemView: {
		flex: 1,
		alignContent: 'flex-end',
		justifyContent: 'center',
	},
	adIconView: {
		alignContent: 'center',
		justifyContent: 'center',
		paddingRight: wp('5%'),
	},
	adIconItemView: {
		alignContent: 'center',
		justifyContent: 'center',
		paddingRight: wp('1.5%'),
	},
	adText: {
		color: ColorEnum.Yellow,
		fontSize: fontSizes.big,
	},
	itemButton: {
	},
	getText: {
		textAlign: 'right',
		color: ColorEnum.White,
		fontSize: fontSizes.medium,
		...commonStyles.bold
	},
	removeBottomBannerIcon: {
		color: ColorEnum.LightGray,
	},
	removeBottomBannerText: {
		color: ColorEnum.LightGray,
		fontSize: fontSizes.tiny,
	},
	plusImage: {
		height: wp('5%'), width: wp('5%')
	},
	pencilImage: {
		height: wp('6%'), width: wp('6%')
	}
});

export default GetPencilModal;