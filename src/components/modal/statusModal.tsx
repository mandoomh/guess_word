import React from 'react';
import {
	View,
	Image,
	StyleSheet,
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import CustomModal from '../util/customModal';
import CustomText from '../util/customText';
import { fontSizes, commonStyles, getOneDimensionalArrayFromTwoDimensionalArray, getAverageFromArray, getEmojiByAverage } from '../../constants';
import { AppThunkDispatch } from '../../store/mains/thunk';
import { ColorEnum } from '../../enums';
import { IRootState } from '../../interfaces';

const getCountOpenedClues = (oneDimensionalGuessWordIndexes: number[], openClues: number) => {
	return oneDimensionalGuessWordIndexes.filter(index => (index + 1) === openClues).length;
};

const getContent = (state: IRootState) => {
	const { wordGuessedIndexes } = state;

	const oneDimensionalGuessWordIndexes = getOneDimensionalArrayFromTwoDimensionalArray(wordGuessedIndexes);
	const guessWordIndexAverage = getAverageFromArray(oneDimensionalGuessWordIndexes);
	const emojiComponent = getEmojiByAverage(guessWordIndexAverage, titleStyles.emojiText, titleStyles.emojiImage);

	return (
		<View style={mainStyles.mainView}>
			<View style={titleStyles.containerView}>
				<CustomText text='Your Status' textStyle={titleStyles.text} />
				{emojiComponent}
			</View>

			<View style={mainStyles.paddingView} />

			<View style={linesStyles.containerView}>
				<View style={linesStyles.firstLineView}>
					<View style={linesStyles.leftView}>
						<Image
							source={{ uri: 'checkmark' }}
							style={imageStyles.checkmark}
						/>
						<Image
							source={{ uri: 'pencil' }}
							style={imageStyles.pencil}
						/>
						<CustomText text='Total Words' textStyle={linesStyles.secondText} />
					</View>
					<View style={linesStyles.rightView}>
						<CustomText text={oneDimensionalGuessWordIndexes.length.toString()} textStyle={linesStyles.rightText} />
					</View>
				</View>
				<View style={linesStyles.secondLineView}>
					<View style={linesStyles.secondLineInnerView}>
						<View style={linesStyles.leftView}>
							<Image
								source={{ uri: 'checklist' }}
								style={imageStyles.checlist}
							/>
							<Image
								source={{ uri: 'pencil' }}
								style={imageStyles.pencil}
							/>
							<CustomText text={`Average Clues\nOpened`} textStyle={linesStyles.secondText} />
						</View>
						<View style={linesStyles.rightView}>
							<CustomText text={guessWordIndexAverage.toString()} textStyle={linesStyles.rightText} />
						</View>
					</View>
					<View style={{ height: hp('14%') }}>
						<View style={linesStyles.clueView}>
							<View style={linesStyles.leftView}>
								<CustomText text='1 Clue' textStyle={linesStyles.clueTitleText} />
							</View>
							<View style={linesStyles.rightView}>
								<CustomText text={getCountOpenedClues(oneDimensionalGuessWordIndexes, 1).toString()} textStyle={linesStyles.clueValueText} />
							</View>
						</View>
						<View style={linesStyles.clueView}>
							<View style={linesStyles.leftView}>
								<CustomText text='2 Clues' textStyle={linesStyles.clueTitleText} />
							</View>
							<View style={linesStyles.rightView}>
								<CustomText text={getCountOpenedClues(oneDimensionalGuessWordIndexes, 2).toString()} textStyle={linesStyles.clueValueText} />
							</View>
						</View>
						<View style={linesStyles.clueView}>
							<View style={linesStyles.leftView}>
								<CustomText text='3 Clues' textStyle={linesStyles.clueTitleText} />
							</View>
							<View style={linesStyles.rightView}>
								<CustomText text={getCountOpenedClues(oneDimensionalGuessWordIndexes, 3).toString()} textStyle={linesStyles.clueValueText} />
							</View>
						</View>
						<View style={linesStyles.clueView}>
							<View style={linesStyles.leftView}>
								<CustomText text='4 Clues' textStyle={linesStyles.clueTitleText} />
							</View>
							<View style={linesStyles.rightView}>
								<CustomText text={getCountOpenedClues(oneDimensionalGuessWordIndexes, 4).toString()} textStyle={linesStyles.clueValueText} />
							</View>
						</View>
					</View>
				</View>
			</View>
		</View>
	);
};

interface IProps {
	dispatch: AppThunkDispatch;
	isOpen: boolean;
	state: IRootState;
}

const StatusModal = (props: IProps) => {
	const { dispatch, state, isOpen } = props;

	return (
		<CustomModal
			isOpen={isOpen}
			content={getContent(state)}
			dispatch={dispatch}
			useCloseButton={true}
		/>
	);
};

const mainStyles = StyleSheet.create({
	mainView: {
		backgroundColor: ColorEnum.Gray, width: wp('90%'), padding: wp('7%'), ...commonStyles.buttonRadius
	},
	paddingView: {
		height: wp('7%'),
	},
});

const titleStyles = StyleSheet.create({
	containerView: {
		height: hp('4%'), paddingHorizontal: wp('1%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start',
	},
	text: {
		color: ColorEnum.Pink, fontSize: fontSizes.big, ...commonStyles.bold
	},
	emojiText: {
		fontSize: fontSizes.huge, marginLeft: wp('1.5%')
	},
	emojiImage: {
		height: hp('4.5%'), width: hp('4.5%'), resizeMode: 'contain', marginLeft: wp('2%')
	},
});

const borderBottom = {
	borderBottomWidth: 1, borderBottomColor: ColorEnum.LightGray,
};

const linesStyles = StyleSheet.create({
	containerView: {
		paddingHorizontal: wp('4%'), borderWidth: 1, borderColor: ColorEnum.LightGray, ...commonStyles.buttonRadius
	},
	firstLineView: {
		height: hp('9%'), paddingVertical: hp('2.5%'), paddingHorizontal: wp('1%'), flexDirection: 'row', ...borderBottom
	},
	secondLineView: {
		height: hp('25%'), paddingVertical: hp('2.5%'), paddingHorizontal: wp('1%'),
	},
	secondLineInnerView: {
		height: hp('6%'), flexDirection: 'row'
	},
	leftView: {
		flex: 2, flexDirection: 'row', alignItems: 'center',
	},
	rightView: {
		flex: 1, justifyContent: 'center', alignItems: 'flex-end',
	},
	icon: {
		color: ColorEnum.Pink,
	},
	text: {
		color: ColorEnum.Yellow, fontSize: fontSizes.medium, ...commonStyles.bold
	},
	secondText: {
		color: ColorEnum.Yellow, fontSize: fontSizes.medium, ...commonStyles.bold, marginLeft: wp('3%')
	},
	rightText: {
		color: ColorEnum.White, fontSize: fontSizes.medium, ...commonStyles.bold
	},
	clueView: {
		flex: 1, flexDirection: 'row'
	},
	clueTitleText: {
		color: ColorEnum.LightGray, fontSize: fontSizes.small, paddingLeft: wp('10.5%'), ...commonStyles.bold
	},
	clueValueText: {
		color: ColorEnum.White, fontSize: fontSizes.small,
	},
});

const imageStyles = StyleSheet.create({
	checkmark: {
		height: wp('5%'), width: wp('5%'), marginRight: wp('5%')
	},
	checlist: {
		height: wp('5%'), width: wp('5%'), marginRight: wp('5%')
	},
	pencil: {
		height: wp('6%'), width: wp('6%')
	},
});

export default StatusModal;