import React from 'react';
import {
	View,
	Image,
	NativeSyntheticEvent,
	NativeTouchEvent,
	StyleSheet,
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import CustomButton from '../util/customButton';
import CustomModal from '../util/customModal';
import CustomText from '../util/customText';
import { fontSizes, commonStyles, playSound } from '../../constants';
import { AppThunkDispatch, hideModal, openHint } from '../../store/mains/thunk';
import { ColorEnum, UpdatingPencilCount, SoundEffect } from '../../enums';

const getContent = (dispatch: AppThunkDispatch) => {
	return (
		<View style={mainStyles.mainView}>
			<View style={textStyles.containerView}>
				<View style={textStyles.titleView}>
					<CustomText text={'Show me one letter.'} textStyle={textStyles.title} />
				</View>
				<View style={textStyles.textView}>
					<Image
						source={{ uri: 'pencil' }}
						style={textStyles.image}
					/>
					<CustomText text={(UpdatingPencilCount.OpenHint).toString()} textStyle={textStyles.text} />
				</View>
			</View>
			<View style={buttonStyles.containerView}>
				<View style={buttonStyles.cancelView}>
					<CustomButton
						content={(
							<View>
								<CustomText
									text={`No,\nThanks.`}
									textStyle={buttonStyles.cancelText}
								/>
							</View>
						)}
						onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
							playSound(SoundEffect.TouchButton);
							dispatch(hideModal());
						}}
						buttonStyle={buttonStyles.cancelButton}
					/>
				</View>
				<View style={buttonStyles.okView}>
					<CustomButton
						content={(
							<View>
								<CustomText
									text={`Yes,\nPlease.`}
									textStyle={buttonStyles.okText}
								/>
							</View>
						)}
						onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
							dispatch(openHint());
						}}
						buttonStyle={buttonStyles.okButton}
					/>
				</View>
			</View>
		</View>
	);
};

interface IProps {
	dispatch: AppThunkDispatch;
	isOpen: boolean;
}

const HintModal = (props: IProps) => {
	const { dispatch, isOpen } = props;

	return (
		<CustomModal isOpen={isOpen} content={getContent(dispatch)} dispatch={dispatch} useCloseButton={false}/>
	);
};

const mainStyles = StyleSheet.create({
	mainView: {
		backgroundColor: ColorEnum.Gray,
		width: wp('84%'),
		height: wp('72%'),
		...commonStyles.buttonRadius
	},
});

const textStyles = StyleSheet.create({
	containerView: {
		height: wp('30%')
	},
	titleView: {
		flex: 1,
		...commonStyles.alignCenter,
	},
	textView: {
		flex: 1,
		flexDirection: 'row',
		...commonStyles.alignCenter,
	},
	title: {
		color: ColorEnum.Yellow,
		fontSize: fontSizes.big,
		...commonStyles.bold,
		paddingTop: wp('4%'),
	},
	text: {
		color: ColorEnum.Yellow,
		fontSize: fontSizes.big,
	},
	image: {
		height: hp('4.8%'), width: hp('4.8%'), right: wp('1%')
	},
});

const buttonStyles = StyleSheet.create({
	containerView: {
		height: wp('42%'),
		flexDirection: 'row',
	},
	cancelView: {
		flex: 1,
		paddingTop: wp('2%'),
		paddingRight: wp('2%'),
		paddingBottom: wp('6%'),
		paddingLeft: wp('6%'),
	},
	okView: {
		flex: 1,
		paddingTop: wp('2%'),
		paddingRight: wp('6%'),
		paddingBottom: wp('6%'),
		paddingLeft: wp('2%'),
	},
	cancelButton: {
	},
	cancelText: {
		color: ColorEnum.LightGray,
		fontSize: fontSizes.big,
		...commonStyles.bold,
	},
	okButton: {
		borderRadius: 500,
		backgroundColor: ColorEnum.Pink
	},
	okText: {
		color: ColorEnum.Gray,
		fontSize: fontSizes.big,
		...commonStyles.bold,
	},
});

export default HintModal;