import React from 'react';
import {
	View,
	StyleSheet,
	NativeSyntheticEvent,
	NativeTouchEvent
} from 'react-native';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import SlideDownView from './util/slideDownView';
import { AppThunkDispatch, movePage } from '../store/mains/thunk';
import { ColorEnum, PageEnum, SoundEffect } from '../enums';
import { commonStyles, fontSizes, commonBorderWidth, playSound } from '../constants';
import CustomButton from './util/customButton';
import CustomText from './util/customText';

interface IProps {
	dispatch: AppThunkDispatch;
}

const Menu = (props: IProps) => {
	const handleOnPressStart = (ev: NativeSyntheticEvent<NativeTouchEvent>) => {
		playSound(SoundEffect.TouchButton);
		props.dispatch(movePage(PageEnum.Stage));
	};

	return (
		<SlideDownView
			content={(
				<>
					<View style={styles.viewTop} />
					<View style={styles.viewTitle}>
						<CustomText text='Guess Word.' textStyle={styles.title} />
					</View>
					<View style={styles.viewMiddle} />
					<View style={styles.viewButton}>
						<CustomButton
							content={<CustomText text='Game Start' textStyle={styles.buttonText} />}
							onPress={handleOnPressStart}
							buttonStyle={styles.buttonStyle}
						/>
					</View>
					<View style={styles.viewBottom} />
				</>
			)}
		/>
	);
};

const styles = StyleSheet.create({
	viewTop: {
		flex: 3,
	},
	viewMiddle: {
		flex: 1,
	},
	viewBottom: {
		flex: 4,
	},
	viewTitle: {
		flex: 1, ...commonStyles.alignCenter
	},
	viewButton: {
		flex: 1, ...commonStyles.alignCenter
	},
	title: {
		fontSize: fontSizes.huge, color: ColorEnum.Yellow, ...commonStyles.bold
	},
	buttonText: {
		color: ColorEnum.Pink, fontSize: fontSizes.big, ...commonStyles.bold
	},
	buttonStyle: {
		borderColor: ColorEnum.Pink, borderWidth: commonBorderWidth, padding: hp('2%'), ...commonStyles.buttonRadius
	},
});

export default Menu;
