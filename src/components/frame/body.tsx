import React, { useEffect, useState } from 'react';
import {
	StyleSheet,
	View,
} from 'react-native';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { PageEnum } from '../../enums';
import { IRootState } from '../../interfaces';
import Header from './header';
import Bottom from './bottom';
import Menu from '../menu';
import Stage from '../stage';
import Game from '../game';
import StageClear from '../stageClear';
import MainModal from '../modal/mainModal';
import Debug from '../util/debug';
import Loading from '../util/loading';
import Help from '../util/help';
import { AppThunkDispatch } from '../../store/mains/thunk';
import { ColorEnum } from '../../enums';
import { isInitializeAdMob } from '../../constants/admob';

const getBackgroundColor = (page: PageEnum) => {
	switch (page) {
		case PageEnum.Menu:
			return ColorEnum.Gray;
		case PageEnum.Stage:
			return ColorEnum.Yellow;
		case PageEnum.Game:
			return ColorEnum.Gray;
		case PageEnum.StageClear:
			return ColorEnum.Gray;
		default:
			return ColorEnum.Gray;
	}
};

const getCurrentComponent = (state: IRootState, dispatch: AppThunkDispatch, now: number) => {
	switch (state.currentPage) {
		case PageEnum.Menu:
			return <Menu dispatch={dispatch} />;
		case PageEnum.Stage:
			return <Stage dispatch={dispatch} state={state} />;
		case PageEnum.Game:
			return <Game dispatch={dispatch} state={state} now={now} />;
		case PageEnum.StageClear:
			return <StageClear dispatch={dispatch} state={state} />;
		default:
			return null;
	}
};

interface IProps {
	dispatch: AppThunkDispatch;
	state: IRootState;
}

const Body = (props: IProps) => {
	const { dispatch, state } = props;
	const { currentPage, isLoading, isShowHelp } = state;

	useEffect(() => {
		// 동의 여부는 언제든지 바뀔 수 있으므로 항상 앱 실행 시마다 동의 체크 필요.
		isInitializeAdMob(dispatch);
	}, []);

	const [now, setNow] = useState(new Date().getTime());

	useEffect(() => {
		const timeout = setTimeout(() => {
			setNow(new Date().getTime());
		}, 1000);

		return () => {
			clearTimeout(timeout);
		};
	}, [now]);

	const isHideBottom = currentPage === PageEnum.StageClear;

	return isShowHelp ? (
		<Help dispatch={dispatch} />
	) : (
			<View style={{ ...styles.view, backgroundColor: getBackgroundColor(currentPage) }}>
				<Header dispatch={dispatch} state={state} now={now} />

				<View style={styles.body}>
					{getCurrentComponent(state, dispatch, now)}
				</View>

				<View style={{ ...styles.bottomView, ...(isHideBottom && { height: 0 }) }}>
					<Bottom state={state} isHideBottom={isHideBottom} />
				</View>

				<Debug dispatch={dispatch} state={state} />

				<MainModal dispatch={dispatch} state={state} />

				<Loading isLoading={isLoading} />
			</View>
		);
};

const styles = StyleSheet.create({
	view: {
		flex: 1
	},
	bottomView: {
		height: hp('8%')
	},
	body: {
		height: hp('83%')
	}
});

export default Body;