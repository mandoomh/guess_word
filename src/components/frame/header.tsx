import React, { useMemo, useCallback, useState, useEffect } from 'react';
import { View, Image, StyleSheet, GestureResponderEvent } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import AnimationText from '../util/animationText';
import AnimationView from '../util/animationView';
import Timer from '../util/timer';
import { AppThunkDispatch, movePage, showGetPencilModal, showSettingModal, showStatusModal, getGiftPencilBonus } from '../../store/mains/thunk';
import { IRootState } from '../../interfaces';
import { ColorEnum, PageEnum, SoundEffect } from '../../enums';
import {
	commonStyles, fontSizes, fastUpdatingPencilInterval, getOneDimensionalArrayFromTwoDimensionalArray,
	getAverageFromArray, getEmojiByAverage, playSound
} from '../../constants';
import CustomText from '../util/customText';
import CustomButton from '../util/customButton';

interface IProps {
	dispatch: AppThunkDispatch;
	state: IRootState;
	now: number;
}

const Header = (props: IProps) => {
	const { dispatch, state, now } = props;
	const { currentPage, pencilCount, currentStageIndex, currentWordIndex, wordGuessedIndexes, pencilComponentKey,
		pencilIncreased, nextGetPencilDate } = state;

	const [timerComponentKey, setTimerComponentKey] = useState(0);

	const remainingTime = nextGetPencilDate - now;
	const isReadyToGet = remainingTime < 0;

	useEffect(() => {
		const timeout = setTimeout(() => {
			// 선물을 받을 준비가 되면 사용자에게 알려주기 위해 element를 흔들어 준다.
			if (isReadyToGet) {
				setTimerComponentKey(timerComponentKey + 1);
			}
		}, 3000);

		return () => {
			clearTimeout(timeout);
		};
	}, [isReadyToGet, timerComponentKey]);

	const goStage = useCallback((ev: GestureResponderEvent) => {
		playSound(SoundEffect.TouchButton);
		dispatch(movePage(PageEnum.Stage));
	}, []);

	const openSetting = useCallback((ev: GestureResponderEvent) => {
		dispatch(showSettingModal());
	}, []);

	const openStatusModal = useCallback((ev: GestureResponderEvent) => {
		dispatch(showStatusModal());
	}, []);

	const openGetPencilModal = useCallback((ev: GestureResponderEvent) => {
		playSound(SoundEffect.TouchButton);
		dispatch(showGetPencilModal());
	}, []);

	// 간혹 발생하거나 무거운 작업은 성능을 위해 memo 사용.
	const emojiComponent = useMemo(() => {
		const oneDimensionalGuessWordIndexes = getOneDimensionalArrayFromTwoDimensionalArray(wordGuessedIndexes);
		const guessWordIndexAverage = getAverageFromArray(oneDimensionalGuessWordIndexes);
		return getEmojiByAverage(guessWordIndexAverage, styles.averageText, styles.averageImage);
	}, [wordGuessedIndexes]);

	const isShow = [PageEnum.Stage, PageEnum.Game].includes(currentPage);

	const isStage = currentPage === PageEnum.Stage;

	const menuColor = isStage ? ColorEnum.Gray : ColorEnum.LightGray;

	const imageSrouce = isStage ? 'setting' : 'home';

	return (
		<View>
			<View style={styles.marginTopView} />
			<View style={styles.container}>
				{isShow ? (
					<>
						<View style={styles.leftPart}>
							<CustomButton
								content={
									<Image source={{ uri: imageSrouce }} style={styles.homeImage} />
								}
								onPress={isStage ? openSetting : goStage}
								buttonStyle={styles.homeButton}
							/>
							<CustomButton
								content={(
									<View style={styles.emojiView}>
										{emojiComponent}
									</View>
								)}
								onPress={openStatusModal}
								buttonStyle={styles.emojiButton}
							/>
							<AnimationView
								componentKey={timerComponentKey}
								animation={isReadyToGet ? 'pulse' : ''}
								duration={700}
								iterationCount={2}
								content={(
									<Timer
										innerViewStyle={styles.giftInnerView}
										textStyle={{ color: menuColor }}
										pencilStyle={styles.giftPencil}
										remainingTime={remainingTime}
										buttonStyle={styles.giftButton}
										onTouch={() => { dispatch(getGiftPencilBonus()); }}
									/>
								)}
								viewStyle={styles.giftView}
							/>
						</View>

						<View style={styles.rightPart}>
							<View style={{ ...styles.fourthItem, ...([PageEnum.Menu, PageEnum.Stage].includes(currentPage) && commonStyles.hidden) }}>
								<CustomText text={`${currentStageIndex + 1}-${currentWordIndex + 1}`} textStyle={{ ...styles.textProgress, color: menuColor }} />
							</View>
							<CustomButton
								content={
									<View style={styles.pencilView}>
										<View>
											<Image source={{ uri: 'pencil' }} style={styles.pencilImage} />
										</View>
										<View style={styles.pencilInnerView}>
											<AnimationText
												componentKey={pencilComponentKey}
												animation={pencilIncreased ? 'slideInUp' : 'slideInDown'}
												duration={fastUpdatingPencilInterval}
												text={pencilCount.toLocaleString('en')}
												textStyle={{ ...styles.textPencilCount, color: menuColor }}
											/>
										</View>
									</View>
								}
								onPress={openGetPencilModal}
								buttonStyle={styles.pencilButton}
							/>
						</View>
					</>
				) : null}
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	marginTopView: {
		height: hp('5%'),
	},
	container: {
		height: hp('4%'), flexDirection: 'row'
	},
	leftPart: {
		width: wp('60%'), flexDirection: 'row', justifyContent: 'flex-start'
	},
	rightPart: {
		width: wp('40%'), flexDirection: 'row', justifyContent: 'flex-end'
	},
	fourthItem: {
		width: wp('13%'), marginRight: wp('2%'), ...commonStyles.alignCenter
	},

	icon: {
	},
	averageText: {
		fontSize: fontSizes.huge,
	},
	averageImage: {
		height: hp('4%'),
		width: hp('4%'),
		resizeMode: 'contain'
	},
	textProgress: {
		fontSize: fontSizes.medium
	},
	textPencilCount: {
		fontSize: fontSizes.medium,
		left: wp('0.8%')
	},
	pencilImage: {
		height: hp('4%'),
		width: hp('4%'),
		resizeMode: 'contain'
	},
	homeButton: {
		flex: 0, marginLeft: wp('7%')
	},
	homeImage: {
		height: hp('4%'),
		width: hp('4%'),
		resizeMode: 'contain'
	},
	emojiButton: {
		flex: 0, marginLeft: wp('5%')
	},
	emojiView: {
		width: hp('4%'), ...commonStyles.alignCenter
	},
	giftInnerView: {
		width: wp('29%'), flexDirection: 'row',
	},
	giftView: {
		marginLeft: wp('5%'), alignItems: 'flex-start'
	},
	giftPencil: {
		height: hp('4%'), width: hp('3.52%'), resizeMode: 'contain'
	},
	giftButton: {
		alignItems: 'flex-start'
	},
	pencilButton: {
		flex: 0
	},
	pencilView: {
		width: wp('18%'), marginRight: wp('7%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'
	},
	pencilInnerView: {
		justifyContent: 'center', alignItems: 'flex-start'
	},
});

export default Header;