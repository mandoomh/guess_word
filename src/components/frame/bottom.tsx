import React from 'react';
import {
	View,
	StyleSheet,
} from 'react-native';

import { IRootState } from '../../interfaces';
import { getIsAllowToShowAdvertToUser, getIsAllowNonPersonalizedAdsOnly } from '../../constants/admob';
import { getIsShowAds } from '../../constants';
import CustomBannerAd from '../util/bannerAd';

interface IProps {
	state: IRootState;
	isHideBottom: boolean;
}

const Bottom = (props: IProps) => {
	const { state, isHideBottom } = props;
	const { isNeedConsent, consentStatus, hideAdsDate } = state;

	// 동의 여부 등은 변경될 일이 거의 없으므로 memo 사용.
	const { isShowBanner, showNonPersonalizedAdsOnly } = React.useMemo(() => {
		const isAllowToShowAdvertToUser = getIsAllowToShowAdvertToUser(isNeedConsent, consentStatus);
		const showNonPersonalizedAdsOnly = getIsAllowNonPersonalizedAdsOnly(isNeedConsent, consentStatus);
		const isShowBanner = isAllowToShowAdvertToUser && getIsShowAds(hideAdsDate);

		return { isShowBanner, showNonPersonalizedAdsOnly };
	}, [isNeedConsent, consentStatus, hideAdsDate]);

	return (
		<View style={styles.container}>
			<CustomBannerAd isShow={isShowBanner && !isHideBottom} showNonPersonalizedAdsOnly={showNonPersonalizedAdsOnly} />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1, justifyContent: 'flex-end'
	},
});

export default Bottom;