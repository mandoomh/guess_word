import React, { useEffect, useState, useCallback } from 'react';
import {
	View,
	StyleSheet,
} from 'react-native';

import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import SlideDownView from './util/slideDownView';
import CustomText from './util/customText';
import { AppThunkDispatch, goNextWord, setLastEnterKeyDate } from '../store/mains/thunk';
import { IRootState } from '../interfaces';
import {
	commonStyles, getWord, getClues, getInputs, fontSizes, playSound, timer, getIsTimePassesAfterEnterKey,
	isCurrentWordAlreadGuessed
} from '../constants';
import Clue from './game/clue';
import Input from './game/input';
import Keyboard from './game/keyboard';
import { ColorEnum, SoundEffect } from '../enums';

// currentInputLetters가 앱 설치 후 최초 실행 시에는 빈 배열이다.
const isInitialRun = (currentInputLetters: string[]) => {
	return currentInputLetters.length === 0;
};

const getUpdatedComponentKeys = (shakeComponentKeys: number[], clueIndex: number, numberToUpdate: number) => {
	const newShakeComponentKeys = [...shakeComponentKeys];
	newShakeComponentKeys[clueIndex + 1] = newShakeComponentKeys[clueIndex + 1] + numberToUpdate;
	return newShakeComponentKeys;
};

interface IProps {
	dispatch: AppThunkDispatch;
	state: IRootState;
	now: number;
}

const Game = (props: IProps) => {
	const { dispatch, state, now } = props;
	const { wordGuessedIndexes, currentStageIndex, currentWordIndex, currentClueIndex,
		language, currentHintIndexes, currentInputLetters, currentInputFocusIndex, lastEnterKeyDate,
	} = state;

	const isAlreadyGuessed = isCurrentWordAlreadGuessed(wordGuessedIndexes, currentStageIndex, currentWordIndex);

	const word = getWord(currentStageIndex, currentWordIndex, language);
	const clues = getClues(word.clues, currentClueIndex);
	const inputs = getInputs(word.answer, currentInputLetters, currentHintIndexes, currentInputFocusIndex, isAlreadyGuessed);

	// 0~3은 clues component의 key, 4는 hint component의 key.
	const [shakeComponentKeys, setShakeComponentKeys] = useState([0, 0, 0, 0, 0]);

	// componentDidMount.
	useEffect(() => {
		if (isInitialRun(currentInputLetters) || isAlreadyGuessed) {
			dispatch(goNextWord());
		}

		setShakeComponentKeys([0, 0, 0, 0, 0]);
		dispatch(setLastEnterKeyDate());
	}, []);

	const isTimePassesAfterEnterKey = getIsTimePassesAfterEnterKey(isAlreadyGuessed, now, lastEnterKeyDate);

	useEffect(() => {
		const timeout = setTimeout(async () => {
			if (isTimePassesAfterEnterKey) {
				shakeComponentIfNoInput();
			}
		}, 0);

		return () => {
			clearTimeout(timeout);
		};
	}, [isTimePassesAfterEnterKey]);

	// 일정 시간 동안 아무런 입력 없이 대기하고 있는 경우에 각 단서+힌트 순으로 element를 흔들어준다.
	const shakeComponentIfNoInput = useCallback(async () => {
		dispatch(setLastEnterKeyDate());
		playSound(SoundEffect.NoticeCluesAndHint);
		setShakeComponentKeys(getUpdatedComponentKeys(shakeComponentKeys, currentClueIndex, 1));
		await timer(300);
		playSound(SoundEffect.NoticeCluesAndHint);
		setShakeComponentKeys(getUpdatedComponentKeys(shakeComponentKeys, currentClueIndex, 2));
	}, [shakeComponentKeys, currentClueIndex]);

	return (
		<SlideDownView
			key={`${currentStageIndex}-${currentWordIndex}`}
			content={(
				<>
					<View style={styles.titleView}>
						<CustomText text='Guess Word.' textStyle={styles.title} />
					</View>

					<Clue dispatch={dispatch} clues={clues} currentClueIndex={currentClueIndex} shakeComponentKeys={shakeComponentKeys} />

					<Input dispatch={dispatch} inputs={inputs} answer={word.answer} isAlreadyGuessed={isAlreadyGuessed} />

					<Keyboard dispatch={dispatch} state={state} inputs={inputs} answer={word.answer} isAlreadyGuessed={isAlreadyGuessed} shakeComponentKeys={shakeComponentKeys} />
				</>
			)}
		/>
	);
};

const styles = StyleSheet.create({
	containerView: {
		flex: 1, color: ColorEnum.Yellow,
	},
	titleView: {
		height: hp('12%'), color: ColorEnum.Yellow, flexDirection: 'row',
		paddingVertical: hp('2%'), ...commonStyles.alignCenter
	},
	title: {
		fontSize: fontSizes.huge, color: ColorEnum.Yellow, ...commonStyles.bold
	},
	viewBottom: {
		flex: 1, color: ColorEnum.Yellow, ...commonStyles.alignCenter
	},
});

export default Game;