import React from 'react';
import {
	View,
	StyleSheet,
	Image,
	NativeSyntheticEvent, NativeTouchEvent
} from 'react-native';

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

import SlideDownView from './util/slideDownView';
import CustomText from './util/customText';
import CustomButton from './util/customButton';
import AnimationView from './util/animationView';
import { AppThunkDispatch, goNextWord } from '../store/mains/thunk';
import { IRootState } from '../interfaces';
import { commonStyles, fontSizes } from '../constants';
import { ColorEnum } from '../enums';

interface IProps {
	dispatch: AppThunkDispatch;
	state: IRootState;
}

const StageClear = (props: IProps) => {
	const { dispatch, state } = props;

	return (
		<SlideDownView
			content={(
				<>
					<View style={stageClearStyles.topView} />
					<View style={stageClearStyles.congratulationView}>
						<View style={congratulationStyles.topView}>
							<View style={congratulationStyles.topInnerView} />
							<AnimationView
								componentKey={1}
								animation={'bounceIn'}
								duration={1500}
								iterationCount={1}
								content={(
									<Image
										source={{ uri: 'congratulation_01' }}
										style={congratulationStyles.image}
									/>
								)}
								viewStyle={congratulationStyles.topImageView}
							/>
							<View style={congratulationStyles.topInnerView} />
						</View>
						<AnimationView
							componentKey={1}
							animation={'fadeIn'}
							duration={2000}
							iterationCount={1}
							content={(
								<CustomText text='Congratulation!' textStyle={congratulationStyles.text} />
							)}
							viewStyle={congratulationStyles.bottomView}
						/>
					</View>
					<View style={stageClearStyles.textView}>
						<View style={textStyles.containerView}>
							<AnimationView
								componentKey={1}
								animation={'bounceInLeft'}
								duration={1000}
								iterationCount={1}
								delay={500}
								content={(
									<CustomText
										text={`Stage ${(state.currentStageIndex + 1)} Clear.\nGo to the Next Stage.`}
										textStyle={textStyles.text}
									/>
								)}
								viewStyle={textStyles.leftView}
							/>
							<AnimationView
								componentKey={1}
								animation={'bounceInRight'}
								duration={1000}
								iterationCount={1}
								delay={500}
								content={(
									<View style={textStyles.buttonView}>
										<CustomButton
											content={<CustomText text='Next.' textStyle={textStyles.buttonText} />}
											onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
												dispatch(goNextWord());
											}}
											buttonStyle={{ ...textStyles.button }}
										/>
									</View>
								)}
								viewStyle={textStyles.rightView}
							/>
						</View>
						<View style={commonStyles.flexOne} />
					</View>
					<View style={stageClearStyles.bottomView}>
						<View style={bottomStyles.topView} />
						<AnimationView
							componentKey={1}
							animation={'bounce'}
							duration={500}
							iterationCount={3}
							delay={0}
							content={(
								<Image
									source={{ uri: 'congratulation_02' }}
									style={bottomStyles.image}
								/>
							)}
							viewStyle={bottomStyles.bottomView}
						/>
					</View>
				</>
			)}
		/>
	);
};

const stageClearStyles = StyleSheet.create({
	topView: {
		height: hp('17.5%')
	},
	congratulationView: {
		height: hp('25%')
	},
	textView: {
		height: hp('25%')
	},
	bottomView: {
		height: hp('25%')
	},
});

const congratulationStyles = StyleSheet.create({
	topView: {
		flex: 2,
		flexDirection: 'row'
	},
	topInnerView: {
		flex: 1,
	},
	topImageView: {
		flex: 3,
		...commonStyles.alignCenter
	},
	image: {
		height: '100%',
		width: '100%',
		resizeMode: 'contain'
	},
	bottomView: {
		flex: 1,
		...commonStyles.alignCenter
	},
	text: {
		textAlign: 'center',
		fontSize: fontSizes.huge,
		color: ColorEnum.Pink,
		...commonStyles.bold
	}
});

const textStyles = StyleSheet.create({
	containerView: {
		flex: 1,
		flexDirection: 'row',
		paddingTop: hp('3%'),
		paddingHorizontal: wp('8%'),
	},
	leftView: {
		flex: 2,
		...commonStyles.alignCenter
	},
	text: {
		textAlign: 'left',
		fontSize: fontSizes.big,
		color: ColorEnum.Yellow,
	},
	rightView: {
		flex: 1,
		...commonStyles.alignCenter
	},
	buttonView: {
		width: wp('24%'),
		height: wp('24%'),
	},
	button: {
		borderRadius: 100,
		backgroundColor: ColorEnum.Pink
	},
	buttonText: {
		color: ColorEnum.Gray,
		fontSize: fontSizes.big,
		...commonStyles.bold
	},
});

const bottomStyles = StyleSheet.create({
	topView: {
		flex: 2,
	},
	bottomView: {
		flex: 1,
		...commonStyles.alignCenter
	},
	image: {
		height: '100%',
		width: '100%',
		resizeMode: 'contain'
	},
});

export default StageClear;