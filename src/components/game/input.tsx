import React from 'react';
import {
	View,
	StyleSheet,
	NativeSyntheticEvent,
	NativeTouchEvent
} from 'react-native';
import FlashMessage from 'react-native-flash-message';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import AnimationView from '../util/animationView';
import CustomButton from '../util/customButton';
import CustomText from '../util/customText';
import { AppThunkDispatch, focusInput } from '../../store/mains/thunk';
import { IInputLetter } from '../../interfaces';
import { commonStyles, fontSizes, BLANK, commonBorderWidth, isTakeScreenshot } from '../../constants';
import { ColorEnum, InputStatus } from '../../enums';

const blankInput: Pick<IInputLetter, 'input' | 'status'> = {
	input: BLANK, status: InputStatus.Blank
};

const getInputPerLine = (inputs: IInputLetter[]) => {
	const inputPerLine: IInputLetter[][] = [];

	let lineIndex = 0;
	inputs.forEach(input => {
		if (input.status === InputStatus.Blank) {
			lineIndex = lineIndex + 1;
		} else {
			if (!inputPerLine[lineIndex]) {
				inputPerLine.push([]);
			}
			inputPerLine[lineIndex].push(input);
		}
	});

	return inputPerLine;
};

/**
 * 줄 하나당 하나의 IInputLetter[]배열을 생성하여 반환한다. (알파벳 하나당 IInputLetter 1개)
 * 한 줄에는 최대 10자(공백 포함)까지만 표시하고 10자가 넘어가면 줄바꿈하여 배열로 추가로 만든다.
 * (예1) 'mouse'가 정답이면 [ IInputLetter[5] ]
 * (예2) 'visual studio'가 정답이면 'visual studio'는 공백 포함하여 10자가 넘으므로
 *       [ IInputLetter[6], IInputLetter[6] ]
 *       가 된다.
 *
 * @param inputs
 *
 * @return IInputLetter[][]
 */
const getThisInputs = (inputs: IInputLetter[]) => {
	const inputPerLine = getInputPerLine(inputs);

	const maxInputsPerLine: IInputLetter[][] = [];

	const maxLengthPerLine = 10;
	let lineIndex = 0;
	inputPerLine.forEach(input => {
		if (!maxInputsPerLine[lineIndex]) {
			maxInputsPerLine[lineIndex] = input;
		} else {
			const thisLineInputs = maxInputsPerLine[lineIndex];
			if (thisLineInputs.length + input.length <= maxLengthPerLine) {
				const lastInput = thisLineInputs[thisLineInputs.length - 1];
				const newBlankInput: IInputLetter = { ...blankInput, letterIndex: lastInput.letterIndex + 1 };
				maxInputsPerLine[lineIndex] = thisLineInputs.concat([newBlankInput]).concat(input);
			} else {
				maxInputsPerLine[++lineIndex] = input;
			}
		}
	});

	return maxInputsPerLine;
};

interface IProps {
	dispatch: AppThunkDispatch;
	inputs: IInputLetter[];
	answer: string;
	isAlreadyGuessed: boolean;
}

const Input = (props: IProps) => {
	const { dispatch, inputs, answer, isAlreadyGuessed } = props;

	const thisInputs = getThisInputs(inputs);

	return (
		<AnimationView
			componentKey={isAlreadyGuessed ? 1 : 0}
			animation={isAlreadyGuessed ? 'pulse' : ''}
			duration={800}
			iterationCount={'infinite'}
			content={(
				<>
					{thisInputs.map((lineInputs, lineIndex) => {
						return (
							<View key={`inputLine-${lineIndex}`} style={styles.viewInputLine}>
								{lineInputs.map(inputItem => {
									const thisInputButtonStyle = inputStyles[inputItem.status];

									return (
										<View key={`input-${inputItem.letterIndex}`} style={styles.viewInput}>
											<CustomButton
												content={
													<CustomText
														text={inputItem.status === InputStatus.Hinted ? answer[inputItem.letterIndex] : inputItem.input}
														textStyle={{ ...styles.buttonText, ...thisInputButtonStyle.text, ...(isAlreadyGuessed && isAlreadyGuessedStyle.text) }}
													/>
												}
												onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
													dispatch(focusInput(inputItem.letterIndex));
												}}
												buttonStyle={{ ...styles.button, ...thisInputButtonStyle.button, ...(isAlreadyGuessed && isAlreadyGuessedStyle.button) }}
												disabled={[InputStatus.Blank, InputStatus.Hinted].includes(inputItem.status) || isAlreadyGuessed}
												// disable touch animation.
												activeOpacity={1}
											/>
										</View>
									);
								})}
							</View>
						);
					})}

					<FlashMessage position={{ top: hp('6%') }} />
				</>
			)}
			viewStyle={styles.viewInputContainer}
		/>
	);
};

const styles = StyleSheet.create({
	viewInputContainer: {
		height: hp('19%'), color: ColorEnum.Yellow, paddingVertical: hp('2%'), ...commonStyles.alignCenter
	},
	viewInputLine: {
		height: hp('7.5%'),
		color: ColorEnum.Yellow,
		flexDirection: 'row',
		...commonStyles.alignCenter
	},
	viewInput: {
		paddingHorizontal: wp('0.75%'),
		...commonStyles.alignCenter
	},
	buttonText: {
		fontSize: fontSizes.big, ...commonStyles.bold
	},
	button: {
		marginVertical: hp('0.75%'), width: wp('8%'), ...commonStyles.buttonRadius,
		borderWidth: isTakeScreenshot ? 0 : commonBorderWidth,
	},
});

const inputTextColor = ColorEnum.White;
const inputBackgroundColor = ColorEnum.Pink;
const inputBorderColor = ColorEnum.Pink;

const inputStyles = {
	[InputStatus.None]: StyleSheet.create({
		text: {
			color: inputTextColor,
		},
		button: {
			backgroundColor: inputBackgroundColor,
			borderColor: inputBorderColor,
		}
	}),
	[InputStatus.Focused]: StyleSheet.create({
		text: {
			color: inputTextColor,
		},
		button: {
			backgroundColor: inputBackgroundColor,
			borderColor: ColorEnum.Yellow,
		}
	}),
	[InputStatus.Hinted]: StyleSheet.create({
		text: {
			color: ColorEnum.White,
		},
		button: {
			backgroundColor: ColorEnum.Gray,
			borderColor: ColorEnum.Gray,
		}
	}),
	[InputStatus.Blank]: StyleSheet.create({
		text: {
			color: inputTextColor,
		},
		button: {
			backgroundColor: ColorEnum.Gray,
			borderColor: ColorEnum.Gray,
			width: wp('2%')
		}
	}),
};

const isAlreadyGuessedStyle = StyleSheet.create({
	text: {
		color: ColorEnum.Pink,
		fontSize: fontSizes.huge
	},
	button: {
		backgroundColor: ColorEnum.Gray,
		borderColor: ColorEnum.Gray,
	}
});

export default Input;