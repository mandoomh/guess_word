import React, { useState, useEffect } from 'react';
import {
	View, Image, StyleSheet, NativeSyntheticEvent, NativeTouchEvent, Alert
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as Animatable from 'react-native-animatable';
import { faRocket, faBackspace } from '@fortawesome/free-solid-svg-icons';

import CustomButton from '../util/customButton';
import CustomText from '../util/customText';
import AnimationView from '../util/animationView';
import Icon from '../util/icon';
import { AppThunkDispatch, showOpenHintModal, deleteInput, insertInput, goNextWord, moveToStageClear, deleteAllInput } from '../../store/mains/thunk';
import {
	commonStyles, fontSizes, getUnhintedLettersCount, commonBorderWidth, stages, fastUpdatingPencilInterval,
	getIsAllInputsFilled, getIsInputsCorrect
} from '../../constants';
import { ColorEnum, UpdatingPencilCount } from '../../enums';
import { IRootState, IInputLetter } from '../../interfaces';

enum KeyTypeEnum {
	Alphabet = 'Alphabet',
	Delete = 'Del',
	Hint = 'Hint',
}

const keyboards = [
	['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
	['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'],
	[KeyTypeEnum.Hint, 'Z', 'X', 'C', 'V', 'B', 'N', 'M', KeyTypeEnum.Delete]
];

const getKey = (keyName: string, inputs: IInputLetter[], dispatch: AppThunkDispatch, shakeComponentKeys: number[], isInputCorrect: boolean) => {
	if (keyName === KeyTypeEnum.Hint) {
		const keyStyle = keyStyles[KeyTypeEnum.Hint];
		const isHintKeyDisabled = getUnhintedLettersCount(inputs) <= 1;
		return (
			<AnimationView
				componentKey={shakeComponentKeys[4]}
				animation={shakeComponentKeys[4] === 0 ? '' : 'bounce'}
				duration={300}
				iterationCount={1}
				content={(
					<CustomButton
						content={<Icon iconType={faRocket} iconSize={wp('7%')} iconStyle={keyStyle.icon} />}
						onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
							if (isHintKeyDisabled) {
								Alert.alert(
									'No More Hint Available',
									'Guess the last letter for yourself.',
									[{ text: 'OK' }],
								);
							} else {
								dispatch(showOpenHintModal());
							}
						}}
						disabled={isInputCorrect}
						buttonStyle={{ ...styles.button, ...keyStyle.button }}
					/>
				)}
				viewStyle={{}}
			/>
		);
	} else if (keyName === KeyTypeEnum.Delete) {
		const keyStyle = keyStyles[KeyTypeEnum.Delete];
		return (
			<CustomButton
				content={<Icon iconType={faBackspace} iconSize={wp('8%')} iconStyle={keyStyle.icon} />}
				onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
					dispatch(deleteInput());
				}}
				onLongPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
					dispatch(deleteAllInput());
				}}
				buttonStyle={{ ...styles.button, ...keyStyle.button }}
				disabled={isInputCorrect}
			/>
		);
	} else {
		const keyStyle = keyStyles[KeyTypeEnum.Alphabet];
		return (
			<CustomButton
				content={<CustomText text={keyName} textStyle={{ ...styles.buttonText, ...keyStyle.text }} />}
				onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
					dispatch(insertInput(keyName));
				}}
				buttonStyle={{ ...styles.button, ...keyStyle.button }}
				disabled={isInputCorrect}
			/>
		);
	}
};

// 단어를 맞춘 후 해당 화면에서 다음 단어로 넘어가지 않고 바로 종료할 경우에는 currentWordIndex가 이전 값일 수 있다.
// 따라서 현재 해당 스테이지가 종료된 상황인지 체크한다.
const getIsCurrentStageCleared = (wordGuessedIndexes: number[][]) => {
	const lastGuessedStageIndex = wordGuessedIndexes.length - 1;
	return stages[lastGuessedStageIndex].length === wordGuessedIndexes[lastGuessedStageIndex].length;
};

const getCongratulationView = (dispatch: AppThunkDispatch, wordGuessedIndexes: number[][]) => {
	const isCurrentStageCleared = getIsCurrentStageCleared(wordGuessedIndexes);
	const buttonText = isCurrentStageCleared ? `Stage\nClear.` : 'Next.';

	return (
		<AnimationView
			componentKey={1}
			animation={'pulse'}
			duration={300}
			iterationCount={3}
			delay={200}
			content={(
				<>
					<Animatable.View animation='bounceIn' style={congratulationStyles.leftView}>
						<View style={congratulationStyles.leftInnerView}>
							<CustomText
								text='Great!'
								textStyle={congratulationStyles.leftTopText}
							/>
							<View style={congratulationStyles.pencilView}>
								<View>
									<Image source={{ uri: 'pencil' }} style={congratulationStyles.pencilImage} />
								</View>
								<View style={{ marginLeft: wp('1%') }}>
									<CustomText
										text={`+${UpdatingPencilCount.GuessWord}`}
										textStyle={congratulationStyles.leftBottomText}
									/>
								</View>
							</View>
						</View>
					</Animatable.View>
					<Animatable.View animation='bounceIn' style={congratulationStyles.rightView}>
						<View style={congratulationStyles.rightInnerView}>
							<CustomButton
								content={<CustomText text={buttonText} textStyle={{ ...congratulationStyles.rightText }} />}
								onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
									if (isCurrentStageCleared) {
										dispatch(moveToStageClear());
									} else {
										dispatch(goNextWord());
									}
								}}
								buttonStyle={{ ...congratulationStyles.rightButton }}
							/>
						</View>
					</Animatable.View>
				</>
			)}
			viewStyle={congratulationStyles.containerView}
		/>
	);
};

const getKeyboardView = (dispatch: AppThunkDispatch, inputs: IInputLetter[], shakeComponentKeys: number[], isInputCorrect: boolean) => {
	return (
		<>
			{keyboards.map((line, lineIndex) => {
				return (
					<View key={`keyboardLine-${lineIndex}`} style={keyLineStyles.common}>
						{line.map((keyName, keyIndex) => {
							return (
								<View key={`keyboard-${lineIndex}-${keyIndex}`} style={styles.viewKey}>
									{getKey(keyName, inputs, dispatch, shakeComponentKeys, isInputCorrect)}
								</View>
							);
						})}
					</View>
				);
			})}
		</>
	);
};

interface IProps {
	dispatch: AppThunkDispatch;
	state: IRootState;
	inputs: IInputLetter[];
	isAlreadyGuessed: boolean;
	shakeComponentKeys: number[];
	answer: string;
}

const Keyboard = (props: IProps) => {
	const { dispatch, state, inputs, isAlreadyGuessed, shakeComponentKeys, answer } = props;
	const { wordGuessedIndexes } = state;

	const [wordGuessedKey, setWordGuessedKey] = useState(0);

	const isInputCorrect = getIsAllInputsFilled(inputs) && getIsInputsCorrect(inputs, answer);

	useEffect(() => {
		const timeout = setTimeout(async () => {
			setWordGuessedKey(isAlreadyGuessed ? 1 : 0);
		}, isAlreadyGuessed ? fastUpdatingPencilInterval : 0);

		return () => {
			clearTimeout(timeout);
		};
	}, [isAlreadyGuessed]);

	return (
		<View style={styles.viewKeyboardContainer}>
			{wordGuessedKey === 1 ?
				getCongratulationView(dispatch, wordGuessedIndexes)
				:
				getKeyboardView(dispatch, inputs, shakeComponentKeys, isInputCorrect)
			}
		</View>
	);
};

const styles = StyleSheet.create({
	viewKeyboardContainer: {
		height: hp('22%'), color: ColorEnum.Yellow, paddingBottom: hp('2%')
	},
	viewKey: {
		height: hp('7%'),
		marginHorizontal: wp('1%'),
		...commonStyles.alignCenter
	},
	buttonText: {
		fontSize: fontSizes.medium, ...commonStyles.bold
	},
	button: {
		marginVertical: hp('0.5%'),
		width: wp('8%'), borderWidth: commonBorderWidth
	},
});

const congratulationStyles = StyleSheet.create({
	containerView: {
		flex: 1, flexDirection: 'row'
	},
	leftView: {
		width: wp('50%'),
		justifyContent: 'center',
		textAlign: 'right',
		alignItems: 'flex-end',
	},
	leftInnerView: {
		paddingRight: wp('5%')
	},
	leftTopText: {
		color: ColorEnum.Yellow,
		fontSize: fontSizes.huge,
		...commonStyles.bold
	},
	pencilView: {
		flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: hp('1%')
	},
	pencilImage: {
		height: hp('4.8%'), width: hp('4.8%')
	},
	leftBottomText: {
		color: ColorEnum.Yellow,
		fontSize: fontSizes.big,
	},
	rightView: {
		width: wp('50%'),
		justifyContent: 'center',
	},
	rightInnerView: {
		marginLeft: wp('5%'),
		width: wp('24%'),
		height: wp('24%'),
	},
	rightButton: {
		borderRadius: 500,
		backgroundColor: ColorEnum.Pink
	},
	rightText: {
		color: ColorEnum.Gray,
		fontSize: fontSizes.big,
		...commonStyles.bold
	},
});

const keyLineStyles = StyleSheet.create({
	common: {
		height: hp('7%'),
		color: ColorEnum.Yellow,
		flexDirection: 'row',
		...commonStyles.alignCenter
	},
});

const keyStyles = {
	[KeyTypeEnum.Alphabet]: StyleSheet.create({
		text: {
			color: ColorEnum.White,
		},
		button: {
			backgroundColor: ColorEnum.Gray,
			borderColor: ColorEnum.White,
			borderWidth: commonBorderWidth,
			...commonStyles.buttonRadius
		}
	}),
	[KeyTypeEnum.Delete]: StyleSheet.create({
		text: {
			color: ColorEnum.Gray,
		},
		button: {
			backgroundColor: ColorEnum.Gray,
			borderColor: ColorEnum.Gray,
			width: wp('8.5%'),
		},
		icon: {
			color: ColorEnum.LightGray,
		},
	}),
	[KeyTypeEnum.Hint]: StyleSheet.create({
		text: {
			color: ColorEnum.Pink,
		},
		button: {
			backgroundColor: ColorEnum.Gray,
			borderColor: ColorEnum.Yellow,
			width: wp('12%'),
			borderWidth: commonBorderWidth,
			...commonStyles.buttonRadius
		},
		icon: {
			color: ColorEnum.Yellow,
		},
	}),
};

export default Keyboard;