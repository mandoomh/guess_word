import React from 'react';
import {
	View,
	Image,
	StyleSheet,
	NativeSyntheticEvent,
	NativeTouchEvent
} from 'react-native';

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

import CustomButton from '../util/customButton';
import AnimationView from '../util/animationView';
import CustomText from '../util/customText';
import { AppThunkDispatch, openClue } from '../../store/mains/thunk';
import { IClue } from '../../interfaces';
import { commonStyles, fontSizes, commonBorderWidth, isTakeScreenshot } from '../../constants';
import { ColorEnum, ClueStatus, UpdatingPencilCount } from '../../enums';

const getDynamicFontSize = (clueLength: number) => {
	// 28자 이상이 되면 화면에서 글자가 잘릴 가능성이 있다.
	if (clueLength >= 28) {
		return fontSizes.small;
	} else {
		return fontSizes.medium;
	}
};

interface IProps {
	dispatch: AppThunkDispatch;
	clues: IClue[];
	currentClueIndex: number;
	shakeComponentKeys: number[];
}

const Clue = (props: IProps) => {
	const { dispatch, clues, currentClueIndex, shakeComponentKeys } = props;

	return (
		<View style={styles.viewClueContainer}>
			<View style={styles.viewClueInnerContainer}>
				{clues.map((clueItem, clueIndex) => {
					const thisClueButtonStyle = clueStyles[clueItem.status];
					const isClosed = clueItem.status === ClueStatus.Closed;

					return (
						<View key={`line-${clueIndex}`} style={styles.viewClue}>
							<View style={styles.viewSide} />
							<AnimationView
								componentKey={shakeComponentKeys[clueIndex]}
								animation={shakeComponentKeys[clueIndex] === 0 ? '' : 'bounce'}
								duration={300}
								iterationCount={1}
								content={(
									<CustomButton
										content={
											<>
												<View>
													{/* 닫혀있는 경우에는 텍스트를 표시하면 안된다. 새로 단어가 열릴 때 닫혀있는 경우에도 순간적으로 텍스트가 표시될 수 있다. 먼저 컴포넌트를 만들고 그 다음에 스타일을 적용하게 되는데 그 시차 동안 표시되는 것으로 보인다. */}
													<CustomText text={isClosed ? '' : clueItem.clue} textStyle={{ ...styles.buttonText, ...thisClueButtonStyle.text, fontSize: getDynamicFontSize(clueItem.clue.length) }} />
												</View>
												{isClosed && currentClueIndex + 1 === clueIndex && !isTakeScreenshot ? (
													<View style={pencilStyles.outerView}>
														<View style={pencilStyles.innerView}>
															<Image source={{ uri: 'pencil' }} style={pencilStyles.image} />
															<CustomText text={(UpdatingPencilCount.OpenClue).toString()} textStyle={pencilStyles.text} />
														</View>
													</View>
												) : null}
											</>
										}
										onPress={(ev: NativeSyntheticEvent<NativeTouchEvent>) => {
											if (isClosed) {
												dispatch(openClue(false));
											}
										}}
										buttonStyle={{ ...styles.button, ...thisClueButtonStyle.button }}
										disabled={currentClueIndex + 1 !== clueIndex}
									/>
								)}
								viewStyle={styles.viewCenter}
							/>
							<View style={styles.viewSide} />
						</View>
					);
				})}
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	viewClueContainer: {
		height: hp('28%'),
	},
	viewClueInnerContainer: {
		flex: 1,
	},
	viewClue: {
		flex: 1, flexDirection: 'row', ...commonStyles.alignCenter
	},
	viewSide: {
		flex: 1
	},
	viewCenter: {
		flex: 4,
		height: hp('7%')
	},
	buttonText: {
		...commonStyles.bold
	},
	button: {
		marginVertical: hp('0.5%'), paddingHorizontal: wp('2%'), borderWidth: commonBorderWidth, ...commonStyles.buttonRadius
	},
});

const pencilStyles = StyleSheet.create({
	outerView: {
		height: '100%', width: '100%', position: 'absolute', ...commonStyles.alignCenter
	},
	innerView: {
		width: '100%', flexDirection: 'row', ...commonStyles.alignCenter
	},
	image: {
		height: hp('3.5%'), width: hp('3.5%'), right: wp('0.7%')
	},
	text: {
		color: ColorEnum.Gray, fontSize: fontSizes.bigSmall
	},
});

const clueStyles = {
	[ClueStatus.Opened]: StyleSheet.create({
		text: {
			color: ColorEnum.White, textAlign: 'center'
		},
		button: {
			backgroundColor: ColorEnum.Gray,
			borderColor: ColorEnum.Yellow,
		}
	}),
	[ClueStatus.Closed]: StyleSheet.create({
		text: {
			color: ColorEnum.Yellow,
		},
		button: {
			backgroundColor: ColorEnum.Yellow,
			borderColor: ColorEnum.Yellow,
		}
	}),
};

export default Clue;