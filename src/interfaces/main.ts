import { Product } from 'react-native-iap';
import { Animation } from 'react-native-animatable';

import { LanguageEnum, PageEnum, StageStatus, ClueStatus, InputStatus, ModalType, ConsentStatus, UpdatingPencilCount } from '../enums';

// 단어가 바뀔 때마다 초기화가 필요한 항목들.
export interface IInitialStateEachWord {
	currentClueIndex: number;
	currentInputFocusIndex: number;
	// 현재 펼쳐진 힌트 위치들.
	currentHintIndexes: number[];
	modalToShow: ModalType;
	lastEnterKeyDate: number;
}

export interface IRootState extends IInitialStateEachWord {
	language: LanguageEnum;
	isSoundOn: boolean;
	isMusicOn: boolean;
	currentStageIndex: number;
	currentWordIndex: number;
	pencilCount: number;
	currentPage: PageEnum;
	currentInputLetters: string[];
	// wordGuessedIndexes: 각 문제를 맞힌 단서 위치: 진도 체크 및 통계용.
	wordGuessedIndexes: number[][];
	pencilComponentKey: number;
	pencilIncreased: boolean;
	isNeedConsent: boolean;
	consentStatus: ConsentStatus;
	isLoading: boolean;
	iapProducts: IapProduct[];
	// hiddenDateAds: milliseconds로 저장. Date object로 저정하면 표시 형식이 다양하지므로 Date로 저장하지 않는다.
	hideAdsDate: number;
	nextGetPencilDate: number;
	isShowHelp: boolean;
}

// 정답을 한글자씩 관리.
export interface IClueAnimation {
	key: number;
	animationType: Animation | '';

}

export interface IInputLetter {
	input: string;
	letterIndex: number;
	status: InputStatus;
}

export interface IWord {
	answer: string;
	clues: string[];
}

export interface IWords {
	[LanguageEnum.English]: IWord;
}

export interface IStage {
	stageIndex: number;
	status: StageStatus;
}

export interface IClue {
	clue: string;
	status: ClueStatus;
}

export interface INextStageWordIndex {
	nextStageIndex: number;
	nextWordIndex: number;
}

export interface ILocalIapProduct {
	productId: string;
	localizedPrice: string;
	pencilBonus: UpdatingPencilCount;
	weeksHideAds: number;
	handleOnPress: () => void;
}

export type IServerIapProduct = Product<string>;
export interface IUnitedIapProduct extends ILocalIapProduct, IServerIapProduct { }

export type IapProduct = ILocalIapProduct | IUnitedIapProduct;

/***** IOS *****/
// "latest_receipt_info":
// {
// 	"is_trial_period":"false",
// 	"original_purchase_date":"2020-12-17 13:21:33 Etc/GMT",
// 	"original_purchase_date_ms":"1608211293000",
// 	"original_purchase_date_pst":"2020-12-17 05:21:33 America/Los_Angeles",
// 	"original_transaction_id":"1000000755785231",
// 	"product_id":"com.onemounttobaek.guessword.seconditem",
// 	"purchase_date":"2020-12-17 13:21:33 Etc/GMT",
// 	"purchase_date_ms":"1608211293000",
// 	"purchase_date_pst":"2020-12-17 05:21:33 America/Los_Angeles",
// 	"quantity":"1",
// 	"transaction_id":"1000000755785231"
// },
export interface IReceiptInfo {
	product_id?: string;	// ios
	productId?: string;		// android
}

/***** IOS *****/
// "environment": "Sandbox",
// "latest_receipt": "MIIT2AYJKoZIhvcNAQcCoIITyTCCE8UCAQExCzAJBgUrDgMCGgUAMIIDeQYJKoZIhvcNAQcBoIIDagSCA2YxggNiMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgEDAgEBBAMMATEwCwIBCwIBAQQDAgEAMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDAIBDgIBAQQEAgIAizANAgENAgEBBAUCAwIjqDANAgETAgEBBAUMAzEuMDAOAgEJAgEBBAYCBFAyNTYwGAIBBAIBAgQQAF4OwkFtm2asUzei1jyIJzAbAgEAAgEBBBMMEVByb2R1Y3Rpb25TYW5kYm94MBwCAQUCAQEEFP176wOiMQwHjdgEx4/nqxX2hDZaMB4CAQwCAQEEFhYUMjAyMC0xMi0xN1QxMzowNDoyN1owHgIBEgIBAQQWFhQyMDEzLTA4LTAxVDA3OjAwOjAwWjAmAgECAgEBBB4MHGNvbS5vbmVtb3VudHRvYmFlay5ndWVzc3dvcmQwNQIBBwIBAQQtNQDqMZ9HAk2BZ7oJX2yUDgtj8oX8Vkc4X6YOyEq2ii/PZ3P6TWQ0rkdbc/BGME0CAQYCAQEERRx6PwfRYOduHR8ljgG0O2E4d/DU57nGUWRfTwiNV6+qnqEVFwKAFQuwxQD1S6CylQPYRmtqEeJ/+VxOb1dYACyc0WyRBTCCAWsCARECAQEEggFhMYIBXTALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBATAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwNzU1Nzc1NTYwMBsCAgapAgEBBBIMEDEwMDAwMDA3NTU3NzU1NjAwHwICBqgCAQEEFhYUMjAyMC0xMi0xN1QxMzowNDoxNFowHwICBqoCAQEEFhYUMjAyMC0xMi0xN1QxMzowNDoxNFowMQICBqYCAQEEKAwmY29tLm9uZW1vdW50dG9iYWVrLmd1ZXNzd29yZC5maXJzdGl0ZW2ggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAJF8CgA7AcqphtdcSIpYMQol/PW30vWZgUGaneq2Hij4Mg+zYGaK875dNgtTKKpxQhaGjJFgdjUJyVl2YctC5y2bk1N5rAFB6IkxZo9Hghx6w5jgycbNrhZQC4Nc5rA9LfHuwNafbN/g76zRM1kysVg6OMd69VJuTUoNUw7399ieio4gM3xK1QThQFBB/+Q8TFnU+KpJnNacVYl0DIRhN2Ce1UZ990bCs0hIuJ0RSvAzibvutge6ETEQHZ+Z+1huVJEbPDOGLo4sm0dFmEJq+tZ0xEhQjx3QfnagBXwD5KFuWwHPGcX4cVywYl1N39Hcxd61z0vNjnrSLDOAzHlcQc0=",
// "latest_receipt_info": [{
// 	"is_trial_period": "false",
// 	"original_purchase_date": "2020-12-17 13:04:14 Etc/GMT",
// 	"original_purchase_date_ms": "1608210254000",
// 	"original_purchase_date_pst": "2020-12-17 05:04:14 America/Los_Angeles",
// 	"original_transaction_id": "1000000755775560",
// 	"product_id": "com.onemounttobaek.guessword.firstitem",
// 	"purchase_date": "2020-12-17 13:04:14 Etc/GMT",
// 	"purchase_date_ms": "1608210254000",
// 	"purchase_date_pst": "2020-12-17 05:04:14 America/Los_Angeles",
// 	"quantity": "1",
// 	"transaction_id": "1000000755775560"
// }],
// "receipt": {
// 	"adam_id": 0,
// 	"app_item_id": 0,
// 	"application_version": "1",
// 	"bundle_id": "com.onemounttobaek.guessword",
// 	"download_id": 0,
// 	"in_app": [
// 		[Object]
// 	],
// 	"original_application_version": "1.0",
// 	"original_purchase_date": "2013-08-01 07:00:00 Etc/GMT",
// 	"original_purchase_date_ms": "1375340400000",
// 	"original_purchase_date_pst": "2013-08-01 00:00:00 America/Los_Angeles",
// 	"receipt_creation_date": "2020-12-17 13:04:14 Etc/GMT",
// 	"receipt_creation_date_ms": "1608210254000",
// 	"receipt_creation_date_pst": "2020-12-17 05:04:14 America/Los_Angeles",
// 	"receipt_type": "ProductionSandbox",
// 	"request_date": "2020-12-17 13:04:27 Etc/GMT",
// 	"request_date_ms": "1608210267013",
// 	"request_date_pst": "2020-12-17 05:04:27 America/Los_Angeles",
// 	"version_external_identifier": 0
// },
export interface IReceiptResponseIOS {
	environment: string;
	status: number;
	latest_receipt_info: IReceiptInfo[];
}

export type AnimationType = Animation | '';