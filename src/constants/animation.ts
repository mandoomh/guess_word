// milliseconds.
export const fastUpdatingPencilInterval = 100;
export const slowUpdatingPencilInterval = 200;
export const hideLoadingTimeout = 10000;

export const clueAndHintShakeInterval = 20 * 1000;