import { Platform } from 'react-native';
import { InterstitialAd, AdEventType, TestIds } from '@react-native-firebase/admob';

import { AppThunkDispatch, showLoading, hideLoading } from '../store/mains/thunk';
import { ConsentStatus } from '../enums';
import { getIsAllowToShowAdvertToUser, isInitializeAdMob } from './admob';
import { log } from './log';
import { playMusic, stopMusic } from './sound';

// @todo rewarded ad와 공유하는 코드가 많으니 하나로 통합하면 좋을 듯...

const myAdUnitId = __DEV__ ? TestIds.INTERSTITIAL : (Platform.OS === 'ios' ? 'ca-app-pub-7668923106539535/4421819919' : 'ca-app-pub-7668923106539535/8439097725');
const testDevices = __DEV__ ? ['EMULATOR'] : [];

export const showInterstitialAd = async (dispatch: AppThunkDispatch) => {
	const { isNeedConsent, consentStatus } = await isInitializeAdMob(dispatch);

	log.debug(`showInterstitialAd > BEFORE START / __DEV__: ${__DEV__} / isNeedConsent: ${isNeedConsent} / consentStatus: ${consentStatus}`);
	if (!getIsAllowToShowAdvertToUser(isNeedConsent, consentStatus)) {
		return;
	}

	try {
		const requestNonPersonalizedAdsOnly = isNeedConsent && consentStatus === ConsentStatus.NON_PERSONALIZED;

		log.debug(`showInterstitialAd > START / adUnitId: ${myAdUnitId} / testDevices: ${testDevices} / requestNonPersonalizedAdsOnly: ${requestNonPersonalizedAdsOnly}`);

		const interstitialAd = InterstitialAd.createForAdRequest(myAdUnitId, {
			testDevices,
			requestNonPersonalizedAdsOnly
		});

		interstitialAd.onAdEvent((type, error, reward) => {
			dispatch(hideLoading());

			if (type === AdEventType.LOADED) {
				log.debug(`showInterstitialAd > LOADED`);
				stopMusic();
				dispatch(showLoading());
				interstitialAd.show();
			} else if (type === AdEventType.CLOSED) {
				log.debug(`showInterstitialAd > CLOSED`);
				playMusic();
			}

			if (error) {
				playMusic();
				log.error(`showInterstitialAd.onAdEvent > error`, error);
			}
		});

		// Start loading the rewarded ad straight away
		interstitialAd.load();
	} catch (error) {
		log.error(`showInterstitialAd > error`, error);
	}
};