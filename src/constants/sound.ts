import Sound from 'react-native-sound';

import { SoundEffect, SoundBgm } from '../enums';
import { log } from './log';

// 소리 on/off값의 변경 여부를 알기 위해서는 항상 호출하는 측으로부터 on/off 여부를 전달 받아야 하는데 코드가 너무 단순반복적으로 중복이 된다.
// 따라서 main.tsx에서 on/off의 변경 여부를 subscribe하여 여기에 항상 최신값을 저장하도록 한다.
let isMusicOn = false;
let isSoundOn = false;

export const setIsMusicOn = (newIsMusicOn: boolean) => {
	if (isMusicOn !== newIsMusicOn) {
		isMusicOn = newIsMusicOn;
		// 음악은 항상 on/off가 변경되면 재생을 시도한다.
		playMusic();
	}
};

export const setIsSoundOn = (newIsSoundOn: boolean) => {
	isSoundOn = newIsSoundOn;
};

// 사운드 아이템은 하나씩만 생성하여 재사용한다.
const sounds: { [key in SoundEffect]?: Sound } = {};

const initializeSoundItem = (item: SoundEffect) => {
	const soundItem = new Sound(item, Sound.MAIN_BUNDLE, (error) => {
		if (error) {
			log.error(`[${item}] failed to load the  sound: `, error);
			return;
		}
		sounds[item as SoundEffect] = soundItem;

		playSound(item);
	});
};

export const playSound = (item: SoundEffect) => {
	if (isSoundOn) {
		if (sounds[item]) {
			sounds[item]?.stop();
			sounds[item]?.play((success) => {
				if (!success) {
					log.error(`[${item}] playback failed due to audio decoding errors`);
				}
			});
		} else {
			initializeSoundItem(item as SoundEffect);
		}
	}
};

let music: Sound | null = null;

const _playMusic = () => {
	// 무한 반복 재생.
	// 소리의 길이가 너무 짧으면 무한재생이 안되는 듯도 하다. 2,3초 짜리는 안되고 1분 24초 짜리는 잘 되었음.
	// 혹시 무한재생이 안되면 아래 play()함수의 onEnd callback에서 다시 플레이를 시켜주면 무한재생이 가능하다.
	music?.setNumberOfLoops(-1);

	music?.play((success) => {
		if (!success) {
			log.error(`[${music}] playback failed due to audio decoding errors`);
		}
	});
};

export const playMusic = () => {
	if (isMusicOn) {
		if (music) {
			music.stop();
			_playMusic();
		} else {
			const musicItem = new Sound(SoundBgm.Bgm01, Sound.MAIN_BUNDLE, (error) => {
				if (error) {
					log.error(`[${musicItem}] failed to load the  sound: `, error);
					return;
				}
				music = musicItem;

				_playMusic();
			});
		}
	} else {
		if (music) {
			music.stop();
		}
	}
};

export const stopMusic = () => {
	if (music) {
		music.stop();
	}
};