import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';

export const commonStyles = StyleSheet.create({
	alignCenter: {
		alignItems: 'center', justifyContent: 'center',
	},
	bold: {
		fontWeight: '600',
	},
	hidden: {
		height: 0, width: 0
	},
	flexOne: {
		flex: 1
	},
	flexZero: {
		flex: 0
	},
	buttonRadius: {
		borderRadius: 4
	}
});

export const commonBorderWidth = 2;

// 기기 사이즈마다 폰트 사이즈를 조정해서 사용.
const getRelativeFontSize = (size: number) => {
	const standardHeight = 640;
	return RFValue(size, standardHeight);
};

export const fontSizes = {
	emoticon: getRelativeFontSize(36),
	huge: getRelativeFontSize(24),
	big: getRelativeFontSize(18),
	medium: getRelativeFontSize(15),
	bigSmall: getRelativeFontSize(13),
	small: getRelativeFontSize(12),
	tiny: getRelativeFontSize(11),
};