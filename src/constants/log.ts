// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore: Could not find a declaration file for module 'react-native-log-level'.
import createLogger from 'react-native-log-level';

interface ILog {
	trace(message?: any, ...optionalParams: any[]): void;
	debug(message?: any, ...optionalParams: any[]): void;
	info(message?: any, ...optionalParams: any[]): void;
	warn(message?: any, ...optionalParams: any[]): void;
	error(message?: any, ...optionalParams: any[]): void;
}

/**
 * log.trace('hi')
 * log.debug('hi')
 * log.info('hi', obj)
 * log.warn('hi', obj)
 * log.error('hi')
 */
export const log: ILog = createLogger({
	level: __DEV__ ? 'info' : 'info'
});