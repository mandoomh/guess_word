import { Alert, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';

import { LanguageEnum, ClueStatus, InputStatus } from '../enums';
import { IClue, INextStageWordIndex, IInputLetter } from '../interfaces';
import { clueAndHintShakeInterval } from '../constants';
import { stages } from './stages';

export const BLANK = ' ';

export const CLUE_LENGTH = 4;

export const hasNextWord = (stageIndex: number, wordIndex: number) => {
	return !!stages[stageIndex] && !!stages[stageIndex][wordIndex];
};

export const getWord = (stageIndex: number, wordIndex: number, language: LanguageEnum) => {
	return stages[stageIndex][wordIndex][language];
};

export const getNextStageWordIndex = (wordGuessedIndexes: number[][]): INextStageWordIndex => {
	let nextStageIndex = 0;
	let nextWordIndex = 0;

	if (wordGuessedIndexes.length > 0) {
		nextStageIndex = wordGuessedIndexes.length - 1;
		nextWordIndex = wordGuessedIndexes[nextStageIndex].length;

		if (isStageCleared(nextStageIndex, nextWordIndex)) {
			nextStageIndex = nextStageIndex + 1;
			nextWordIndex = 0;
		}
	}

	return { nextStageIndex, nextWordIndex };
};

const isStageCleared = (nextStageIndex: number, nextWordIndex: number) => {
	return !stages[nextStageIndex][nextWordIndex];
};

const getClueStatus = (clueIndex: number, currentClueIndex: number) => {
	if (clueIndex <= currentClueIndex) {
		return ClueStatus.Opened;
	} else {
		return ClueStatus.Closed;
	}
};

export const getClues = (clues: string[], currentClueIndex: number): IClue[] => {
	return clues.map((clue, clueIndex) => {
		return { clue, status: getClueStatus(clueIndex, currentClueIndex) };
	});
};

export const getInitialInputLetters = (stageIndex: number, wordIndex: number, language: LanguageEnum): string[] => {
	const word = getWord(stageIndex, wordIndex, language);

	const answerLength = word.answer.length;

	return Array(answerLength).fill('');
};

export const getInputs = (answer: string, currentInputLetters: string[], currentHintIndexes: number[], currentInputFocusIndex: number, isAlreadyGuessed: boolean): IInputLetter[] => {
	// 정답을 맞춘 경우에는 입력값 대신 정답으로 표시.
	const inputLettes = isAlreadyGuessed ? answer.split('') : currentInputLetters;

	return inputLettes.map((inputLetter, index) => {
		let inputStatus = InputStatus.None;

		if (answer[index] === BLANK) {
			inputStatus = InputStatus.Blank;
		} else if (currentHintIndexes.includes(index)) {
			inputStatus = InputStatus.Hinted;
		} else if (index === currentInputFocusIndex) {
			inputStatus = InputStatus.Focused;
		}

		return { input: inputLetter, letterIndex: index, status: inputStatus };
	});
};

// 힌트로 사용될 수 있는 index들만 반환. (기존 힌트 및 공백 제외)
export const getPossibleHintIndexes = (answer: string, currentHintIndexes: number[]) => {
	const possibleHintIndexes: number[] = [];
	answer.split('').forEach((char, index) => {
		if (!currentHintIndexes.includes(index) && char !== BLANK) {
			possibleHintIndexes.push(index);
		}
	});
	return possibleHintIndexes;
};

const isInputEntered = (input: IInputLetter) => {
	let isEntered = false;
	// 힌트와 공백은 사용자가 값을 입력하지 않아도 이미 채워져 있는 값으로 판단한다.
	if ([InputStatus.Hinted, InputStatus.Blank].includes(input.status)) {
		isEntered = true;
	} else {
		isEntered = !!input.input;
	}

	return isEntered;
};

export const getIsAllInputsFilled = (inputs: IInputLetter[]) => {
	return inputs.findIndex(input => {
		return !isInputEntered(input);
	}) === -1;
};

const isInputCorrects = (input: IInputLetter, answer: string) => {
	if ([InputStatus.Hinted, InputStatus.Blank].includes(input.status)) {
		return true;
	} else {
		if (input.input === answer[input.letterIndex]) {
			return true;
		} else {
			return false;
		}
	}
};

export const getIsInputsCorrect = (inputs: IInputLetter[], answer: string) => {
	return inputs.findIndex(input => {
		return !isInputCorrects(input, answer);
	}) === -1;
};

// 정답 단어에서 힌트 처리가 되지 안은 개수.
export const getUnhintedLettersCount = (inputs: IInputLetter[]) => {
	return inputs.filter(input => {
		return ![InputStatus.Hinted, InputStatus.Blank].includes(input.status);
	}).length;
};

export const showMessage = (message: { title: string; message: string }) => {
	Alert.alert(message.title, message.message);
};

export const timer = (interval: number) => new Promise(resolve => setTimeout(resolve, interval));

export const getIsShowAds = (hideAdsDate: number) => {
	return hideAdsDate < new Date().getTime();
};

// 마지막 키 입력(단서 펼치기, 힌트 보기, 키 입력, 삭제 등) 후 일정 시간이 지났는지 여부를 반환.
export const getIsTimePassesAfterEnterKey = (isAlreadyGuessed: boolean, now: number, lastEnterKeyDate: number) => {
	if (isAlreadyGuessed) {
		return false;
	} else {
		return (lastEnterKeyDate + clueAndHintShakeInterval) <= now;
	}
};

// 단어를 맞춘 후 해당 화면에서 다음 단어로 넘어가지 않고 바로 종료할 경우에는 currentWordIndex가 이전 값일 수 있다.
export const isCurrentWordAlreadGuessed = (wordGuessedIndexes: number[][], currentStageIndex: number, currentWordIndex: number) => {
	return !!(wordGuessedIndexes[currentStageIndex] !== undefined && wordGuessedIndexes[currentStageIndex][currentWordIndex] !== undefined);
};

// 이모지가 안드로이드 27 버전 이하에서는 정상적으로 표시되지 않는 경우가 있다.
// 따라서 이런 경우에는 이모지 대신 이미지로 표시해야 한다.
let isEmojiSupported = true;
if (Platform.OS === 'android') {
	DeviceInfo.getApiLevel().then(apiLevel => {
		const emojiUnsupportedVersion = 27;
		if (apiLevel <= emojiUnsupportedVersion) {
			isEmojiSupported = false;
		}
	});
}

export const getIsEmojiSupported = () => {
	return isEmojiSupported;
};

export const alertMessage = {
	SomethingWrong: {
		title: 'Sorry',
		message: `Something wrong.\nPlease try again.`,
	},
	NoAdsNow: {
		title: 'Sorry',
		message: `There are currently no ads to display.`,
	},
	BadInternetConnection: {
		title: 'Bad internet connection',
		message: `Please check the internet connection and try again.`,
	},
	PurchaseCompleted: {
		title: 'Complete',
		message: `Purchasing pencils is completed.`,
	},
	PurchaseCanceled: {
		title: 'Cancel',
		message: `Purchasing pencils is canceled.`,
	},
	PurchaseFailed: {
		title: 'Sorry',
		message: `Purchasing pencils is failed.`,
	},
	InvalidReceipt: {
		title: 'Fail',
		message: `Purchasing pencils is failed.\nPlease try again.`,
	},
	InitAdConsent: {
		title: 'Complete',
		message: `Ad consent has been successfully initialized.`,
	},
	FailedToConnectToGoogleAd: {
		title: 'Sorry',
		message: `Cannot access the advertisement now.\nIf you are using vpn, please turn off vpn and try again.`,
	},
};

export const isTakeScreenshot = 0;