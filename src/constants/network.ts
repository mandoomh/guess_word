import NetInfo from '@react-native-community/netinfo';

import { showMessage, timer, alertMessage } from './util';

const getIsInternetReachable = async () => {
	let isInternetReachable = false;

	let netInfoState = null;

	const tryCount = 20;
	for (let i = 0; i < tryCount; i++) {
		netInfoState = await NetInfo.fetch();
		// netInfoState.isInternetReachable가 boolean으로 반환되어야 하는데 앱을 처음 수행하면 null로 반환이 된다.
		// 따라서 null이 아니라 boolean을 반환할 때까지 반복적으로 체크한다.
		if (typeof netInfoState.isInternetReachable === 'boolean') {
			isInternetReachable = netInfoState.isInternetReachable;
			break;
		}

		await timer(100);
	}

	return isInternetReachable;
};

export const runCallbackAfterInternetConnectionCheck = (callback: () => void) => {
	getIsInternetReachable()
		.then(isInternetReachable => {
			if (isInternetReachable) {
				callback();
			} else {
				showMessage(alertMessage.BadInternetConnection);
			}
		});
};