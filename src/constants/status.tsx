import React from 'react';
import { Image } from 'react-native';

// 2차원 배열들의 원소들을 모두 합쳐서 1차원 배열로 만든 후 반환.
export const getOneDimensionalArrayFromTwoDimensionalArray = (twoDimensionalArray: number[][]) => {
	return twoDimensionalArray.reduce(function (oneDimensionalArray, items, currentIndex, array) {
		items.forEach(item => { oneDimensionalArray.push(item); });
		return oneDimensionalArray;
	}, []);
};

/**
 * 맞춘 문제들을 맞출 시의 단서 개수들의 평균을 구한다.
 * (예: 1번문제를 2번째 단서에서 맞추고 2번 문제를 3번째 단서에서 맞추면 2.5)
 * 낮을수록 더 적은 단서들로 맞춘 것임.
 *
 * @param guessWordIndexes
 */
export const getAverageFromArray = (array: number[]) => {
	if (array.length > 0) {
		const sum = array.reduce((prev, current) => prev + current, 0);
		const average = (sum / array.length) || 0;

		// index는 0부터 시작하므로 기본값 1을 더해준다.
		const calibrationValue = 1;
		return parseFloat((calibrationValue + average).toFixed(2));
	} else {
		return 0;
	}
};

export const getEmojiByAverage = (average: number, textStyle: {}, imageStyle: {}) => {
	// 안드로이드 27 버전 이하에서는 정상적으로 표시되지 않는 이모지가 있다.
	// 따라서 이모지 대신 이미지로 표시하도록 한다.
	// 통일성을 위해 지원 여부에 따라 이모지와 이미지 중 선택하는 방식 대신,
	// 항상 이미지로 표시하는 방식을 택하였다.
	// const isEmojiSupported = false;
	// const emoji = ['🥳', '😎', '🤓', '🙄', '🧐', '🤭'];

	if (average <= 1.5) {
		return (
			<Image
				source={{ uri: 'emoji_01' }}
				style={imageStyle}
			/>
		);
	} else if (average <= 2) {
		return (
			<Image
				source={{ uri: 'emoji_02' }}
				style={imageStyle}
			/>
		);
	} else if (average <= 2.5) {
		return (
			<Image
				source={{ uri: 'emoji_03' }}
				style={imageStyle}
			/>
		);
	} else if (average <= 3) {
		return (
			<Image
				source={{ uri: 'emoji_04' }}
				style={imageStyle}
			/>
		);
	} else if (average <= 3.5) {
		return (
			<Image
				source={{ uri: 'emoji_05' }}
				style={imageStyle}
			/>
		);
	} else {
		return (
			<Image
				source={{ uri: 'emoji_06' }}
				style={imageStyle}
			/>
		);
	}
};