import { Platform, EmitterSubscription } from 'react-native';
import RNIap, {
	purchaseErrorListener,
	purchaseUpdatedListener,
	ProductPurchase,
	PurchaseError,
	getProducts,
	InAppPurchase,
	SubscriptionPurchase,
	IAPErrorCode
} from 'react-native-iap';

import { UpdatingPencilCount } from '../enums';
import { showMessage, alertMessage } from '../constants';
import { IapProduct, ILocalIapProduct, IServerIapProduct, IUnitedIapProduct, IReceiptResponseIOS, IReceiptInfo } from '../interfaces';
import { AppThunkDispatch, setIapProducts, hideLoading, hideModal, userGetsPencils, showLoading } from '../store/mains/thunk';
import { log } from './log';

const appSharedSecret = '555aa2b5ab4d420f9416d05cffc7edc8';

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
let purchaseUpdateSubscription: EmitterSubscription = null;
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
let purchaseErrorSubscription: EmitterSubscription = null;

// https://appstoreconnect.apple.com/apps/1542176282/appstore/addons?m= 에서 각 항목에 들어가면 제품 ID.
const productIds = Platform.select({
	ios: [
		'com.onemounttobaek.guessword.firstitem',
	],
	android: [
		'com.onemounttobaek.guessword.firstitem',
	],
}) as string[];

export const requestPurchase = (sku: string) => {
	try {
		RNIap.requestPurchase(sku, false);
	} catch (error) {
		log.error(`requestPurchase > error: `, error);
		throw new Error(alertMessage.SomethingWrong.message);
	}
};

export const localIapProducts: ILocalIapProduct[] = [{
	productId: productIds[0], localizedPrice: '$ 0.99', pencilBonus: UpdatingPencilCount.InAppPurchaseFirstItemBonus,
	weeksHideAds: 0, handleOnPress: () => { },
}];

const getIapProducts = (productsFromServer: IServerIapProduct[]): IapProduct[] => {
	return localIapProducts.map(localIapProduct => {
		const productFromServer = productsFromServer.find(serverProduct => localIapProduct.productId === serverProduct.productId);
		return { ...localIapProduct, ...(productFromServer && productFromServer) };
	});
};

const getIapProductsFromServer = (): Promise<IapProduct[]> => {
	let iapProducts: IapProduct[] = [...localIapProducts];

	return new Promise((resolve, reject) => {
		// getProducts()가 아마도 내부적으로 서버와 통신(?)하게 되어 있으므로 await를 사용하지 않도록 한다.
		getProducts(productIds)
			.then(productsFromServer => {
				iapProducts = getIapProducts(productsFromServer);
			})
			.catch(error => {
				log.error(`getIapProductsFromServer > err: `, error);	// standardized err.code and err.message available
			})
			.finally(() => {
				resolve(iapProducts);
			});
	});
};

export const getIsReadyIap = (iapProducts: IapProduct[]) => {
	// currency는 서버에서 읽어온 경우에만 존재하는 값이므로 currency가 있으면 서버에서 읽기가 성공한 것이다.
	return iapProducts.filter(iapProduct => (iapProduct as IUnitedIapProduct).currency).length > 0;
};

// 애플 서버에서 결제상품 정보 읽어오기.
export const prepareIapProducts = (dispatch: AppThunkDispatch) => {
	return getIapProductsFromServer()
		.then(iapProducts => {
			dispatch(setIapProducts(iapProducts));
			return Promise.resolve(iapProducts);
		});
};

const getReceiptResponseIOS = (verifyReceiptUrl: string, data: { 'receipt-data': string; password: string }): Promise<IReceiptResponseIOS> => {
	return fetch(verifyReceiptUrl, {
		method: 'POST', body: JSON.stringify(data),
	})
		.then(response => {
			return Promise.resolve(response.json());
		})
		.catch(error => log.error(`getVerifyResponse >> error: `, error));
};

const isDevReceipt = (verifyResponse: IReceiptResponseIOS) => {
	// 21007: This receipt is from the test environment,
	// but it was sent to the production environment for verification.
	return verifyResponse.status && verifyResponse.status === 21007;
};

const getPurchasedProductIds = async (receipt: string) => {
	if (Platform.OS === 'ios') {
		return getPurchasedProductIdsIOS(receipt);
	} else if (Platform.OS === 'android') {
		return getPurchasedProductIdsAndroid(receipt);
	} else {
		return [];
	}
};

const getPurchasedProductIdsAndroid = async (receipt: string) => {
	let validPurchases: IReceiptInfo[] = [];
	try {
		const availablePurchases = await RNIap.getAvailablePurchases();

		validPurchases = availablePurchases.filter(availablePurchase => availablePurchase.purchaseToken === receipt) as unknown as IReceiptInfo[];
	} catch (error) {
		validPurchases = [];
	}

	return validPurchases.map(validPurchase => validPurchase.productId);
};

const getPurchasedProductIdsIOS = async (receipt: string) => {
	const liveVerifyReceiptUrl = 'https://buy.itunes.apple.com/verifyReceipt';
	const devVerifyReceiptUrl = 'https://sandbox.itunes.apple.com/verifyReceipt';
	const data = { 'receipt-data': receipt, password: appSharedSecret };

	// 영수증 검증을 live url로 먼저 검증 후, 해당 요청에서 dev 영수증이라는 응답값이 오면 dev url로 재검증한다.
	// 애플에서 추천한 검증 방법이다.
	// Verify your receipt first with the production URL;
	// then verify with the sandbox URL if you receive a 21007 status code.
	// This approach ensures you do not have to switch between URLs while your application is tested,
	// reviewed by App Review, or live in the App Store.

	let verifyResponse = await getReceiptResponseIOS(liveVerifyReceiptUrl, data);

	if (isDevReceipt(verifyResponse)) {
		verifyResponse = await getReceiptResponseIOS(devVerifyReceiptUrl, data);
	}

	const receiptInfos = verifyResponse && verifyResponse.latest_receipt_info ? verifyResponse.latest_receipt_info : [];

	return receiptInfos.map(receiptInfo => receiptInfo.product_id);
};

export const handleOnReceiptVerified = (dispatch: AppThunkDispatch, purchasedProductId: string) => {
	const iapProduct = localIapProducts.find(iapProduct => iapProduct.productId === purchasedProductId);

	if (iapProduct) {
		dispatch(hideModal());
		dispatch(hideLoading());

		const { pencilBonus, weeksHideAds } = iapProduct;

		return dispatch(userGetsPencils(pencilBonus, weeksHideAds))
			.then(result => {
				return Promise.resolve(true);
			});
	} else {
		return Promise.resolve(false);
	}
};

// Iap 초기화: 결제 성공 시의 callback 생성. callback내에서 영수증 검증 및 결제 완료 처리 수행.
export const initIapConnection = (dispatch: AppThunkDispatch) => {
	try {
		RNIap.initConnection().then(() => {
			// we make sure that "ghost" pending payment are removed
			// (ghost = failed pending payment that are still marked as pending in Google's native Vending module cache)
			RNIap.flushFailedPurchasesCachedAsPendingAndroid().catch(() => {
				// exception can happen here if:
				// - there are pending purchases that are still pending (we can't consume a pending purchase)
				// in any case, you might not want to do anything special with the error
			}).then(() => {
				purchaseUpdateSubscription = purchaseUpdatedListener(async (purchase: InAppPurchase | SubscriptionPurchase | ProductPurchase) => {
					dispatch(showLoading());

					const receipt = Platform.OS === 'ios' ? purchase.transactionReceipt : purchase.purchaseToken as string;

					const purchasedProductIds = await getPurchasedProductIds(receipt);

					if (purchasedProductIds.length > 0) {
						(purchasedProductIds as string[]).forEach(purchasedProductId => {
							handleOnReceiptVerified(dispatch, purchasedProductId)
								.then(async (deliveryResult) => {
									if (deliveryResult) {
										// Tell the store that you have delivered what has been paid for.
										// Failure to do this will result in the purchase being refunded on Android and
										// the purchase event will reappear on every relaunch of the app until you succeed
										// in doing the below. It will also be impossible for the user to purchase consumables
										// again until you do this.
										// if (Platform.OS === 'ios') {
										// 	await RNIap.finishTransactionIOS(purchase.transactionId as string);
										// } else if (Platform.OS === 'android') {
										// 	// If consumable (can be purchased again)
										// 	await RNIap.consumePurchaseAndroid(purchase.purchaseToken as string);
										// 	// If not consumable
										// 	// await RNIap.acknowledgePurchaseAndroid(purchase.purchaseToken);
										// }

										// From react-native-iap@4.1.0 you can simplify above `method`. Try to wrap the statement with `try` and `catch` to also grab the `error` message.
										// If consumable (can be purchased again)

										try {
											await RNIap.finishTransaction(purchase, true);
											showMessage(alertMessage.PurchaseCompleted);
										} catch (error) {
										}

										// If not consumable
										// await RNIap.finishTransaction(purchase, false);
									} else {
										dispatch(hideLoading());
										// Retry / conclude the purchase is fraudulent, etc...
									}
								});
						});
					} else {
						// 영수증이 유효하지 않음: 서버와의 통신이 실패했을 가능성이 있을 듯.
						dispatch(hideLoading());
						showMessage(alertMessage.InvalidReceipt);
					}
				});

				// 구매 도중 사용자가 취소하는 경우에도 호출된다.
				purchaseErrorSubscription = purchaseErrorListener((error: PurchaseError) => {
					dispatch(hideLoading());
					if (error.code && error.code === IAPErrorCode.E_NETWORK_ERROR) {
						showMessage(alertMessage.BadInternetConnection);
					} else {
						showMessage(alertMessage.PurchaseCanceled);
					}
				});
			});
		});
	} catch (error) {
		dispatch(hideLoading());
	}
};

export const removeIapConnection = () => {
	if (purchaseUpdateSubscription) {
		purchaseUpdateSubscription.remove();
		// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
		// @ts-ignore
		purchaseUpdateSubscription = null;
	}
	if (purchaseErrorSubscription) {
		purchaseErrorSubscription.remove();
		// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
		// @ts-ignore
		purchaseErrorSubscription = null;
	}
};