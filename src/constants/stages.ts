import { IWords } from '../interfaces';
import { LanguageEnum } from '../enums';

// 긴 단어 길이 테스트 용.
// [LanguageEnum.English]: {
// 	answer: 'TRAFFIC JAM',
// 	answer: 'ROLLER COASTER',
// 	clues: ['Normally the tallest player\non the team in a basketball game', 'Containing personal information\nand usually a photograph', 'After Venus and before Mars', 'Main part of a town or city'],
// }

// 1개의 stage당 10개의 word를 포함하고 있어야 함.
export const stages: IWords[][] = [
	[{
		[LanguageEnum.English]: {
			answer: 'WATER',
			clues: ['Colorless and tasteless', 'Most of the human body', 'Liquid in a fountain', 'Melted ice'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'COIN',
			clues: ['Change, often', 'Metal', 'Small, flat and round', 'Money'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PIZZA',
			clues: ['Flat round in a square box', 'Delivery with toppings', 'Italian pie', 'Papa John\'s or Domino\'s __'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'JUMP',
			clues: ['Super Mario', 'Hurdle', 'Slam dunk', 'Trampoline'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'RAIN',
			clues: ['Drops from the sky', 'Rare weather in a desert', 'Umbrella', 'Water falling from clouds'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'NIGHT',
			clues: ['Dark', 'Time for sleep', 'Saying goodbye', 'After sunset'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'DOG',
			clues: ['Best friend', 'Animal with four legs', 'Common pet', 'Border Collie or Siberian Husky'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'GOLD',
			clues: ['Precious metal', 'Shiny yellow', 'Chemical element\nwith the symbol Au', 'Winner\'s medal\nat the Olympic Games'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'TODAY',
			clues: ['Modern times', 'Current', 'Present day', 'Not yesterday, not tomorrow'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'CUP',
			clues: ['Trophy;\nOften awarded for sporting events', 'With a handle, often', 'Small, round container', 'Used for drinking water, tea, etc.'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'KEY',
			clues: ['Escape room item', 'Very important', 'Ctrl, Alt or Delete', 'Lock opener'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'SPRING',
			clues: ['Symbol of regrowth', 'Source of water', 'Between winter and summer', 'Warmer season'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MODEL',
			clues: ['Good example', 'Role __', 'Person who wears fashionable clothes in fashion shows', 'Physical object\nto represent something'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MASK',
			clues: ['Bandit\'s item', 'For protection or evasion', 'COVlD-19 period accessory', 'Face covering'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'LAKE',
			clues: ['Water surrounded by land', 'Large pond', 'Swan __; Ballet', 'Baikal, Caspian Sea,\nSuperior or Victoria'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'TWELVE',
			clues: ['Noon or midnight', 'Number on a clock', 'Dozen', 'Number of months in a calendar'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'GAME',
			clues: ['Poker or Tetris', 'Zero-sum __', 'Entertaining activity', 'Chess or football'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'STAR',
			clues: ['Rating of a hotel or restaurant', 'Asterisk', 'The Sun', 'Celebrity'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'BUBBLE',
			clues: ['Dot-com __', 'Champagne', 'Soap', '__ gum'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'EYE',
			clues: ['__ of a storm', 'Hole in a needle', 'London __', 'One of the two organs for seeing'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'CURRY',
			clues: ['Spicy seasoning', 'Basketball star, "Stephen __"', 'Cooking powder', 'Indian dish'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MILK',
			clues: ['Cheese', 'Butter', 'White liquid which people drink', 'Cow'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'STRAWBERRY',
			clues: ['Juicy and red', 'Has small brown seeds on its surface', 'Jam', 'Shortcake fruit'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'BIRTHDAY',
			clues: ['Annual event', 'Personal celebration', 'Time for a cake and candles', 'Age anniversary'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'COFFEE',
			clues: ['From roasted beans', 'Dark color', 'Bitter drink', 'Caffeine'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'RIVER',
			clues: ['Water that moves', 'Usually flowing \ntowards an ocean, sea or lake', 'Very large stream', 'Amazon, Mississippi,\nThames or Yangtze'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'ALIBI',
			clues: ['Criminal defense', 'Was in another place', 'At the time it happened', 'Suspect\'s\n"I couldn\'t have done it!" story'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'ADDRESS',
			clues: ['Home or email', 'Street, district, city, and county', 'ZIP code', 'Place where someone lives'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'SHADOW',
			clues: ['It follows you', 'Dark area', 'Silhouette', 'Light being blocked'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'COMMUTE',
			clues: ['Regular journey', 'Round trip', 'Travel daily', 'Between work and home'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'EARTH',
			clues: ['Rotates once in about 24 hours', 'Before Mars', 'Third planet from the Sun', 'Where you currently are'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'SEVEN',
			clues: ['Most common roll of two dice', 'Snow White and the __ Dwarfs', 'Number of colors in a rainbow', 'Number of days in a week'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'GATE',
			clues: ['Suffix for a scandal', 'Door in a fence', 'Airport boarding point', 'Entrance'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'FREE',
			clues: ['Caffeine/sugar/tax __', 'Available', 'Not a prisoner any longer', 'Costing nothing'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PENCIL',
			clues: ['Long, thin device', 'Usually made of wood', 'Eyebrow __', 'Tool for writing or drawing'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PASSPORT',
			clues: ['Identification', 'Official document\nfor international travel', 'A permit', 'Containing personal information\nand usually a photograph'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MAGNET',
			clues: ['On a refrigerator door', 'South Pole or North Pole', 'Pushes and pulls', 'Attracts iron and steel objects'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'TRAFFIC JAM',
			clues: ['Rush hour', 'Long line of vehicles', 'Commuter\'s congestion problem', 'Unable to move\nor moving very slowly'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'DIARY',
			clues: ['Personal', 'Daily', 'Record', 'Book'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'CIGARETTE',
			clues: ['Small paper tube', 'What people smoke', 'Pieces of tobacco', 'Such as Marlboro'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'MEMORY',
			clues: ['RAM; Part of a computer', 'From the past', 'Ability to remember', 'Information,\nexperiences, and people'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'LIFE',
			clues: ['Heart symbol in a video game', 'Way of living', 'State of being alive', 'Period between birth and death'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'SPORTS',
			clues: ['Man cave topic', 'Basketball, football,\ngolf or marathon', 'Competitive games', 'Physical activity\nfor enjoyment or keeping healthy'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'ICE',
			clues: ['Temperature below 0 °C (Celsius)\nor below 32 °F (Fahrenheit)', 'Solid state', 'Cubes in a drink', 'Frozen water'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'CAMERA',
			clues: ['Tool for a tourist', 'Smartphone feature', 'DSLR', 'Device for taking photographs'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PURPLE',
			clues: ['Lavender', 'Color of Roman emperor', 'Violet', 'Mixing red and blue'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'AIR',
			clues: ['Tire, basketball or swim ring', 'On the __; Broadcasting', 'Atmosphere of Earth', 'You\'re breathing it'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'BOOKMARK',
			clues: ['Between the pages of a book', 'Shortcut to a website', 'Keep track of a reader\'s progress', 'Find a page again quickly'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'SHIP',
			clues: ['FedEx or DHL', 'Titanic or the Black Pearl', 'Friend__ or leader__', 'Large boat at sea'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PRESENT',
			clues: ['Introduce', 'Gift', 'Now', 'Neither the past nor the future'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'SMILE',
			clues: [')', '"Say cheese"', 'Selfie', 'Happy expression on a face'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'BEER',
			clues: ['Cold and bitter', 'Lager or ale', 'Alcoholic drink', 'Brew'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MARATHON',
			clues: ['Long-distance race', 'Battle in 490 BC during the first Persian invasion of Greece', 'Road running event', '42.195 kilometers \n(26 miles 385 yards)'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'COLLECT',
			clues: ['Pick up', 'Gather', 'Bring information together\nfrom different places', 'To get stamps or coins as a hobby'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'SELFIE',
			clues: ['Modern self-portrait', 'Social media', 'Personal photo', 'Front-facing camera photo'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'DELIVERY',
			clues: ['Door-to-door', 'Gives birth', 'Pizza __', 'Process of transporting goods'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'UMBRELLA',
			clues: ['J-shaped handle, often', 'Dome-shaped', 'Waterproof', 'Rainy-day item'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'WEATHER',
			clues: ['Safe topic', 'Conditions in the air', 'Daily forecasting', 'Wind, rain, or temperature'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'FLOWER',
			clues: ['Often brightly colored\nand has a pleasant smell', 'Bloom or blossom', 'Part of a plant', 'Rose, tulip or lily'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'JOB',
			clues: ['Regular work', 'Responsibility or duty', 'Person\'s role in society', 'What people do for money'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'GRAPH',
			clues: ['Visual symbol', 'PowerPoint diagram', 'Statistical information', 'Chart with X and Y'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'VIRUS',
			clues: ['Computer attack', 'Infection', 'Vaccine', 'Pandemic cause'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'BEE',
			clues: ['Worker in a flower garden', 'Yellow and black flying insect', 'Lives in a hive', 'Makes honey'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'QUEEN',
			clues: ['Victoria', 'Royal family', 'Woman who is married to a king', 'Woman who rules a country'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'AGE',
			clues: ['Can only go up', 'Number', 'Count of candles in a cake', 'Tree ring'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'INTERNET',
			clues: ['Surfing place', 'Communication system', 'Website', 'Computer network'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'SNOW',
			clues: ['Crystal', 'Ice within clouds', 'Weather for skiing', 'The "white" in "White Christmas"'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'CHICKEN',
			clues: ['Bird', 'Not good at flying', '__ game', 'KFC'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'LEGO',
			clues: ['Small building bricks', 'Danish brand', 'Plastic', 'Children\'s toy'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'HELLO',
			clues: ['Telephone greeting', 'Hi or \'Good morning\'', 'Used to \nattract someone’s attention', 'Hallo'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'MONEY',
			clues: ['Dollars, Euros, etc.', 'Coins or bills', 'What you keep in a bank', 'Payment for goods and services'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PEN',
			clues: ['Long and thin', 'Ink', 'Writing instrument', '"The __ is mightier than the sword"'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MUTE',
			clues: ['Silent', 'Sound off', 'Expressed without speech', 'TV volume option'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PLURAL',
			clues: ['Children, but not child', 'Women, but not woman', 'More than one', 'Opposite of \'singular\''],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'HURRICANE',
			clues: ['Has a calm eye', 'Weather event\nin the West Atlantic Ocean', 'Caribbean cyclone', 'Violent storm with circular winds'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'FIRE',
			clues: ['On __', 'Heat and light', 'Remove from employment', 'Shooting'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'ALARM',
			clues: ['Noise', 'Warning of danger', 'Clock app feature', 'Sound to wake a sleeper'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'JEANS',
			clues: ['Blue casual wear', 'Denim', 'Durable pants', 'Guess or Levi\'s'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'COLUMBUS',
			clues: ['Voyager in 1492', 'Discoverer of the New World', 'One of the first Europeans\nto arrive in the Americas', 'Italian explorer, "Christopher __"'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'ROLLER COASTER',
			clues: ['Up and down experience', 'Thrilling', 'Along the track in open cars', 'Small railroad in an amusement park'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'COPY',
			clues: ['Do you __?; Do you understand?', 'Reproduction of an original', 'Ctrl+C', '__ and paste'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MICHAEL',
			clues: ['Fashion designer, "__ Kors"', 'Archangel who defeated Satan\nin heaven', 'Basketball star, "__ Jordan"', 'King of Pop, "__ Jackson"'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'BRIDGE',
			clues: ['Crossing point', 'Captain\'s place on ship', 'Structure that is built over a river', '"Tower __" or "Golden Gate __"'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MIX',
			clues: ['1009 in Roman numerals', 'Stir well', 'Make a cocktail', 'Combination of \ndifferent things or people'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'CENTER',
			clues: ['Main part of a town or city', 'Normally the tallest player\non the team in a basketball game', 'Sports/shopping __', 'Middle point or part'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'WIRELESS',
			clues: ['Bluetooth or Wi-Fi', 'AirPods', 'Kind of network', 'Requiring no cables'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'LAST',
			clues: ['Z', 'December', 'Most recent', 'Final one'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'BANANA',
			clues: ['One of a tropical bunch', 'Curved', 'Yellow skin and white flesh', 'Fruit for a monkey'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MOUSE',
			clues: ['Long tail', 'Small device to move a cursor', 'Small mammal with short fur', 'Mickey __'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'CHAIR',
			clues: ['Four legs', 'Sometimes two arms', 'Throne', 'Seat at the table'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'TEXT',
			clues: ['Not the picture', 'Written words in a book', 'This is it', 'Message on mobile devices'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'SLEEP',
			clues: ['Night', 'Pajamas', 'Goal for counting sheep', 'Catnap'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PASS',
			clues: ['Succeed in an exam', 'Go past', 'Ticket', 'Give a ball to a teammate'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'HAIR',
			clues: ['Ponytail', 'May be curly', '__ dryer', 'On the head of a person'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'FEBRUARY',
			clues: ['Shortest', 'Second', 'Before March', 'Month with 28 or 29 days'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'GOOGLE',
			clues: ['Android', 'Gmail', 'YouTube', 'Search engine'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'COVER',
			clues: ['Outside of a book', '__ song', 'Envelope', 'Surface of something'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'COLOR',
			clues: ['Crayon', 'Yellow or gray', 'Paint', 'US spelling of \'colour\''],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'TOWEL',
			clues: ['Hand drier', 'After a bath', 'Kitchen item on a roll', 'Cloth for drying the body'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'JAZZ',
			clues: ['Modern music developed\nby African-Americans', 'Music genre', 'Improvised music', 'Louis Armstrong'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'BOOK',
			clues: ['Usually fiction or non-fiction', 'Medium for recording information', 'Make a reservation', 'Library'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'FICTION',
			clues: ['Type of book or story', 'Work of imagination', 'Not based strictly on history or fact', 'Novel'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'AFTERNOON',
			clues: ['Period following lunch', '__ Tea', 'Siesta', 'Time between midday and evening'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MOON',
			clues: ['Nearest object to the Earth', 'Crescent/full/new __', 'Biggest and brightest one\nin the night sky', 'The Earth\'s natural satellite'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MEMBER',
			clues: ['Person who belongs to a group', 'Arm or leg', 'One of a team', 'Family __'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PHONE NUMBER',
			clues: ['Contact details', 'Set of digits', 'Number that you use to call', 'Assigned number for telephone'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'KITCHEN',
			clues: ['Chef\'s room', 'With a stove, a sink,\nand a refrigerator', 'Has a microwave oven,\na dishwasher, etc.', 'Place where food is\nprepared and cooked'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'LIGHT',
			clues: ['Not heavy', 'Fastest', 'Not dark', 'Comes from the Sun or fire'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'COPYRIGHT',
			clues: ['Musician\'s protection', 'Creator\'s ownership', 'License', 'Legal right to reproduce\nan original work'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'ICE CREAM',
			clues: ['Sweet in a cone', 'Frozen', 'Cold dessert', 'Gelato-like'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'CENTURY',
			clues: ['Period', 'The 21st __', 'Ten decades', 'One hundred years'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PEACE',
			clues: ['V\'s meaning', 'Tolstoy\'s "War and __"', 'Dove', 'Freedom from war and violence'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'SPIDER',
			clues: ['Web', 'Eight legs', 'Small creature\nthat looks like an insect', 'Tarantula'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'LIE',
			clues: ['"I\'ll be there in five minutes", often', 'Rest on bed', 'Pinocchio\'s sin', 'Say something that is not true'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'DRUG',
			clues: ['Used as a medicine', 'Chemical illegally used', 'Alcohol, caffeine, cocaine or heroin', '__ addiction'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MUSIC',
			clues: ['Pattern of sounds', 'Pop, rock, jazz, etc.', 'Pleasant song', 'The Sound of __, film'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'RED',
			clues: ['Sunday in a calendar', 'Strawberry', 'Fire truck', 'First color of the rainbow from top'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PLASTIC',
			clues: ['Lego', '__ surgery', 'Credit cards or other bank cards', 'PET, vinyl or nylon'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'CAR',
			clues: ['Private transport', 'Ford, Audi or Tesla', 'Four wheels', 'Motor vehicle'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MORNING',
			clues: ['Good __', 'After sunrise', 'Before noon', 'First part of the day'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'ZERO',
			clues: ['Lowest possible amount', 'Often confused with the letter O', 'Nothing', 'Neither a positive or negative number'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'TREE',
			clues: ['Forest', 'Family diagram', 'Wood', 'Baobab, oak or poplar'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'SHORTCUT',
			clues: ['Saving time', 'More direct', 'Quicker way', 'Ctrl+C or Ctrl+V'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'RANSOM',
			clues: ['__ware', 'Release money', 'Demand from a kidnapper', 'Payment to free a hostage'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PRISON',
			clues: ['Cell', 'Alcatraz', 'The Shawshank Redemption', 'Place to keep criminals'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'BASKET',
			clues: ['Slam dunk', 'Shopping container', 'Laundry/picnic __', 'Woven bag'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PLANET',
			clues: ['Pluto, until 2006', 'Star circler', 'One of 8 in the Solar System', 'Earth'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'LUCK',
			clues: ['Chance', 'Good or bad', 'Success', 'Four-leaf clover'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'KEYBOARD',
			clues: ['QWERTY', 'Part of a piano', 'Typing', 'Computer input device'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'HOSPITAL',
			clues: ['Cross', 'Sick or injured', 'Emergency room', 'Doctors and nurses'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'BANK',
			clues: ['Riverside', 'Place with lots of money', 'Loan', 'Savings'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'HAMMER',
			clues: ['__ and anvil', 'Thor\'s weapon', 'Gavel', 'Nail'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'GREEN',
			clues: ['Grass', '__ light; Go', 'Environmentally friendly', 'Color between blue and yellow'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'FUTURE',
			clues: ['Not yet come', 'Prediction', 'Tomorrow', 'Opposite of past'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'DICE',
			clues: ['Six faces', 'Random numbers', 'From one to six', 'Cubes thrown in board games'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'CUT',
			clues: ['Slice', 'Hair__', 'Shorter, lower or smaller', 'Knife'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'LIVE',
			clues: ['Concert', 'Have a home', 'Broadcast in real-time', 'Life'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'CLASS',
			clues: ['First or economy', 'Rank', 'Economic group', 'Group of students'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'HABIT',
			clues: ['Often and regularly', 'Lack of awareness', 'Automatic behavior', 'Nail biting or leg shaking'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'BLOCK',
			clues: ['City area', 'Prevent', 'Brick', 'Child\'s building cube'],
		}
	},],
	[{
		[LanguageEnum.English]: {
			answer: 'TRAGEDY',
			clues: ['Titanic', 'Serious disaster', 'Hamlet or Macbeth', 'Play with sad ending'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'ANALOG',
			clues: ['Old television or camera', 'Continuous signal', 'Like an old clock with hands', 'Non-digital'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'STOP',
			clues: ['Red traffic sign', 'Bus __', 'Stay', 'Pause'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'AD',
			clues: ['Picture, short film, song, etc.', 'Sudden video or pop-up', 'Promotion', 'Anno Domini'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'WINE',
			clues: ['Dry or sweet', 'Red, white or rosé', 'Sommelier', 'Grape beverage'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'UNLOCK',
			clues: ['Using a face or fingerprint', 'Open a door', 'Enter a password', 'Undo with a key'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'TIME',
			clues: ['Never be turned back', 'Sandglass', 'Clock', 'Minutes, days, etc.'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PRESS',
			clues: ['Bench __', 'Publisher', 'Push', 'Newspapers and magazines'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'MAILBOX',
			clues: ['Email', 'Letter', 'Metal container in the street', 'Postbox'],
		}
	},
	{
		[LanguageEnum.English]: {
			answer: 'PARROT',
			clues: ['Repeat mechanically', 'Colourful talker', 'Pirate\'s pet', 'Cage bird'],
		}
	},],
];