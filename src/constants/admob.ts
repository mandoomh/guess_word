// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import RNAdvertisingId from 'react-native-advertising-id';
import { firebase, AdsConsent, AdsConsentDebugGeography } from '@react-native-firebase/admob';
import { getUniqueId } from 'react-native-device-info';

import { ConsentStatus } from 'src/enums';
import { log } from './log';
import { showMessage, alertMessage } from 'src/constants';
import { AppThunkDispatch, setUserConsentStatusFromAdmob } from '../store/mains/thunk';

const myPublisherId = 'pub-7668923106539535';
// myAppId https://groups.google.com/g/google-admob-ads-sdk/c/v4sS_KcJwBo
const myAppId = 'ca-app-pub-7668923106539535~2948532748';
const myApiKey = 'AIzaSyDl9bcK1nEuVbTMpDwQhYPc4jDubiweHas';
const myProjectId = 'guesswork-19341';
// privacyLinkUrl에 한글이 들어가면 showForm()이 동작하지 않는다.
const privacyLinkUrl = 'https://sites.google.com/view/onemounttobaek/%ED%99%88/privacy-policy';

const isNeedConsent = (consentStatus: ConsentStatus) => {
	return consentStatus === ConsentStatus.UNKNOWN;
};

const isNeedGetConsentFromUser = async () => {
	if (__DEV__) {
		await setDebugGeography();
	}

	// request the consent info update before calling any other method
	const { status: consentStatus, isRequestLocationInEeaOrUnknown } = await AdsConsent.requestInfoUpdate([myPublisherId]);

	log.debug(`isNeedGetConsentFromUser >> isRequestLocationInEeaOrUnknown: ${isRequestLocationInEeaOrUnknown} / consentStatus: ${consentStatus}`);

	return isRequestLocationInEeaOrUnknown && isNeedConsent(consentStatus);
};

const initializeAdMob = () => {
    /**
     * The tracking ID for Google Analytics, e.g. "UA-12345678-1", used to configure Google Analytics.
     */
	// gaTrackingId?: string;

    /**
     * iOS only - The OAuth2 client ID for iOS application used to authenticate Google users, for example
     * "12345.apps.googleusercontent.com", used for signing in with Google.
     */
	// clientId?: string;

    /**
     * iOS only - The Android client ID used in Google AppInvite when an iOS app has its Android version, for
     * example "12345.apps.googleusercontent.com".
     */
	// androidClientId?: string;

    /**
     * iOS only - The URL scheme used to set up Durable Deep Link service.
     */
	// deepLinkURLScheme?: string;

	const firebaseOptions = {
		appId: myAppId,
		projectId: myProjectId,
		apiKey: myApiKey,
		databaseURL: '',
		storageBucket: '',
		messagingSenderId: '',
	};

	firebase.initializeApp(firebaseOptions);
};

const setDebugGeography = async () => {
	const deviceId = getUniqueId();
	if (deviceId) {
		await AdsConsent.addTestDevices([deviceId]);
	}

	log.debug(`setDebugGeography > deviceId: `, deviceId);

	RNAdvertisingId.getAdvertisingId()
		.then((response: any) => {	// eslint-disable-line @typescript-eslint/no-explicit-any
			// advertisingId: admob에서 기기 테스트에 추가할 값.
			// 참조: https://apps.admob.com/v2/settings/test-devices/list
			log.debug(`setDebugGeography > advertisingId: `, response.advertisingId);
		});

	// GDPR 테스트를 위해 EEA로 설정.
	await AdsConsent.setDebugGeography(AdsConsentDebugGeography.EEA);
};

const getIsUserAgreesConsent = (formResponse: ConsentStatus) => {
	return [ConsentStatus.NON_PERSONALIZED, ConsentStatus.PERSONALIZED].includes(formResponse);
};

export const getIsAllowToShowAdvertToUser = (isNeedConsent: boolean, consentStatus: ConsentStatus) => {
	return !isNeedConsent || getIsUserAgreesConsent(consentStatus);
};

export const getIsAllowNonPersonalizedAdsOnly = (isNeedConsent: boolean, consentStatus: ConsentStatus) => {
	return isNeedConsent && consentStatus !== ConsentStatus.PERSONALIZED;
};

// https://rnfirebase.io/admob/european-user-consent#requesting-consent-information
// 1. If the users location is within the EEA or unknown, and their status is unknown,
// 	  you must request consent before showing any ads.
// 2. If the users location is within the EEA or unknown, and their status is not unknown,
// 	  you can show only the ad personalization they have requested.
// 3. If the users location is outside of the EEA, you do not have to give consent.
export const isInitializeAdMob = async (dispatch: AppThunkDispatch, isRewardAd = false) => {
	let isNeedConsent = true;
	let consentStatus = ConsentStatus.UNKNOWN;

	try {
		isNeedConsent = await isNeedGetConsentFromUser();

		log.debug(`isInitializeAdMob > isNeedConsent: `, isNeedConsent);

		if (isNeedConsent) {
			const { status } = await AdsConsent.showForm({
				privacyPolicy: privacyLinkUrl, withPersonalizedAds: true,
				withNonPersonalizedAds: true, withAdFree: false
			});
			consentStatus = status;
			log.debug(`isInitializeAdMob > AdsConsent.showForm > status: ${status}`);

			// admob 서버에 사용자 동의 결과를 저장한다.
			await AdsConsent.setStatus(consentStatus);
		}

		log.debug(`isInitializeAdMob > setUserConsentStatusFromAdmob >> isNeedConsent: ${isNeedConsent} / consentStatus: ${consentStatus}`);

		dispatch(setUserConsentStatusFromAdmob(isNeedConsent, consentStatus));

		const isAllowToShowAdvertToUser = getIsAllowToShowAdvertToUser(isNeedConsent, consentStatus);
		// 초기화는 앱이 한번 실행될 때마다 기본적으로 한번씩만 하면 된다.
		if (isAllowToShowAdvertToUser && !firebase.apps.length) {
			log.debug(`isInitializeAdMob > initializeAdMob(): `);
			initializeAdMob();
		}
	} catch (error) {
		// @todo 광고 초기화가 되지 않았음.
		log.error(`isInitializeAdMob > error: `, error);

		// VPN을 사용하는 경우 계속 IP가 바뀌면 간혹 구글에서 해킹으로 판단해 접속을 차단하는 듯 하다. 삼성 원격 테스트랩에서 테스트할 때 발생한 오류이다.
		if (isRewardAd && error.message && error.message.includes('Failed to connect to adservice.google.com')) {
			showMessage(alertMessage.FailedToConnectToGoogleAd);
		}
	}

	return { isNeedConsent, consentStatus };
};