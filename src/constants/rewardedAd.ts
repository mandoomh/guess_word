import { Platform } from 'react-native';
import { RewardedAd, RewardedAdEventType, TestIds } from '@react-native-firebase/admob';

import { AppThunkDispatch, userGetsPencils, showLoading, hideLoading } from '../store/mains/thunk';
import { ConsentStatus, UpdatingPencilCount } from '../enums';
import { getIsAllowToShowAdvertToUser, isInitializeAdMob } from './admob';
import { log } from './log';
import { playMusic, stopMusic } from './sound';
import { showMessage, alertMessage } from '../constants';

const myAdUnitId = __DEV__ ? TestIds.REWARDED : (Platform.OS === 'ios' ? 'ca-app-pub-7668923106539535/9550805393' : 'ca-app-pub-7668923106539535/1135322770');
const testDevices = __DEV__ ? ['EMULATOR'] : [];

export const showRewaredAd = async (dispatch: AppThunkDispatch) => {
	const { isNeedConsent, consentStatus } = await isInitializeAdMob(dispatch, true);

	log.debug(`showRewaredAd > BEFORE START / __DEV__: ${__DEV__} / isNeedConsent: ${isNeedConsent} / consentStatus: ${consentStatus}`);
	if (!getIsAllowToShowAdvertToUser(isNeedConsent, consentStatus)) {
		return;
	}

	try {
		const requestNonPersonalizedAdsOnly = isNeedConsent && consentStatus === ConsentStatus.NON_PERSONALIZED;

		log.debug(`showRewaredAd > START / adUnitId: ${myAdUnitId} / testDevices: ${testDevices} / requestNonPersonalizedAdsOnly: ${requestNonPersonalizedAdsOnly}`);

		const rewardAd = RewardedAd.createForAdRequest(myAdUnitId, {
			testDevices,
			requestNonPersonalizedAdsOnly
		});

		rewardAd.onAdEvent((type, error, reward) => {
			dispatch(hideLoading());

			if (type === RewardedAdEventType.LOADED) {
				log.debug(`showRewaredAd > LOADED`);
				stopMusic();
				rewardAd.show();
			} else if (type === RewardedAdEventType.EARNED_REWARD) {
				log.debug(`showRewaredAd > EARNED_REWARD`);
				playMusic();
				dispatch(userGetsPencils(UpdatingPencilCount.AdsBonus, 0));
			}

			if (error) {
				playMusic();
				log.error(`rewardAd.onAdEvent > error`, error);
				// 원인은 알 수 없으나 시뮬레이터에서 실행 시 간혹 admob에서 아래 오류가 발생한다.
				if (error.message && error.message.includes('An unknown error occurred')) {
					showMessage(alertMessage.SomethingWrong);
				} else if (error.message && error.message.includes('The ad request was successful, but no ad was returned due to lack of ad inventory.')) {
					showMessage(alertMessage.NoAdsNow);
				}
			}
		});

		// Start loading the rewarded ad straight away
		rewardAd.load();
		dispatch(showLoading());
	} catch (error) {
		log.error(`showRewaredAd > error`, error);
		showMessage(alertMessage.SomethingWrong);
	}
};