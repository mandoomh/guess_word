import { createStore, Store, AnyAction, applyMiddleware, compose } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import { createMigrate, persistStore, persistReducer } from 'redux-persist';
import hardSet from 'redux-persist/lib/stateReconciler/hardSet';
import thunk from 'redux-thunk';

import mainReducer, { initialState } from './mains/reducers';
import { IRootState } from '../interfaces';

const migrations = {
	0: (state: IRootState) => {
		return {
			...state,
		};
	},
	16: (state: IRootState) => {
		return {
			...state,
			lastEnterKeyDate: initialState.lastEnterKeyDate,
		};
	},
	17: (state: IRootState) => {
		return {
			...state,
			isShowHelp: false
		};
	},
	18: (state: IRootState) => {
		return {
			...state,
		};
	},
};

const persistConfig = {
	key: 'root',
	version: 18,
	storage: AsyncStorage,
	// hardSet: 기존 state와 신규 state 병합 시, 신규 state로 덮어씌운다.
	stateReconciler: hardSet,
	// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
	// @ts-ignore: state가 PersistedState여야 한다는데 persist 변수는 사용하지 않으므로 무시한다.
	migrate: createMigrate(migrations, { debug: false }),
};

const finalReducer = persistReducer(persistConfig, mainReducer);

export const rootStore = createStore(
	finalReducer,
	compose(applyMiddleware(thunk))
);

// rootStore를 바로 Store<any, AnyAction>로 타입 추정이 안되므로 unknown으로 먼저 변환.
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const persistor = persistStore(rootStore as unknown as Store<any, AnyAction>);

export const selectState = (state: IRootState) => state;