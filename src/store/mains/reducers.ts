import { cloneDeep } from 'lodash';
import { AdsConsent } from '@react-native-firebase/admob';

import { LanguageEnum, PageEnum, ModalType, ConsentStatus } from '../../enums';
import { IInitialStateEachWord, IRootState } from '../../interfaces';
import { getInitialInputLetters, getWord, getPossibleHintIndexes, BLANK, giftInterval, clueAndHintShakeInterval } from '../../constants';
import { localIapProducts } from '../../constants/inAppPurchase';
import { Actions, ActionTypeEnum } from './actions';

const initialStateEachWord: IInitialStateEachWord = {
	currentClueIndex: 0,
	currentInputFocusIndex: 0,
	currentHintIndexes: [],
	modalToShow: ModalType.None,
	lastEnterKeyDate: (new Date().getTime() - clueAndHintShakeInterval),
};

export const initialState: IRootState = {
	...initialStateEachWord,
	language: LanguageEnum.English,
	isSoundOn: true,
	isMusicOn: true,
	currentStageIndex: 0,
	currentWordIndex: 0,
	currentInputLetters: [],
	pencilCount: 100,
	wordGuessedIndexes: [],
	currentPage: PageEnum.Menu,
	pencilComponentKey: 0,
	pencilIncreased: true,
	isNeedConsent: true,
	consentStatus: ConsentStatus.UNKNOWN,
	isLoading: false,
	// iapProducts: (null: before fetch / []: failed products after fetch)
	iapProducts: [...localIapProducts],
	// getTime(): 현재 시각을 milliseconds로 반환.
	hideAdsDate: new Date().getTime(),
	nextGetPencilDate: new Date().getTime(),
	isShowHelp: false,
};

const getRandomInt = (max: number) => {
	//최댓값은 제외, 최솟값은 포함.
	return Math.floor(Math.random() * max);
};

const getNextHintIndexes = (answer: string, currentHintIndexes: number[]) => {
	const possibleHintIndexes = getPossibleHintIndexes(answer, currentHintIndexes);
	const nextHintIndex = getRandomInt(possibleHintIndexes.length);

	const nextHintIndexes = [...currentHintIndexes];
	nextHintIndexes.push(possibleHintIndexes[nextHintIndex]);

	return nextHintIndexes;
};

const getNextInputFocusIndex = (currentInputFocusIndex: number, action: ActionTypeEnum, currentHintIndexes: number[], answer: string) => {
	let newInputFocusIndex = currentInputFocusIndex;
	if ([ActionTypeEnum.InsertInput, ActionTypeEnum.OpenHint].includes(action)) {
		const startIndex = action === ActionTypeEnum.InsertInput ? currentInputFocusIndex + 1 : currentInputFocusIndex;
		for (let i = startIndex; i < answer.length; i++) {
			if (!currentHintIndexes.includes(i) && answer[i] !== BLANK) {
				newInputFocusIndex = i;
				break;
			}
		}
	} else if (action === ActionTypeEnum.DeleteInput) {
		for (let i = currentInputFocusIndex - 1; 0 <= i; i--) {
			if (!currentHintIndexes.includes(i) && answer[i] !== BLANK) {
				newInputFocusIndex = i;
				break;
			}
		}
	}

	return newInputFocusIndex;
};

const reducer = (state = initialState, action: Actions): IRootState => {
	const prevState = { ...initialState, ...state };
	switch (action.type) {
		case ActionTypeEnum.MovePage:
			return { ...prevState, currentPage: action.payload.newPage };

		case ActionTypeEnum.ChangeSoundOn:
			return { ...prevState, isSoundOn: action.payload.isSoundOn };

		case ActionTypeEnum.ChangeMusicOn:
			return { ...prevState, isMusicOn: action.payload.isMusicOn };

		case ActionTypeEnum.GoNextWord: {
			const { nextStageIndex, nextWordIndex } = action.payload;
			const nextInputLetters = getInitialInputLetters(nextStageIndex, nextWordIndex, prevState.language);

			const nextState: Partial<IRootState> = {
				...initialStateEachWord,
				lastEnterKeyDate: new Date().getTime(),
				currentStageIndex: nextStageIndex,
				currentWordIndex: nextWordIndex,
				currentInputLetters: nextInputLetters,
			};

			// 첫번째 단어를 표시할 시 강제로 도움말 표시.
			const isShowHelp = nextStageIndex === 0 && nextWordIndex === 0;

			// 다음 단어 이동 시에는 항상 게임 화면으로 이동해야 한다.
			return { ...prevState, ...nextState, currentPage: PageEnum.Game, isShowHelp };
		}

		case ActionTypeEnum.OpenClue: {
			const { newClueIndex } = action.payload;
			return { ...prevState, currentClueIndex: newClueIndex, lastEnterKeyDate: new Date().getTime() };
		}

		case ActionTypeEnum.OpenHint: {
			const { currentStageIndex, currentWordIndex, language, currentInputFocusIndex, currentHintIndexes } = prevState;
			const currentWord = getWord(currentStageIndex, currentWordIndex, language);
			const nextHintIndexes = getNextHintIndexes(currentWord.answer, currentHintIndexes);
			const nextInputFocusIndex = getNextInputFocusIndex(currentInputFocusIndex, ActionTypeEnum.OpenHint, nextHintIndexes, currentWord.answer);

			return {
				...prevState, currentHintIndexes: nextHintIndexes,
				currentInputFocusIndex: nextInputFocusIndex, modalToShow: ModalType.OpenHint, lastEnterKeyDate: new Date().getTime()
			};
		}

		case ActionTypeEnum.FocusInput:
			return { ...prevState, currentInputFocusIndex: action.payload.newInputIndex, lastEnterKeyDate: new Date().getTime() };

		case ActionTypeEnum.ChangeIsLoading:
			const { isLoading } = action.payload;
			return { ...prevState, isLoading };

		case ActionTypeEnum.UpdatePencilCount: {
			const { pencilCount: currentPencilCount, pencilComponentKey } = prevState;
			const { updatingPencilCount } = action.payload;
			return {
				...prevState,
				pencilCount: (currentPencilCount + updatingPencilCount),
				pencilComponentKey: pencilComponentKey + 1,
				pencilIncreased: updatingPencilCount >= 0,
			};
		}

		case ActionTypeEnum.UpdateHideAdsDate: {
			const { newHideAdsDate } = action.payload;
			return { ...prevState, hideAdsDate: newHideAdsDate };
		}

		case ActionTypeEnum.ChangeModalType: {
			const { newModalType } = action.payload;
			return { ...prevState, modalToShow: newModalType };
		}

		case ActionTypeEnum.InitAdConsent: {
			AdsConsent.setStatus(ConsentStatus.UNKNOWN);
			return { ...prevState, isNeedConsent: initialState.isNeedConsent, consentStatus: initialState.consentStatus };
		}

		case ActionTypeEnum.SetUserConsentStatusFromAdmob:
			const { isNeedConsent, consentResponse } = action.payload;
			return { ...prevState, isNeedConsent, consentStatus: consentResponse };

		case ActionTypeEnum.InsertInput: {
			const { currentStageIndex, currentWordIndex, language, currentInputLetters, currentInputFocusIndex, currentHintIndexes } = prevState;
			const newInputLetters = [...currentInputLetters];
			newInputLetters[currentInputFocusIndex] = action.payload.inputLetter;
			const currentWord = getWord(currentStageIndex, currentWordIndex, language);
			const nextInputFocusIndex = getNextInputFocusIndex(currentInputFocusIndex, ActionTypeEnum.InsertInput, currentHintIndexes, currentWord.answer);

			return { ...prevState, currentInputLetters: newInputLetters, currentInputFocusIndex: nextInputFocusIndex, lastEnterKeyDate: new Date().getTime() };
		}

		case ActionTypeEnum.InsertWordGuessedIndexes: {
			const { guessedClueIndex } = action.payload;

			const { wordGuessedIndexes, currentStageIndex, currentWordIndex } = prevState;

			const newWordGuessedIndexes = cloneDeep(wordGuessedIndexes);
			if (!newWordGuessedIndexes[currentStageIndex]) {
				newWordGuessedIndexes[currentStageIndex] = [];
			}
			newWordGuessedIndexes[currentStageIndex][currentWordIndex] = guessedClueIndex;

			return { ...prevState, wordGuessedIndexes: newWordGuessedIndexes };
		}

		case ActionTypeEnum.DeleteInput: {
			const { currentStageIndex, currentWordIndex, language, currentInputLetters, currentInputFocusIndex, currentHintIndexes } = prevState;
			const newInputLetters = [...currentInputLetters];
			newInputLetters[currentInputFocusIndex] = '';
			const currentWord = getWord(currentStageIndex, currentWordIndex, language);
			let nextInputFocusIndex = currentInputFocusIndex;
			if (currentInputLetters[currentInputFocusIndex] === '') {
				nextInputFocusIndex = getNextInputFocusIndex(currentInputFocusIndex, ActionTypeEnum.DeleteInput, currentHintIndexes, currentWord.answer);
			}

			return { ...prevState, currentInputLetters: newInputLetters, currentInputFocusIndex: nextInputFocusIndex, lastEnterKeyDate: new Date().getTime() };
		}

		case ActionTypeEnum.DeleteAllInput: {
			const { currentInputLetters, } = prevState;
			return { ...prevState, currentInputLetters: Array(currentInputLetters.length).fill(''), currentInputFocusIndex: 0, lastEnterKeyDate: new Date().getTime() };
		}

		case ActionTypeEnum.SetNextGetPencilDate: {
			return { ...prevState, nextGetPencilDate: new Date().getTime() + giftInterval };
		}

		case ActionTypeEnum.SetIsShowHelp: {
			return { ...prevState, isShowHelp: action.payload.isShowHelp };
		}

		case ActionTypeEnum.SetLastEnterKeyDate: {
			return { ...prevState, lastEnterKeyDate: new Date().getTime() + clueAndHintShakeInterval };
		}

		case ActionTypeEnum.SetIapProducts: {
			const { iapProducts } = action.payload;
			return { ...prevState, iapProducts };
		}

		case ActionTypeEnum.DebugSetNowGetPencilDate: {
			const nextPencilCount = prevState.pencilCount > 1000 ? 0 : 10000;
			return { ...prevState, nextGetPencilDate: new Date().getTime() + 3000, pencilCount: nextPencilCount };
		}

		case ActionTypeEnum.DebugSetWordGuessed: {
			const { inputLetters } = action.payload;
			return { ...prevState, currentInputLetters: inputLetters };
		}

		case ActionTypeEnum.DebugAllReset:
			return { ...initialState };

		case ActionTypeEnum.DebugSetLastWord:
			return {
				...initialState, wordGuessedIndexes: [
					[1, 2, 3, 1, 2, 3, 1, 2, 3, 1],
				]
			};

		default:
			return state;
	}
};

export default reducer;