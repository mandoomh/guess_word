import { PageEnum, ConsentStatus, ModalType } from '../../enums';
import { IapProduct } from '../../interfaces';

export enum ActionTypeEnum {
	MovePage = 'MovePage',
	UpdatePencilCount = 'UpdatePencilCount',
	UpdateHideAdsDate = 'UpdateHideAdsDate',
	InsertInput = 'InsertInput',
	InsertWordGuessedIndexes = 'InsertWordGuessedIndexes',
	DeleteInput = 'DeleteInput',
	DeleteAllInput = 'DeleteAllInput',
	OpenHint = 'OpenHint',
	ChangeModalType = 'ChangeModalType',
	GoNextWord = 'GoNextWord',
	ChangeSoundOn = 'ChangeSoundOn',
	ChangeMusicOn = 'ChangeMusicOn',
	FocusInput = 'FocusInput',
	OpenClue = 'OpenClue',
	ChangeIsLoading = 'ChangeIsLoading',
	DebugAllReset = 'DebugAllReset',
	DebugSetLastWord = 'DebugSetLastWord',
	InitAdConsent = 'InitAdConsent',
	DebugSetNowGetPencilDate = 'DebugSetNowGetPencilDate',
	SetUserConsentStatusFromAdmob = 'SetUserConsentStatusFromAdmob',
	SetIapProducts = 'SetIapProducts',
	SetNextGetPencilDate = 'SetNextGetPencilDate',
	SetLastEnterKeyDate = 'SetLastEnterKeyDate',
	DebugSetWordGuessed = 'DebugSetWordGuessed',
	SetIsShowHelp = 'SetIsShowHelp',
}

interface IPayloadMovePage {
	newPage: PageEnum;
}

interface IPayloadUpdatePencilCount {
	updatingPencilCount: number;
}

interface IPayloadUpdateHideAdsDate {
	newHideAdsDate: number;
}

interface IPayloadChangeSound {
	isSoundOn: boolean;
}

interface IPayloadChangeMusic {
	isMusicOn: boolean;
}

interface IPayloadOpenHint {
	newPencilCount: number;
}

interface IPayloadGoNextWord {
	nextStageIndex: number;
	nextWordIndex: number;
}

interface IPayloadFocusInput {
	newInputIndex: number;
}

interface IPayloadOpenClue {
	newClueIndex: number;
}

interface IPayloadInsertInputLetter {
	inputLetter: string;
}

interface IPayloadChangeModalType {
	newModalType: ModalType;
}

interface IPayloadChangeIsLoading {
	isLoading: boolean;
}

interface IPayloadSetUserConsentStatusFromAdmob {
	isNeedConsent: boolean;
	consentResponse: ConsentStatus;
}

interface IPayloadSetIapProducts {
	iapProducts: IapProduct[];
}

interface IPayloadInsertWordGuessedIndexes {
	guessedClueIndex: number;
}

interface IPayloadDebugSetWordGuessed {
	inputLetters: string[];
}

interface IPayloadSetIsShowHelp {
	isShowHelp: boolean;
}

export type ActionArgs =
	{} | IPayloadMovePage | IPayloadUpdatePencilCount | IPayloadChangeSound | IPayloadChangeMusic
	| IPayloadGoNextWord | IPayloadFocusInput | IPayloadInsertInputLetter | IPayloadInsertWordGuessedIndexes
	| IPayloadOpenHint | IPayloadSetUserConsentStatusFromAdmob | IPayloadChangeModalType | IPayloadChangeIsLoading
	| IPayloadOpenClue | IPayloadSetIapProducts | IPayloadUpdateHideAdsDate | IPayloadDebugSetWordGuessed
	| IPayloadSetIsShowHelp
	;

export type Actions = {
	type: ActionTypeEnum.MovePage;
	payload: IPayloadMovePage;
} | {
	type: ActionTypeEnum.UpdateHideAdsDate;
	payload: IPayloadUpdateHideAdsDate;
} | {
	type: ActionTypeEnum.GoNextWord;
	payload: IPayloadGoNextWord;
} | {
	type: ActionTypeEnum.UpdatePencilCount;
	payload: IPayloadUpdatePencilCount;
} | {
	type: ActionTypeEnum.ChangeSoundOn;
	payload: IPayloadChangeSound;
} | {
	type: ActionTypeEnum.ChangeMusicOn;
	payload: IPayloadChangeMusic;
} | {
	type: ActionTypeEnum.InsertInput;
	payload: IPayloadInsertInputLetter;
} | {
	type: ActionTypeEnum.InsertWordGuessedIndexes;
	payload: IPayloadInsertWordGuessedIndexes;
} | {
	type: ActionTypeEnum.DeleteInput;
	payload: {};
} | {
	type: ActionTypeEnum.OpenHint;
	payload: {};
} | {
	type: ActionTypeEnum.ChangeIsLoading;
	payload: IPayloadChangeIsLoading;
} | {
	type: ActionTypeEnum.ChangeModalType;
	payload: IPayloadChangeModalType;
} | {
	type: ActionTypeEnum.FocusInput;
	payload: IPayloadFocusInput;
} | {
	type: ActionTypeEnum.OpenClue;
	payload: IPayloadOpenClue;
} | {
	type: ActionTypeEnum.DebugAllReset;
	payload: {};
} | {
	type: ActionTypeEnum.DebugSetLastWord;
	payload: {};
} | {
	type: ActionTypeEnum.InitAdConsent;
	payload: {};
} | {
	type: ActionTypeEnum.SetUserConsentStatusFromAdmob;
	payload: IPayloadSetUserConsentStatusFromAdmob;
} | {
	type: ActionTypeEnum.SetIapProducts;
	payload: IPayloadSetIapProducts;
} | {
	type: ActionTypeEnum.SetNextGetPencilDate;
	payload: {};
} | {
	type: ActionTypeEnum.DebugSetNowGetPencilDate;
	payload: {};
} | {
	type: ActionTypeEnum.SetLastEnterKeyDate;
	payload: {};
} | {
	type: ActionTypeEnum.DeleteAllInput;
	payload: {};
} | {
	type: ActionTypeEnum.DebugSetWordGuessed;
	payload: IPayloadDebugSetWordGuessed;
} | {
	type: ActionTypeEnum.SetIsShowHelp;
	payload: IPayloadSetIsShowHelp;
};