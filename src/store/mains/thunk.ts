import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { showMessage as showFlashMessage } from 'react-native-flash-message';

import { PageEnum, UpdatingPencilCount, ConsentStatus, ModalType, ColorEnum, SoundEffect } from '../../enums';
import {
	getWord, getInputs, getIsAllInputsFilled, getIsInputsCorrect, fontSizes, slowUpdatingPencilInterval,
	fastUpdatingPencilInterval, CLUE_LENGTH, getNextStageWordIndex, playSound, hasNextWord, timer, getIsShowAds
} from '../../constants';
import { IRootState, IapProduct } from '../../interfaces';
import { Actions, ActionArgs, ActionTypeEnum } from './actions';
import { showInterstitialAd } from 'src/constants/interstitialAd';

export type AppThunkAction<ReturnType> = ThunkAction<ReturnType, IRootState, ActionArgs, Actions>;
export type AppThunkDispatch = ThunkDispatch<IRootState, ActionArgs, Actions>;

export const movePage = (newPage: PageEnum): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.MovePage, payload: { newPage } });
};

export const moveToStageClear = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	playSound(SoundEffect.StageClear);
	dispatch(movePage(PageEnum.StageClear));
};

export const debugAllReset = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.DebugAllReset, payload: {} });
};

export const debugSetLastWord = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.DebugSetLastWord, payload: {} });
};

export const initConsent = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.InitAdConsent, payload: {} });
};

export const debugSetNowGetPencilDate = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.DebugSetNowGetPencilDate, payload: {} });
};

export const setIapProducts = (iapProducts: IapProduct[]): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.SetIapProducts, payload: { iapProducts } });
};

export const setLastEnterKeyDate = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.SetLastEnterKeyDate, payload: {} });
};

export const insertInput = (inputLetter: string): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	playSound(SoundEffect.InsertInput);

	dispatch({ type: ActionTypeEnum.InsertInput, payload: { inputLetter } });

	dispatch(checkIsInputsCorrect());
};

export const checkIsInputsCorrect = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	const rootState = getState();
	const { answer } = _getWord(rootState);
	const inputs = _getInputs(answer, rootState);
	if (getIsAllInputsFilled(inputs)) {
		if (getIsInputsCorrect(inputs, answer)) {
			dispatch(handleOnInputsCorrect());
		} else {
			playSound(SoundEffect.WordIsWrong);
			showFlashMessage({
				message: 'Oh, Try Again.',
				titleStyle: { textAlign: 'center', lineHeight: fontSizes.big, fontSize: fontSizes.big },
				backgroundColor: ColorEnum.TransparentPink,
				duration: 600
			});
		}
	}
};

const updatePencilCount = (updatingPencilCount: number): AppThunkAction<void> => async (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.UpdatePencilCount, payload: { updatingPencilCount } });
};

const handleOnInputsCorrect = (): AppThunkAction<void> => async (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	const { currentClueIndex } = getState();

	playSound(SoundEffect.GetPencil);

	for (let i = currentClueIndex; i < CLUE_LENGTH; i++) {
		dispatch(openClue(true));
		await timer(fastUpdatingPencilInterval);
	}

	dispatch({ type: ActionTypeEnum.InsertWordGuessedIndexes, payload: { guessedClueIndex: currentClueIndex } });

	await timer(fastUpdatingPencilInterval);

	dispatch(updatePencilCount(UpdatingPencilCount.GuessWord));
};

export const debugSetWordGuessed = (): AppThunkAction<void> => async (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	const { currentStageIndex, currentWordIndex, language } = getState();

	const word = getWord(currentStageIndex, currentWordIndex, language);

	const guessedInputs = Array(word.answer.length).fill('').map((inputLetter, index) => {
		return word.answer[index];
	});

	dispatch({ type: ActionTypeEnum.DebugSetWordGuessed, payload: { inputLetters: guessedInputs } });

	dispatch(handleOnInputsCorrect());
};

export const deleteInput = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	playSound(SoundEffect.DeleteInput);

	dispatch({ type: ActionTypeEnum.DeleteInput, payload: {} });
};

export const deleteAllInput = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	playSound(SoundEffect.DeleteInput);

	dispatch({ type: ActionTypeEnum.DeleteAllInput, payload: {} });
};

export const changeSoundOn = (isSoundOn: boolean): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.ChangeSoundOn, payload: { isSoundOn } });
};

export const changeMusicOn = (isMusicOn: boolean): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.ChangeMusicOn, payload: { isMusicOn } });
};

export const focusInput = (newInputIndex: number): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	playSound(SoundEffect.TouchButton);
	dispatch({ type: ActionTypeEnum.FocusInput, payload: { newInputIndex } });
};

export const openClue = (isAlreadyGuessed: boolean): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	if (!isAlreadyGuessed) {
		if (dispatch(getUpdatePencilCountResult(UpdatingPencilCount.OpenClue))) {
			const { currentClueIndex } = getState();

			playSound(SoundEffect.OpenClue);

			dispatch({ type: ActionTypeEnum.OpenClue, payload: { newClueIndex: currentClueIndex + 1 } });
		} else {
			dispatch(showGetPencilModal());
		}
	} else {
		const { currentClueIndex } = getState();
		dispatch({ type: ActionTypeEnum.OpenClue, payload: { newClueIndex: currentClueIndex + 1 } });
	}
};

export const showGetPencilModal = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.ChangeModalType, payload: { newModalType: ModalType.GetPencil } });
};

export const showSettingModal = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	playSound(SoundEffect.TouchButton);
	dispatch({ type: ActionTypeEnum.ChangeModalType, payload: { newModalType: ModalType.Setting } });
};

export const showStatusModal = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	playSound(SoundEffect.TouchButton);
	dispatch({ type: ActionTypeEnum.ChangeModalType, payload: { newModalType: ModalType.Status } });
};

export const setIsShowHelp = (isShowHelp: boolean): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.SetIsShowHelp, payload: { isShowHelp } });
};

export const showLoading = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.ChangeIsLoading, payload: { isLoading: true } });
};

export const hideLoading = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.ChangeIsLoading, payload: { isLoading: false } });
};

export const hideModal = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	dispatch({ type: ActionTypeEnum.ChangeModalType, payload: { newModalType: ModalType.None } });
};

export const setUserConsentStatusFromAdmob = (isNeedConsent: boolean,
	consentResponse: ConsentStatus): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
		dispatch({ type: ActionTypeEnum.SetUserConsentStatusFromAdmob, payload: { isNeedConsent, consentResponse } });
	};

const getDateWithAddDays = (milliseconds: number, days: number) => {
	const prevDate = new Date(milliseconds);
	prevDate.setDate(prevDate.getDate() + days);
	prevDate.setHours(23, 59, 59);
	return prevDate.getTime();
};

export const updateHideAdsDate = (weeksHideAds: number): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	if (weeksHideAds > 0) {
		const daysWeek = 7;
		const addDays = weeksHideAds * daysWeek;
		const { hideAdsDate } = getState();

		const newHideAdsDate = getDateWithAddDays(hideAdsDate, addDays);

		dispatch({ type: ActionTypeEnum.UpdateHideAdsDate, payload: { newHideAdsDate } });
	}
};

export const getGiftPencilBonus = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	// updateTotalPencilCount() 수행 후 SetNextGetPencilDate를 수행하게 되면 SetNextGetPencilDate 수행 전에 중복 터치가 가능하다.
	// 따라서 반드시 SetNextGetPencilDate를 먼저 수행 후 updateTotalPencilCount()를 수행하도록 한다.
	dispatch({ type: ActionTypeEnum.SetNextGetPencilDate, payload: {} });
	dispatch(updateTotalPencilCount(UpdatingPencilCount.GiftBonus, true));
};

export const userGetsPencils = (pencilCount: UpdatingPencilCount, weeksHideAds: number): AppThunkAction<Promise<boolean>> => (dispatch: AppThunkDispatch, getState: () => IRootState): Promise<boolean> => {
	dispatch(updateHideAdsDate(weeksHideAds));
	return dispatch(updateTotalPencilCount(pencilCount, true))
		.then(result => {
			return Promise.resolve(true);
		});
};

const hasEnoughPencils = (currentPencilCount: number, updatingPencilCount: number) => {
	// 차감인 경우 updatingPencilCount가 마이너스이므로 더해주어야 한다.
	return currentPencilCount + updatingPencilCount >= 0;
};

// 액션 수행을 하는 데에 있어 연필 소모가 필요한 액션 목록 및 연필 개수.
const pencilCountForAction = {
	[ActionTypeEnum.OpenHint]: UpdatingPencilCount.OpenHint,
	[ActionTypeEnum.OpenClue]: UpdatingPencilCount.OpenClue,
};

export const showOpenHintModal = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	playSound(SoundEffect.TouchButton);

	const currentPencilCount = getState().pencilCount;
	const neededPencilCount = pencilCountForAction[ActionTypeEnum.OpenHint];

	// 어차피 연필이 부족하면 힌트 확인이 안되니 사용자 경험 향상을 위해 힌트 확인 팝업창을 열기 전에도 연필 개수를 체크해준다...
	if (hasEnoughPencils(currentPencilCount, neededPencilCount)) {
		dispatch({ type: ActionTypeEnum.ChangeModalType, payload: { newModalType: ModalType.OpenHint } });
	} else {
		dispatch(showGetPencilModal());
	}
};

export const goNextWord = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	const { wordGuessedIndexes, hideAdsDate } = getState();
	const { nextStageIndex, nextWordIndex } = getNextStageWordIndex(wordGuessedIndexes);

	// 5, 10번째 단어인 경우에만 5초 광고 표시.
	if (getIsShowAds(hideAdsDate) && [4, 9].includes(nextWordIndex)) {
		showInterstitialAd(dispatch);
	}

	if (hasNextWord(nextStageIndex, nextWordIndex)) {
		playSound(SoundEffect.GoNextWord);
		dispatch({ type: ActionTypeEnum.GoNextWord, payload: { nextStageIndex, nextWordIndex } });
	} else {
		dispatch(movePage(PageEnum.Stage));
	}
};

export const openHint = (): AppThunkAction<void> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	const updatingPencilCount = pencilCountForAction[ActionTypeEnum.OpenHint];
	if (dispatch(getUpdatePencilCountResult(updatingPencilCount))) {
		playSound(SoundEffect.OpenHint);

		dispatch({ type: ActionTypeEnum.OpenHint, payload: {} });
		dispatch(hideModal());
		dispatch(checkIsInputsCorrect());
	} else {
		dispatch(showGetPencilModal());
	}
};

/**
 * 연필 차감이 필요한 액션 수행 전 연필을 차감한 후 성공 여부를 반환.
 *
 * @param updatingPencilCount 갱신할 연필 개수
 */
const updateTotalPencilCount = (updatingPencilCount: number, withSound: boolean): AppThunkAction<Promise<boolean>> => async (dispatch: AppThunkDispatch, getState: () => IRootState): Promise<boolean> => {
	const updateUnit = Math.abs(updatingPencilCount) >= 100 ? 100 : (Math.abs(updatingPencilCount) >= 10 ? 10 : 1);
	const updateTimes = Math.abs(updatingPencilCount / updateUnit);
	const updatingCountOnce = updateUnit * (updatingPencilCount >= 0 ? 1 : -1);

	const updatingInterval = updatingPencilCount >= 100 ? fastUpdatingPencilInterval : slowUpdatingPencilInterval;

	if (withSound) {
		playSound(SoundEffect.GetPencil);
	}

	for (let i = 0; i < updateTimes; i++) {
		dispatch(updatePencilCount(updatingCountOnce));
		await timer(updatingInterval);
	}

	return Promise.resolve(true);
};

/**
 * 연필 차감이 필요한 액션 수행 전 연필을 차감한 후 성공 여부를 반환.
 *
 * @param updatingPencilCount 갱신할 연필 개수
 */
const getUpdatePencilCountResult = (updatingPencilCount: number): AppThunkAction<boolean> => (dispatch: AppThunkDispatch, getState: () => IRootState) => {
	const currentPencilCount = getState().pencilCount;
	if (hasEnoughPencils(currentPencilCount, updatingPencilCount)) {
		// 연필 차감.
		dispatch(updateTotalPencilCount(updatingPencilCount, false));
		return true;
	} else {
		return false;
	}
};

const _getWord = (rootState: IRootState) => {
	const { currentStageIndex, currentWordIndex, language } = rootState;
	return getWord(currentStageIndex, currentWordIndex, language);
};

const _getInputs = (answer: string, rootState: IRootState) => {
	const { currentInputLetters, currentHintIndexes, currentInputFocusIndex } = rootState;
	return getInputs(answer, currentInputLetters, currentHintIndexes, currentInputFocusIndex, false);
};