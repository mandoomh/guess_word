/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useEffect, useRef } from 'react';
import { StatusBar, AppState, AppStateStatus } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';

import { selectState, rootStore } from 'src/store/index';
import { AppThunkDispatch, hideLoading, hideModal, movePage } from './store/mains/thunk';
import Body from './components/frame/body';
import { playMusic, stopMusic, setIsMusicOn, setIsSoundOn } from 'src/constants';
import { initIapConnection, removeIapConnection } from './constants/inAppPurchase';
import { PageEnum } from './enums';

const handleSoundOnOff = () => {
	const { isMusicOn: newIsMusicOn, isSoundOn: newIsSoundOn } = rootStore.getState();
	setIsMusicOn(newIsMusicOn);
	setIsSoundOn(newIsSoundOn);
};

const Main = () => {
	const state = useSelector(selectState);
	const { isMusicOn, isSoundOn } = state;
	const dispatch = useDispatch<AppThunkDispatch>();

	useEffect(() => {
		SplashScreen.hide();
		dispatch(movePage(PageEnum.Menu));
		dispatch(hideLoading());
		dispatch(hideModal());

		setIsMusicOn(isMusicOn);
		setIsSoundOn(isSoundOn);

		initIapConnection(dispatch);
		AppState.addEventListener('change', handleAppStateChange);
		const unsubscribe = rootStore.subscribe(handleSoundOnOff);

		return () => {
			removeIapConnection();	// initIapConnection() 해제.
			AppState.removeEventListener('change', handleAppStateChange);
			unsubscribe();
		};
	}, []);

	const appState = useRef(AppState.currentState);

	const handleAppStateChange = (nextAppState: AppStateStatus) => {
		// 안드로이드 device에서는 앱이 백그라운드 상태에서도 계속 음악이 재생되어 수동으로 음악 종료 및 재생.
		if (appState.current.match(/inactive|background/) && nextAppState === 'active') {
			playMusic();
		} else if (nextAppState.match(/inactive|background/)) {
			stopMusic();
		}

		appState.current = nextAppState;
	};

	return (
		<>
			<StatusBar barStyle="dark-content" />
			<Body dispatch={dispatch} state={state} />
		</>
	);
};

export default Main;
