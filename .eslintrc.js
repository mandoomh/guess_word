module.exports = {
	root: true,
	extends: ["plugin:@typescript-eslint/recommended"],
	parser: '@typescript-eslint/parser',
	plugins: ['@typescript-eslint'],
	rules: {
		"@typescript-eslint/no-use-before-define": "off",
		"@typescript-eslint/explicit-function-return-type": "off",
		"@typescript-eslint/interface-name-prefix": "off",
		"@typescript-eslint/no-empty-function": "off",
		"@typescript-eslint/no-unused-vars": "off",
		"semi": ["error", "always"],
		"quotes": ["error", "single", { "allowTemplateLiterals": true }],
	}
};
