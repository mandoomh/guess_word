module.exports = {
	// preset: 'ts-jest',
	preset: 'react-native',
	transform: {
		'^.+\\.(ts|tsx|js|jsx)?$': 'babel-jest',
	},
	transformIgnorePatterns: [
		'node_modules/(?!(react-native'
		+ '|react-navigation-tabs'
		+ '|react-native-splash-screen'
		+ '|react-native-screens'
		+ '|react-native-reanimated'
		+ ')/)',
	],
	globals: {
		__DEV__: true,
	}
};